/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.etern.it.jserienkollar;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import de.etern.it.jserienkollar.datenbank.importobjekte.Episode;
import de.etern.it.jserienkollar.datenbank.importobjekte.Link;
import de.etern.it.jserienkollar.net.LinkProvider;
import de.etern.it.jserienkollar.net.ProviderHandler;
import de.etern.it.jserienkollar.net.StreamProvider;
import de.etern.it.progressMonitor.CommonProgressMonitor;
import de.etern.it.progressMonitor.Progress;

/**
 * Threadklassen zur Linkpr�fung.
 *
 * @author Dennis van der Wals
 *
 */
public class Checker<T> implements Runnable {
    private final Queue<T> thingsToCheck;
    private final String   name;
    
    /**
     * Erstellt einen neuen Checker
     * 
     * @param name
     *            Setzt den Namen des Checkers
     */
    public Checker(String name) {
        this.thingsToCheck = new LinkedList<>();
        this.name = name;
    }
    
    /**
     * F�gt ein neues Objekt ein, wenn es nicht schon vorhanden ist.
     *
     * @param toCheck
     */
    public void addToCheck(T toCheck) {
        this.thingsToCheck.add(toCheck);
    }
    
    /**
     * Fuegt alle Objekte hinzu, die nicht schon vorhanden sind.
     *
     * @param toCheck
     */
    public void addToCheck(List<T> toCheck) {
        toCheck.forEach(this::addToCheck);
    }
    
    @Override
    public void run() {
        DatabaseChecker.getInstance().threadCounter++;
        
        Progress update = CommonProgressMonitor.INSTANCE.createProgress(
                this.name, false, false, true, true);
        update.setMax(this.thingsToCheck.size());
        update.start();
        
        while (!this.thingsToCheck.isEmpty()) {
            // Aktuallisiert die Links zu einer Episode
            
            T nextToCheck = this.thingsToCheck.remove();
            
            if (nextToCheck instanceof Episode) {
                for (LinkProvider provider : ProviderHandler.getInstance()
                        .getLinkProvider()) {
                    provider.updateStreamLinks((Episode) nextToCheck);
                }
                
            } else if (nextToCheck instanceof Link) {
                StreamProvider sp = ProviderHandler.getInstance()
                        .identifyStreamProvider((Link) nextToCheck);
                if (sp != null) {
                    sp.checkLink((Link) nextToCheck);
                    
                    /* Die Pause soll die Verbindung entlassten */
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            
            update.increment();
        }
        
        update.finish();
        DatabaseChecker.getInstance().threadCounter--;
    }
}

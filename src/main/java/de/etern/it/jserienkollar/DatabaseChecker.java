/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
/**
 *
 */
package de.etern.it.jserienkollar;

import de.etern.it.jserienkollar.datenbank.*;
import de.etern.it.jserienkollar.datenbank.importobjekte.Episode;
import de.etern.it.jserienkollar.datenbank.importobjekte.Link;
import de.etern.it.jserienkollar.datenbank.importobjekte.Season;
import de.etern.it.jserienkollar.datenbank.importobjekte.Series;
import de.etern.it.jserienkollar.implement.BugReport;
import de.etern.it.jserienkollar.implement.Console;
import de.etern.it.properties.Property;
import de.etern.it.jserienkollar.net.ProviderHandler;
import de.etern.it.progressMonitor.CommonProgressMonitor;
import de.etern.it.progressMonitor.Counter;
import de.etern.it.progressMonitor.Progress;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Klasse zur regelmäßigen Prüfung, Reinigung und Aktualisierung der
 * Datenbankeinträge.
 *
 * @author Dennis van der Wals
 */
public class DatabaseChecker implements Runnable {
    private static final DatabaseChecker INSTANCE = new DatabaseChecker();
    private static final int NULL_CONDITION = 0, OLD_CONDITION = 1, NO_CONDITION = 2;
    private static final int THREAD_LIMIT = 15;

    int threadCounter;
    private final LinkedList<Thread> waitingThreads;
    private boolean managerRuns;

    /**
     * Gibt die einzige Instanz der Klasse zurück.
     *
     * @return DatabaseChecker
     */
    public static DatabaseChecker getInstance() {
        return INSTANCE;
    }

    public DatabaseChecker() {
        waitingThreads = new LinkedList<>();
        managerRuns = false;
    }

    /**
     * Prüft, welche Bedingungen das Datum erfüllen.
     *
     * @param last  Datum der Letzten Prüfung
     * @param limit Datum, wie lange die letzte Prüfung zurückliegen darf
     * @return NULL_CONDITION: Keine Letzte Prüfung vorhangen, OLD_CONDITION:
     * Letzte Prüfung vor erlaubten Datum, NO_CONDITION: Ist noch
     * aktuell.
     */
    private int checkCondition(Date last, Date limit) {
        if (last == null) {
            return NULL_CONDITION;
        }

        if (last.before(limit)) {
            return OLD_CONDITION;
        }

        return NO_CONDITION;
    }

    /**
     * Erzeugt einen neuen Thread und fuegt ihn der Liste der wartenden Threads
     * hinzu.
     *
     * @param ep Episode, die aktualisiert werden soll.
     */
    public void createChecker(Episode ep) {

        final Episode e = ep;
        waitingThreads.add(new Thread(() -> {

            final Checker<Episode> episodeChecker = new Checker<>(e.getStaffel().getSerie().getTitel() + "-Streamthread");
            episodeChecker.addToCheck(e);
            Thread t = new Thread(episodeChecker);
            t.start();
            try {
                t.join();
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }

            final Checker<Link> linkChecker = new Checker<>(e.getStaffel().getSerie().getTitel() + "-Linkthread");
            linkChecker.addToCheck(e.getStreams());

            new Thread(linkChecker).start();
        }));

    }

    /**
     * Erzeugt einen neuen Thread und fuegt ihn der Liste der wartenden Threads
     * hinzu.
     *
     * @param ep Episode, die aktualisiert werden soll.
     */
    public void createEpisodesChecker(String name, ArrayList<Episode> ep) {

        final String n = name;
        final ArrayList<Episode> e = ep;
        waitingThreads.add(new Thread(() -> {

            final Checker<Episode> episodeChecker = new Checker<>(n + "-Streamthread");
            episodeChecker.addToCheck(e);

            Thread t = new Thread(episodeChecker);
            t.start();
            try {
                t.join();
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }

            final Checker<Link> linkChecker = new Checker<>(n + "-Linkthread");
            for (Episode episode : e) {
                linkChecker.addToCheck(episode.getStreams());
            }

            new Thread(linkChecker).start();

        }));

    }

    /**
     * Erzeugt und reiht eine Linkprüfung ein.
     *
     * @param name    Name des Streams
     * @param streams Liste mit Links
     */
    public void createChecker(String name, ArrayList<Link> streams) {

        final String n = name;
        final ArrayList<Link> s = streams;
        waitingThreads.add(new Thread(() -> {

            final Checker<Link> linkChecker = new Checker<>(n + "-Linkthread");
            linkChecker.addToCheck(s);

            new Thread(linkChecker).start();

        }));

    }

    /**
     * Startet den Threadmanager
     */
    public void startThreadManager() {
        new Thread(() -> {
            Progress aktiv = CommonProgressMonitor.INSTANCE.createProgress("Aktive Threads", false, false, false, false);
            Counter waiting = aktiv.createCounter("Threads in Warteliste", null);
            aktiv.setMax(THREAD_LIMIT);
            // aktiv.setVertical(true);
            aktiv.start();

            while (true) {

                /* Warte auf schleißende Threads */
                while (DatabaseChecker.this.threadCounter > DatabaseChecker.THREAD_LIMIT || DatabaseChecker.this.waitingThreads.isEmpty())
                    try {
                        aktiv.setValue(threadCounter);
                        waiting.setValue(waitingThreads.size());
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                /* Aktiviere Threads, bis limit erreicht ist */
                while (DatabaseChecker.this.threadCounter <= DatabaseChecker.THREAD_LIMIT && !DatabaseChecker.this.waitingThreads.isEmpty()) {
                    DatabaseChecker.this.waitingThreads.poll().start();

                    try {
                        aktiv.setValue(threadCounter);
                        waiting.setValue(waitingThreads.size());
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    @Override
    public void run() {
        Console.getInstance().write(Property.getString("DatabaseChecker.0")); //$NON-NLS-1$

        while (true) {
            Progress update = CommonProgressMonitor.INSTANCE.createProgress("Datenbank prüfung", false, true, true,
                    true);
            update.setMax(SeriesHandler.getInstance().getSeries().size());
            Counter counterEpisodeToCheck = update.createCounter("Episoden zu aktualisieren", null);
            Counter counterEpisode = update.createCounter("Episoden analysiert", null);
            Counter counterStreamLinkToCheck = update.createCounter("Streams zu aktualisieren", null);
            Counter counterStreamLink = update.createCounter("Streams analysiert", null);
            Counter neueEpisoden = update.createCounter("Neue Episoden", null);

            update.start();

            // Initialisiere Daten
            GregorianCalendar lastWeek = new GregorianCalendar();
            GregorianCalendar yesterday = new GregorianCalendar();
            lastWeek.roll(Calendar.WEEK_OF_YEAR, false);
            yesterday.roll(Calendar.DAY_OF_YEAR, false);

			/*
             * Prüfe in 2 Runden. 1. Runde: Nur Elemente ohne Updatedatum 2.
			 * Runde: Elemente mit abgelaufenem Updatedatum
			 */
            for (int i = 0; i < 2; i++) {

                // Prüfe alle Serien ohne Updatedatum
                synchronized (SeriesHandler.getInstance().getSeries()) {

                    for (int index = 0; index < SeriesHandler.getInstance().getSeries().size(); index++) {
                        Series serie = SeriesHandler.getInstance().getSeries().get(index);
                        Progress serienUpdate = CommonProgressMonitor.INSTANCE.createProgress(serie.getTitel() + " update", true, false, false, false);
                        serienUpdate.start();
                        if (this.checkCondition(serie.getSeriesUpdate(), yesterday.getTime()) == i) {
                            // Prüfe auf neue Staffeln/Episoden
                            for (Link providerLink : serie.getLinks()) {
                                int neu = ProviderHandler.getInstance().identifyLinkProvider(providerLink.getProviderID()).updateSerie(serie);

                                neueEpisoden.addValue(neu);
                            }
                        }
                        serienUpdate.finish();

                        // Prüfe Streamliste aller Episoden auf Aktualität
                        ArrayList<Link> streams = new ArrayList<>();
                        ArrayList<Episode> episodes = new ArrayList<>();

                        for (Season staffel : serie.getSeasons()) {

                            // Zaehle nur 1 mal
                            if (i == 0)
                                counterEpisode.addValue(staffel.getEpisodes().size());

                            for (Episode episode : staffel.getEpisodes()) {

                                // Zaehle nur 1 mal
                                if (i == 0)
                                    counterStreamLink.addValue(episode.getStreams().size());

                                // Pruefe nur Episoden, die noch nicht angesehen
                                // wurden
                                if (!episode.hasBeenSeen())
                                    if ((this.checkCondition(episode.getEpisodeUpdate(), yesterday.getTime()) == i))

                                        episodes.add(episode);

                                    else
                                        // Prüfe alle Links auf Aktuallität
                                        for (Link streamlink : episode.getStreams())

                                            if ((this.checkCondition(streamlink.getLinkUpdated(), lastWeek.getTime()) == i))
                                                streams.add(streamlink);
                            }

                        }

                        counterEpisodeToCheck.addValue(episodes.size());
                        if (episodes.size() > 0)
                            this.createEpisodesChecker(serie.getTitel(), episodes);

                        counterStreamLinkToCheck.addValue(streams.size());
                        if (streams.size() > 0)
                            this.createChecker(serie.getTitel(), streams);

                        if (i == 1)
                            update.increment();

                    }
                }
            }

            Counter counterDoubleSeriesDeleted = update.createCounter("Doppelte Serien gelöscht", null);
            Counter counterDoubleSeasonsDeleted = update.createCounter("Doppelte Staffeln gelöscht", null);
            Counter counterDoubleEpisodesDeleted = update.createCounter("Doppelte Episoden gelöscht", null);
            Counter counterDoubleProviderDeleted = update.createCounter("Doppelte Provider gelöscht", null);
            Counter counterDoubleStreamDeleted = update.createCounter("Doppelte Streams gelöscht", null);

			/*
			 * Lösche doppelte Einträge auf allen Ebenen.
			 */
            counterDoubleSeriesDeleted.setValue(removeDoubleEntries(SeriesHandler.getInstance().getSeries()));
            synchronized (SeriesHandler.getInstance().getSeries()) {

                for (Series serie : SeriesHandler.getInstance().getSeries()) {
                    counterDoubleSeasonsDeleted.addValue(removeDoubleEntries(serie.getSeasons()));
                    counterDoubleProviderDeleted.addValue(removeDoubleEntries(serie.getLinks()));

                    for (Season staffel : serie.getSeasons()) {
                        counterDoubleEpisodesDeleted.addValue(removeDoubleEntries(staffel.getEpisodes()));

                        for (Episode episode : staffel.getEpisodes()) {
                            counterDoubleProviderDeleted.addValue(removeDoubleEntries(episode.getProvider()));
                            counterDoubleStreamDeleted.addValue(removeDoubleEntries(episode.getStreams()));
                        }
                    }

                }
            }

            // Anzahl der Datenbankänderungen ausgeben
            if ((counterEpisode.getValue() > 0) && (counterStreamLink.getValue() > 0)) {
                Console.getInstance().write(String.format(Property.getString("DatabaseChecker.1"), //$NON-NLS-1$
                                neueEpisoden.getValue()));

                Console.getInstance().write(String.format(Property.getString("DatabaseChecker.2"), //$NON-NLS-1$
                                counterEpisodeToCheck.getValue(), counterEpisodeToCheck.getPercentage(counterEpisode)));

                Console.getInstance().write(String.format(Property.getString("DatabaseChecker.3"), //$NON-NLS-1$
                                counterStreamLinkToCheck.getValue(), counterStreamLinkToCheck.getPercentage(counterStreamLink), (100 * SeriesHandler.getInstance().getOfflineLinksSize()) / counterStreamLink.getValue()));
            }

            if (counterDoubleEpisodesDeleted.getValue() + counterDoubleProviderDeleted.getValue() + counterDoubleSeasonsDeleted.getValue() + counterDoubleSeriesDeleted.getValue() + counterDoubleStreamDeleted.getValue() > 0) {
                Console.getInstance().write(String.format(Property.getString("DatabaseChecker.4"), //$NON-NLS-1$
                                counterDoubleSeriesDeleted.getValue(), counterDoubleSeasonsDeleted.getValue(), counterDoubleEpisodesDeleted.getValue(), counterDoubleProviderDeleted.getValue(), counterDoubleStreamDeleted.getValue()));
            }

            update.finish();

            if (!managerRuns) {
                startThreadManager();
                managerRuns = true;
            }

            // Thread für eine Stunde anhalten
            try {
                TimeUnit.HOURS.sleep(1);
            } catch (InterruptedException e) {
                BugReport.recordBug(e, false);
            }

            // Speichere Datenbank, damit regelmaeüßige Speicherung stattfindet.
            SeriesHandler.getInstance().save();
        }
    }

    /**
     * Entfernt doppelte Eintrage aus der äbergebenen Liste
     *
     * @param entries Liste mit doppelten Einträgen.
     * @return Anzahl gefundener und geläschter doppelter Einträge.
     */
    public static <T> int removeDoubleEntries(List<T> entries) {

        // Durchsuche alle Ebenen nach doppelten Einträgen
        ArrayList<T> toDelete = new ArrayList<>();
        for (int index = 0; index < entries.size(); index++) {
            T l = entries.get(index);

            if (!toDelete.contains(l)) {

                for (int indexVergleich = index + 1; indexVergleich < entries.size(); indexVergleich++) {
                    T vergleich = entries.get(indexVergleich);

                    // Jeder mehrfache Eintrag muss nur 1 mal gefunden werden.
                    if (vergleich.equals(l) && !toDelete.contains(vergleich))
                        toDelete.add(vergleich);
                }
            }
        }

        // Löscht alle Einträge und das Original
        entries.removeAll(toDelete);

        // Stellt die Originale wieder her
        entries.addAll(toDelete);

        return toDelete.size();
    }

}

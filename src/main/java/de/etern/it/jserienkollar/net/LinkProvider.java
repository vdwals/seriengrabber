/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.etern.it.jserienkollar.net;

import java.util.ArrayList;

import de.etern.it.jserienkollar.datenbank.importobjekte.Episode;
import de.etern.it.jserienkollar.datenbank.SearchResult;
import de.etern.it.jserienkollar.datenbank.importobjekte.Series;

/**
 * Interface als Schnittstelle zu Webseiten, die links zu den Serien sammeln.
 *
 * @author Dennis van der Wals
 *
 */
public abstract class LinkProvider extends Provider {
    protected static final String TAB_CLOSE = ">", TAB_OPEN = "<";
    
    /**
     * Entfernt einen Teil aus dem String und gibt den Nachfolgenden Rest
     * zurueck.
     *
     * @param toCut
     *            Zu zerlegender String.
     * @param startPattern
     *            String, nachdem der gesuchte Teil kommt. Wenn null, wir beim
     *            Anfang gestartet.
     * @param stopPattern
     *            String, der nach dem gesuchten Teil kommt. Wenn null, wird nur
     *            der bis zum Anfang gekuerzte Teil zurueckgegeben.
     * @return gesuchter String
     */
    public static EditedStringPair cutString(String toCut, String startPattern,
            String stopPattern) {
        if (startPattern != null) {
            toCut = toCut.substring(toCut.indexOf(startPattern)
                    + startPattern.length());
        }
        
        if (stopPattern != null) {
            String result = toCut.substring(0, toCut.indexOf(stopPattern));
            toCut = toCut.substring(toCut.indexOf(stopPattern)
                    + stopPattern.length());
            
            return new EditedStringPair(toCut, result);
        }
        return new EditedStringPair(toCut, null);
    }
    
    public LinkProvider(String name, String basicLink, int providerID) {
        super(name, basicLink, providerID);
    }
    
    /**
     * Sammelt Daten zur Serie hinter dem angegebenem Link.
     *
     * @param link
     *            Link zur Serie
     * @return Serie
     */
    public abstract Series getSerie(String link, String name);
    
    /**
     * Gibt die Suchergebnisse zurueck.
     *
     * @param searchString
     *            Sucheingabe
     * @return Ergebnisse
     */
    public abstract ArrayList<SearchResult> search(String searchString);
    
    /**
     * Aktuallisiert eine Serie, pr�ft dabei auf neue Staffeln und Episoden.
     * Neue Episoden werden f�r die Streamlinkaktuallisierung eingetragen.
     *
     * @param serie
     * @return
     */
    public abstract int updateSerie(Series serie);
    
    /**
     * Aktuallisiert die Links der Streamprovider. Neue Links werden zur
     * Link�berpr�fung eingetragen.
     *
     * @param episode
     *            Episoden
     * @param silent
     *            Stellt ein, ob eine Fortschritt angezeigt werden soll
     */
    public abstract void updateStreamLinks(Episode episode);
}

package de.etern.it.jserienkollar.net.streamprovider;

import java.util.Date;

import de.etern.it.jserienkollar.datenbank.Link;
import de.etern.it.jserienkollar.net.StreamProvider;
import de.etern.it.jserienkollar.net.WebsiteGrabber;

/**
 * Klasse zur Analyse von Links auf bitshare
 *
 * @author Dennis
 *
 */
public class Nowvideo_sx extends StreamProvider {
	public static final String NAME = "nowvideo.sx", BASIC_LINK = "www." + NAME + "/video/";

	public Nowvideo_sx(int id) {
		super(NAME, BASIC_LINK, id);
	}

	@Override
	public boolean checkLink(Link link) {
		try {
			String test = WebsiteGrabber.ladeSeite(link.getLink(), "<h3>", "</h3>");

			// DEBUG closed
			// System.out.println("Offline: " + (test.contains("Error")));
			// System.out.println("Link: " + link.getLink());
			// System.out.println();

			link.setOffline(test.contains("This file no longer exists"));
			link.setLinkUpdated(new Date());

		} catch (Exception e) {
			link.setLinkUpdated(null);
			// e.printStackTrace();
		}

		return link.isOffline();
	}

}

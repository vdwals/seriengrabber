/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.etern.it.jserienkollar.net;

import de.etern.it.jserienkollar.datenbank.importobjekte.Link;
import de.etern.it.jserienkollar.jaxb.StreamProviderJ;
import de.etern.it.jserienkollar.implement.CommonUtils;

import java.util.Date;

/**
 * Klasse zur Analyse von Links auf bitshare
 *
 * @author Dennis
 */
public class GenericStreamProvider extends de.etern.it.jserienkollar.net.StreamProvider {
    private String startPattern;
    private String endPattern;
    private String basicEmbeddedLink;
    private String embeddedPattern;
    private String embeddedStartPattern;
    private String embeddedEndPattern;
    private String testStringPattern;

    private boolean testWithStringLoad;
    private boolean tranformEmbeddedLink;
    private boolean testIfPatternIncluded;

    public GenericStreamProvider(String name, String basicLink, String startPattern, String endPattern, String basicEmbeddedLink, String embeddedPattern, String embeddedStartPattern, String embeddedEndPattern, String testStringPattern, boolean testWithStringLoad, boolean tranformEmbeddedLink, boolean testIfPatternIncluded, int priority) {
        super(name, basicLink,0, priority);
        this.startPattern = startPattern;
        this.endPattern = endPattern;
        this.basicEmbeddedLink = basicEmbeddedLink;
        this.embeddedPattern = embeddedPattern;
        this.embeddedStartPattern = embeddedStartPattern;
        this.embeddedEndPattern = embeddedEndPattern;
        this.testStringPattern = testStringPattern;
        this.testWithStringLoad = testWithStringLoad;
        this.tranformEmbeddedLink = tranformEmbeddedLink;
        this.testIfPatternIncluded = testIfPatternIncluded;
    }

    private boolean checkEmbedded() {
        return embeddedPattern != null && !embeddedPattern.isEmpty();
    }

    public GenericStreamProvider(String name) {
        super(name, name, -1, 0);
    }

    public StreamProviderJ getJAXB() {
        return new StreamProviderJ(getName(), getBasicLink(), startPattern, endPattern, basicEmbeddedLink, embeddedPattern, embeddedStartPattern, embeddedEndPattern, testStringPattern, testWithStringLoad, tranformEmbeddedLink, testIfPatternIncluded, getPriority());
    }

    @Override
    public boolean checkLink(Link link) {
        try {
            String linkToCheck = link.getLink();
            // Pruefe ob eingebetteter Link vorliegt
            if (checkEmbedded() && linkToCheck.contains(embeddedPattern)) {

                // Lade korrekten Link
                linkToCheck = WebsiteGrabber.ladeSeite(linkToCheck, embeddedStartPattern, embeddedEndPattern);
                String basicLink = getBasicLink();
                if (basicEmbeddedLink != null && !basicEmbeddedLink.isEmpty())
                    basicLink = basicEmbeddedLink;

                if (tranformEmbeddedLink)
                    link.setLink(Link.HTTP + basicLink + linkToCheck);
            }

            String test;
            if (!CommonUtils.isOneStringNullOrEmpty(startPattern, endPattern))
                test = WebsiteGrabber.ladeSeite(linkToCheck, startPattern, endPattern);
            else
                test = WebsiteGrabber.ladeSeite(linkToCheck);

            // TODO equals
            if (testStringPattern != null && !testStringPattern.isEmpty())
                link.setOffline(test.contains(testStringPattern) && !testIfPatternIncluded);
            else
                link.setOffline(test.isEmpty());

            link.setLinkUpdated(new Date());

        } catch (Exception e) {
            if (!testWithStringLoad) {
                link.setOffline(true);
                link.setLinkUpdated(new Date());
            } else {
                link.setLinkUpdated(null);
            }
        }

        return link.isOffline();
    }

}

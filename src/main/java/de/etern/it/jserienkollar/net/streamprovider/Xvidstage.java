/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.etern.it.jserienkollar.net.streamprovider;

import java.util.Date;

import de.etern.it.jserienkollar.datenbank.importobjekte.Link;
import de.etern.it.jserienkollar.net.StreamProvider;

/**
 * Klasse zur Analyse von Links auf bitshare
 *
 * @author Dennis
 *
 */
public class Xvidstage extends StreamProvider {
    public static final String NAME = "xvidstage.com", BASIC_LINK = NAME + "/";

    public Xvidstage(int id) {
        super(NAME, BASIC_LINK, id);
    }

    @Override
    public boolean checkLink(Link link) {
        link.setOffline(true);
        link.setLinkUpdated(new Date());

        return true;
    }

}

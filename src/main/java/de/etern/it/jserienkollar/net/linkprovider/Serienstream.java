package de.etern.it.jserienkollar.net.linkprovider;

import java.util.ArrayList;

import de.etern.it.jserienkollar.datenbank.Episode;
import de.etern.it.jserienkollar.datenbank.SearchResult;
import de.etern.it.jserienkollar.datenbank.Series;
import de.etern.it.jserienkollar.net.LinkProvider;
import de.etern.it.jserienkollar.net.WebsiteGrabber;

public class Serienstream extends LinkProvider {
	private static final String NAME = "serienstream.to", BASIC_LINK = "https://" + NAME;

	private static final String SEARCH_LINK = BASIC_LINK + "/serien", SEARCH_START = "class=\"seriesList\"",
			SEARCH_END = "class=\"homeContentGenresList\"";

	public Serienstream(int providerID) {
		super(NAME, BASIC_LINK, providerID);
	}

	@Override
	public Series getSerie(String link, String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<SearchResult> search(String searchString) {
		String serienSeite = WebsiteGrabber.ladeSeite(SEARCH_LINK, SEARCH_START, SEARCH_END);

		return null;
	}

	@Override
	public int updateSerie(Series serie) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void updateStreamLinks(Episode episode) {
		// TODO Auto-generated method stub

	}

}

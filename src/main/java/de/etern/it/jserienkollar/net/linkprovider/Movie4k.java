package de.etern.it.jserienkollar.net.linkprovider;

import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

import de.etern.it.jserienkollar.datenbank.Language;
import de.etern.it.jserienkollar.datenbank.SearchResult;
import de.etern.it.jserienkollar.datenbank.tables.Episode;
import de.etern.it.jserienkollar.datenbank.tables.Link;
import de.etern.it.jserienkollar.datenbank.tables.Serie;
import de.etern.it.jserienkollar.gui.WorkingInProgress;
import de.etern.it.jserienkollar.implement.BugReport;
import de.etern.it.jserienkollar.implement.Console;
import de.etern.it.jserienkollar.net.EditedStringPair;
import de.etern.it.jserienkollar.net.LinkProvider;
import de.etern.it.jserienkollar.net.ProviderHandler;
import de.etern.it.jserienkollar.net.StreamProvider;
import de.etern.it.jserienkollar.net.WebsiteGrabber;
import de.etern.it.properties.Property;
import javafx.scene.chart.XYChart.Series;

public class Movie4k extends LinkProvider {
	private static final String NAME = "movie4k.tv", //$NON-NLS-1$
			BASIC_LINK = "www." + NAME + "/", //$NON-NLS-1$ //$NON-NLS-2$
			SUCH_URL = "http://" + BASIC_LINK + "movies.php", //$NON-NLS-1$
			SUCH_COMMAND[] = { "list", "search" }, //$NON-NLS-1$
			SUCH_PARAMETER = "search", //$NON-NLS-1$
			SUCH_PATTERN_START = "<TABLE id=\"tablemoviesindex\">", //$NON-NLS-1$
			SUCH_PATTERN_STOPP = "</TABLE>", //$NON-NLS-1$
			LANGUAGE_FILTER_1 = "id=\"tdmovies\"", LANGUAGE_FILTER_2 = "png", //$NON-NLS-1$ //$NON-NLS-2$
			LINK_PATTERN_START = "href=\"", LINK_PATTERN_STOPP = "\"", //$NON-NLS-1$ //$NON-NLS-2$
			REL_LINK_PATTERN_START = "value=\"", REL_LINK_PATTERN_STOPP = "\"", //$NON-NLS-1$ //$NON-NLS-2$
			STREAM_LIST_FILTER = "tablemoviesindex2", //$NON-NLS-1$
			STREAM_BASIC_LINK_PATTERN_START = "tvshows-", //$NON-NLS-1$
			STREAM_BASIC_LINK_PATTERN_STOPP = "'", //$NON-NLS-1$
			STREAM_BASIC_LINK_PATTERN_SPLIT = "-", //$NON-NLS-1$
			STREAM_BASIC_LINK = Link.HTTP + BASIC_LINK + STREAM_BASIC_LINK_PATTERN_START,
			STAFFEL_PATTERN_START = "<SELECT name=\"season\"", //$NON-NLS-1$
			STAFFEL_PATTERN_STOP = "</tr>", //$NON-NLS-1$
			STAFFEL_FILTER = "<OPTION value=\"", //$NON-NLS-1$
			EPISODE_PATTERN_START = "Episode ", EPISODE_PATTERN_STOP = "<", //$NON-NLS-1$ //$NON-NLS-2$
			STREAM_LINK_PATTERN_START = "<a target=\"_blank\" " //$NON-NLS-1$
					+ LINK_PATTERN_START,
			SOURCE_START = "src=\"", //$NON-NLS-1$
			STREAM_FRAME_PATTERN_START = "<iframe " + SOURCE_START, //$NON-NLS-1$
			STREAM_FRAME_FILTER = "underplayer", //$NON-NLS-1$
			STREAM_LINK_MULTI_FILTER = "links[", STREAM_LINK_MULTI_STOPP = "]"; //$NON-NLS-1$ //$NON-NLS-2$

	/** Standardkonstruktor **/
	public Movie4k(int id) {
		super(NAME, BASIC_LINK, id);
	}

	@Override
	public Serie getSerie(String link, String name) {
		Serie neu = new Serie();
		neu.setTitel(name);
		neu.getLinks().add(new Link(link, getProviderID(), false));

		return neu;
	}

	/**
	 * Liest die Sprache aus dem hinterlegtem Icon aus.
	 *
	 * @param language
	 *            hinterlegtes Icon
	 * @return Sprache
	 */
	private Language identifyLanguage(String language) {
		if (language.contains("us_ger")) { //$NON-NLS-1$
			return Language.DEUTSCH;

		} else if (language.contains("us_flag")) { //$NON-NLS-1$
			return Language.ENGLISCH;

		} else if (language.contains("/4.png")) { //$NON-NLS-1$
			return Language.CHINESISCH;

		} else if (language.contains("/flag_spain")) { //$NON-NLS-1$
			return Language.SPANISCH;

		} else if (language.contains("/flag_france")) { //$NON-NLS-1$
			return Language.FRANZOESISCH;

		} else if (language.contains("/7.png")) { //$NON-NLS-1$
			return Language.TUERKISCH;

		} else if (language.contains("/8.png")) { //$NON-NLS-1$
			return Language.JAPANISCH;

		} else if (language.contains("/9.png")) { //$NON-NLS-1$
			return Language.SUERISCH;

		} else if (language.contains("/11.png")) { //$NON-NLS-1$
			return Language.ITALIENISCH;

		} else if (language.contains("/12.png")) { //$NON-NLS-1$
			return Language.KROATISCH;

		} else if (language.contains("russia")) { //$NON-NLS-1$
			return Language.RUSSISCH;

		} else if (language.contains("/14.png")) { //$NON-NLS-1$
			return Language.BOSSNISCH;

		} else if (language.contains("//xxx")) { //$NON-NLS-1$
			return Language.DEUTSCH_ENGLISCH;

		} else if (language.contains("/16.png")) { //$NON-NLS-1$
			return Language.NIEDERLAENDISCH;

		} else if (language.contains("/17.png")) { //$NON-NLS-1$
			return Language.KOREANISCH;

		} else if (language.contains("/21.png")) { //$NON-NLS-1$
			return Language.ISLAENDISCH;

		} else if (language.contains("/22.png")) { //$NON-NLS-1$
			return Language.POLNISCH;

		} else if (language.contains("/23.png")) { //$NON-NLS-1$
			return Language.FINNISCH;

		} else if (language.contains("/24.png")) { //$NON-NLS-1$
			return Language.GRIECHISCH;

		} else {
			BugReport.recordBug(new IllegalArgumentException("Sprache nicht erkannt" + language + " auf " + getName()), //$NON-NLS-1$ //$NON-NLS-2$
					true);
		}
		return null;
	}

	@Override
	public ArrayList<SearchResult> search(String searchString) {

		WorkingInProgress.getInstance().addChild(getName() + Property.getString("Movie4k.1")); //$NON-NLS-1$
		String[] search = { SUCH_PARAMETER, searchString };
		Scanner seitenScanner = new Scanner(
				WebsiteGrabber.sendeGetRequest(SUCH_URL, SUCH_COMMAND, search, SUCH_PATTERN_START, SUCH_PATTERN_STOPP));

		ArrayList<SearchResult> searchResults = new ArrayList<>();
		while (seitenScanner.hasNextLine()) {
			String zeile = ""; //$NON-NLS-1$

			// Bis Zeile mit link springen
			while (seitenScanner.hasNextLine() && !(zeile = seitenScanner.nextLine()).contains(LINK_PATTERN_START)) {
			}
			if (!seitenScanner.hasNextLine()) {
				break;
			}

			EditedStringPair res = LinkProvider.cutString(zeile, LINK_PATTERN_START, LINK_PATTERN_STOPP);
			String link = Link.HTTP + BASIC_LINK + res.getSearched();

			res = LinkProvider.cutString(res.getEdited(), TAB_CLOSE, TAB_OPEN);
			String titel = res.getSearched();

			// id="tdmovies" width="25"><img
			// Bis Zeile mit Sprache springen
			while (seitenScanner.hasNextLine() && !((zeile = seitenScanner.nextLine()).contains(LANGUAGE_FILTER_1)
					&& zeile.contains(LANGUAGE_FILTER_2))) {
			}
			String language = zeile;

			// DEBUG closed
			// System.out.println("Titel: " + titel);
			// System.out.println("Link: " + link);
			// System.out.println("language: " + language);
			// System.out.println();

			searchResults.add(new SearchResult(identifyLanguage(language), titel, link, getProviderID()));
		}

		seitenScanner.close();
		WorkingInProgress.getInstance().removeChild();

		return searchResults;
	}

	@Override
	public int updateSerie(de.etern.it.jserienkollar.datenbank.importobjekte.Series serie) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updateSerie(Series serie) {
		int neu = 0;

		for (Link link : serie.getLinks()) {
			if (link.getProviderID() == getProviderID()) {

				// Seite mit Staffelinformationen auslesen
				Scanner seitenScanner = new Scanner(
						WebsiteGrabber.ladeSeite(link.getLink(), STAFFEL_PATTERN_START, STAFFEL_PATTERN_STOP));

				boolean staffeln = false;
				int staffel = 0;
				// Durch alle Staffeln gehen
				while (seitenScanner.hasNextLine()) {
					String zeile = seitenScanner.nextLine();

					// Filter
					if (zeile.contains(STAFFEL_FILTER)) {

						// Suche staffeln
						if (!staffeln) {
							while (zeile.contains(STAFFEL_FILTER)) {
								EditedStringPair res = LinkProvider.cutString(zeile, REL_LINK_PATTERN_START,
										REL_LINK_PATTERN_STOPP);
								int staffelnummer = Integer.parseInt(res.getSearched());
								zeile = res.getEdited();

								Season neueStaffel = new Season(staffelnummer, serie);

								// Pr�fe auf neue Staffel
								if (serie.addSeason(neueStaffel)) {
									Console.getInstance()
											.write(Property.getString("Movie4k.3") //$NON-NLS-1$
													+ serie.getTitel() + " - " //$NON-NLS-1$
													+ neueStaffel.toString());
								}
							}
							staffeln = true;

							// Episoden suchen
						} else {
							while (zeile.contains(STAFFEL_FILTER)) {
								EditedStringPair res = LinkProvider.cutString(zeile, REL_LINK_PATTERN_START,
										REL_LINK_PATTERN_STOPP);
								String linkZuStreams = res.getSearched();

								// DEBUG closed
								// System.out.println(link.getLink());
								// System.out.println(seite);
								// System.out.println(zeile);

								res = LinkProvider.cutString(res.getEdited(), EPISODE_PATTERN_START,
										EPISODE_PATTERN_STOP);
								int nummer = Integer.parseInt(res.getSearched());

								zeile = res.getEdited();

								Episode neueEpisode = new Episode(nummer, serie.getSeasons().get(staffel));
								neueEpisode.addProviderLink(
										new Link(Link.HTTP + BASIC_LINK + linkZuStreams, getProviderID(), false));

								// Pr�fe, ob Episode schon vorhanden
								if (serie.getSeasons().get(staffel).addEpisode(neueEpisode)) {
									Console.getInstance()
											.write(Property.getString("Movie4k.3") //$NON-NLS-1$
													+ serie.getTitel() + " - " //$NON-NLS-1$
													+ serie.getSeasons().get(staffel).toString() + " - " //$NON-NLS-1$
													+ neueEpisode.toString());

									serie.getSeasons().get(staffel).setSeasonUpdate(new Date());

									neu++;

									updateStreamLinks(neueEpisode);
								}
							}
							staffel++;
						}

					}
					serie.setSeriesUpdate(new Date());
				}

				seitenScanner.close();
			}
		}

		return neu;
	}

	@Override
	public void updateStreamLinks(de.etern.it.jserienkollar.datenbank.importobjekte.Episode episode) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateStreamLinks(Episode episode) {
		if (episode == null) {
			return;
		}

		for (Link link : episode.getProvider()) {
			if (link.getProviderID() == getProviderID()) {

				// Starte Verbindung
				Scanner seitenScanner = new Scanner(
						WebsiteGrabber.ladeSeite(link.getLink(), STREAM_LIST_FILTER, SUCH_PATTERN_STOPP.toLowerCase()));

				String basicLink = ""; //$NON-NLS-1$
				while (seitenScanner.hasNextLine()) {
					String zeile = seitenScanner.nextLine();

					// Filter
					if (zeile.contains(STREAM_BASIC_LINK_PATTERN_START)) {
						// Basislink fuer Streamseiten auslesen
						if (basicLink.length() == 0) {
							EditedStringPair res = LinkProvider.cutString(zeile, STREAM_BASIC_LINK_PATTERN_START,
									STREAM_BASIC_LINK_PATTERN_STOPP);
							basicLink = res.getSearched().replaceAll("[0-9]", ""); //$NON-NLS-1$ //$NON-NLS-2$
						}

						// Link zur Seite mit Streamlink auslesen
						int linkNummer;
						/*
						 * Gibt es mehrere Mirrors, steht die linknummer am
						 * Anfang
						 */
						if (zeile.startsWith(STREAM_LINK_MULTI_FILTER)) {
							EditedStringPair res = LinkProvider.cutString(zeile, STREAM_LINK_MULTI_FILTER,
									STREAM_LINK_MULTI_STOPP);
							linkNummer = Integer.parseInt(res.getSearched());

							/*
							 * Gibt es nur einen Mirror, steht die linknummer im
							 * link
							 */
						} else {
							EditedStringPair res = LinkProvider.cutString(zeile, STREAM_BASIC_LINK_PATTERN_START,
									STREAM_BASIC_LINK_PATTERN_SPLIT);
							linkNummer = Integer.parseInt(res.getSearched());

						}

						String linkZuStreamLink = STREAM_BASIC_LINK + linkNummer + basicLink;

						// Lese Streamlink aus Seite heraus
						String streamlink = WebsiteGrabber.ladeSeite(linkZuStreamLink, STREAM_LINK_PATTERN_START,
								LINK_PATTERN_STOPP);

						/* Wenn Streamlink in eingebetter Form */
						if (streamlink.length() == 0) {
							Scanner linkScanner = new Scanner(WebsiteGrabber.ladeSeite(linkZuStreamLink,
									STREAM_FRAME_PATTERN_START, STREAM_FRAME_FILTER));

							// Link steht in der letzten Zeile
							while (linkScanner.hasNextLine()) {
								String linkzeile = linkScanner.nextLine();

								// Lese Link aus letzter Zeile
								if (!linkScanner.hasNextLine()) {
									EditedStringPair res = LinkProvider.cutString(linkzeile, SOURCE_START,
											LINK_PATTERN_STOPP);
									streamlink = res.getSearched();
								}
							}
							linkScanner.close();
						}

						StreamProvider s = ProviderHandler.getInstance().identifyStreamProvider(streamlink);
						Link streamLink = new Link(streamlink, (s != null) ? s.getProviderID() : 0, true);

						episode.addStreamLink(streamLink);
					}
					episode.setEpisodeUpdate(new Date());
				}
				seitenScanner.close();

				break;
			}
		}
	}
}

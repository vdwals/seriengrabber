/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.etern.it.jserienkollar.net.linkprovider;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

import de.etern.it.jserienkollar.datenbank.importobjekte.Episode;
import de.etern.it.jserienkollar.datenbank.importobjekte.Link;
import de.etern.it.jserienkollar.datenbank.Language;
import de.etern.it.jserienkollar.datenbank.SearchResult;
import de.etern.it.jserienkollar.datenbank.importobjekte.Season;
import de.etern.it.jserienkollar.datenbank.importobjekte.Series;
import de.etern.it.jserienkollar.gui.WorkingInProgress;
import de.etern.it.jserienkollar.implement.BugReport;
import de.etern.it.jserienkollar.implement.Console;
import de.etern.it.properties.Property;
import de.etern.it.jserienkollar.net.EditedStringPair;
import de.etern.it.jserienkollar.net.LinkProvider;
import de.etern.it.jserienkollar.net.ProviderHandler;
import de.etern.it.jserienkollar.net.StreamProvider;
import de.etern.it.jserienkollar.net.WebsiteGrabber;

public class Kinox extends LinkProvider {
    private static final String NAME      = "kinox.to", //$NON-NLS-1$
            SUCH_URL = Link.HTTP + NAME + "/Search.html?q=", //$NON-NLS-1$
            EPISODE_BASIC_LINK = Link.HTTP + NAME + "/aGET/MirrorByEpisode/", //$NON-NLS-1$
            EPISODE_SEASON_LINK = "&Season=", //$NON-NLS-1$
            EPISODE_EPISODE_LINK = "&Episode=", STREAM_BASIC_LINK = Link.HTTP + NAME //$NON-NLS-1$
                    + "/aGET/Mirror/", STREAM_MIRROR_LINK = "&Mirror=", //$NON-NLS-1$ //$NON-NLS-2$
            SUCH_PATTERN_START = "<tbody>", SUCH_PATTERN_STOPP = "</tbody>", //$NON-NLS-1$ //$NON-NLS-2$
            KEY_WORD_SEARCH_RESULTS_1 = "language", //$NON-NLS-1$
            KEY_WORD_SEARCH_RSULTS_2 = "series", //$NON-NLS-1$
            LINK_PATTERN_START = "href=\"", LINK_PATTERN_STOPP = "\"", //$NON-NLS-1$ //$NON-NLS-2$
            REL_LINK_PATTERN_START = "rel=\"", REL_LINK_PATTERN_STOPP = "\"", //$NON-NLS-1$ //$NON-NLS-2$
            STAFFEL_PATTERN_START = "<select", //$NON-NLS-1$
            STAFFEL_PATTERN_STOP = "</select>", EPISODE_FILTER = "value=\"", //$NON-NLS-1$ //$NON-NLS-2$
            EPISODE_SPLITTER = ",", STREAM_LIST_PATTERN_START = "li", //$NON-NLS-1$ //$NON-NLS-2$
            STREAM_LIST_PATTERN_STOPP = "</ul>", STREAM_LIST_FILTER = "id", //$NON-NLS-1$ //$NON-NLS-2$
            LINK_PATTERN_DELETE = "amp;", //$NON-NLS-1$
            STREAM_LIST_MIRROR_FILTER = "<b>Mirror</b>: ", //$NON-NLS-1$
            STREAM_LIST_MIRROR_COUNTER_FILTER = "/", //$NON-NLS-1$
            STREAM_LIST_DATE_FILTER = "<b>Vom</b>: ", //$NON-NLS-1$
            FILTER_ADVERTISEMENT = "http://ads";                               //$NON-NLS-1$
    private static final DateFormat   formatter = new SimpleDateFormat("dd.MM.yyyy"); //$NON-NLS-1$
                                                                                
    /** Standardkonstruktor **/
    public Kinox(int id) {
        super(NAME, NAME, id);
    }
    
    /**
     * Bildet die Links zum Zugriff auf die Streamlinks
     *
     * @param mirrors
     *            Anzahl der Mirrors
     * @param linkPart
     *            Beispiellink zu einem Streamlink
     * @return Liste mit Links zu allen Streamlinks
     */
    private ArrayList<String> createEpisodeLinksToStreamLinks(int mirrors,
            String linkPart) {
        ArrayList<String> links = new ArrayList<>();
        
        if (mirrors == 1) {
            links.add(STREAM_BASIC_LINK + linkPart);
            
        } else {
            String[] elements = linkPart.split("&"); //$NON-NLS-1$
            
            String part1 = STREAM_BASIC_LINK + elements[0] + "&" + elements[1] //$NON-NLS-1$
                    + STREAM_MIRROR_LINK;
            String part2 = "&" + elements[3] + "&" + elements[4]; //$NON-NLS-1$ //$NON-NLS-2$
            for (int i = 1; i <= mirrors; i++) {
                links.add(part1 + i + part2);
            }
        }
        
        return links;
    }
    
    /**
     * Uebersetzt gesammelte Daten in einen Link zu den Streamprovidern.
     *
     * @param seriesLink
     *            Basislink zur Serie
     * @param season
     *            Staffelnummer
     * @param episode
     *            Episodennummer
     * @return Link
     */
    private Link getProviderLinksToStreams(String seriesLink, int season,
            int episode) {
        return new Link(EPISODE_BASIC_LINK + seriesLink + EPISODE_SEASON_LINK
                + season + EPISODE_EPISODE_LINK + episode,
                this.getProviderID(), false);
    }
    
    /**
     * Filter Link zum Stream und Provider heraus.
     *
     * @param linkToStreamLink
     * @return
     */
    private Link getEpisodeStreamLink(String linkToStreamLink) {
        // Link filtern
        // DEBUG closed
        // System.out.println(streamLink);
        
        String link = WebsiteGrabber.ladeSeite(linkToStreamLink,
                "<a href=\\\"", "\\\""); //$NON-NLS-1$ //$NON-NLS-2$
        
        if (link.isEmpty())
            return null;
        
        link = link.replace("/Out/?s=", ""); //$NON-NLS-1$ //$NON-NLS-2$
        link = link.replace("\\", ""); //$NON-NLS-1$ //$NON-NLS-2$
        link = link.replace("/Out/?s=", ""); //$NON-NLS-1$ //$NON-NLS-2$
        
        // DEBUG closed
        // System.out.println(link);
        
        StreamProvider s = ProviderHandler.getInstance()
                .identifyStreamProvider(link);
        return new Link(link, (s != null) ? s.getProviderID() : 0, true);
    }
    
    @Override
    public Series getSerie(String link, String name) {
        Series neu = new Series();
        neu.setTitel(name);
        neu.getLinks().add(new Link(link, this.getProviderID(), false));
        
        return neu;
    }
    
    /**
     * Liest die Sprache aus dem hinterlegtem Icon aus.
     *
     * @param language
     *            hinterlegtes Icon
     * @return Sprache
     */
    private Language identifyLanguage(String language) {
        if (language.contains("/1.png")) { //$NON-NLS-1$
            return Language.DEUTSCH;
            
        } else if (language.contains("/2.png")) { //$NON-NLS-1$
            return Language.ENGLISCH;
            
        } else if (language.contains("/4.png")) { //$NON-NLS-1$
            return Language.CHINESISCH;
            
        } else if (language.contains("/5.png")) { //$NON-NLS-1$
            return Language.SPANISCH;
            
        } else if (language.contains("/6.png")) { //$NON-NLS-1$
            return Language.FRANZOESISCH;
            
        } else if (language.contains("/7.png")) { //$NON-NLS-1$
            return Language.TUERKISCH;
            
        } else if (language.contains("/8.png")) { //$NON-NLS-1$
            return Language.JAPANISCH;
            
        } else if (language.contains("/9.png")) { //$NON-NLS-1$
            return Language.SUERISCH;
            
        } else if (language.contains("/11.png")) { //$NON-NLS-1$
            return Language.ITALIENISCH;
            
        } else if (language.contains("/12.png")) { //$NON-NLS-1$
            return Language.KROATISCH;
            
        } else if (language.contains("/13.png")) { //$NON-NLS-1$
            return Language.RUSSISCH;
            
        } else if (language.contains("/14.png")) { //$NON-NLS-1$
            return Language.BOSSNISCH;
            
        } else if (language.contains("/15.png")) { //$NON-NLS-1$
            return Language.DEUTSCH_ENGLISCH;
            
        } else if (language.contains("/16.png")) { //$NON-NLS-1$
            return Language.NIEDERLAENDISCH;
            
        } else if (language.contains("/17.png")) { //$NON-NLS-1$
            return Language.KOREANISCH;
            
        } else if (language.contains("/21.png")) { //$NON-NLS-1$
            return Language.ISLAENDISCH;
            
        } else if (language.contains("/22.png")) { //$NON-NLS-1$
            return Language.POLNISCH;
            
        } else if (language.contains("/23.png")) { //$NON-NLS-1$
            return Language.FINNISCH;
            
        } else if (language.contains("/24.png")) { //$NON-NLS-1$
            return Language.GRIECHISCH;
            
        } else {
            BugReport
                    .recordBug(
                            new IllegalArgumentException(
                                    "Sprache nicht erkannt" + language + " auf " + this.getName()), true);//$NON-NLS-1$ //$NON-NLS-2$
        }
        return null;
    }
    
    @Override
    public ArrayList<SearchResult> search(String searchString) {
        
        WorkingInProgress.getInstance().addChild(
                this.getName() + Property.getString("Kinox.4")); //$NON-NLS-1$
        
        Scanner seitenScanner = new Scanner(WebsiteGrabber.ladeSeite(SUCH_URL
                + searchString, SUCH_PATTERN_START, SUCH_PATTERN_STOPP));
        
        ArrayList<SearchResult> searchResults = new ArrayList<>();
        while (seitenScanner.hasNextLine()) {
            String zeile = seitenScanner.nextLine();
            
            // Sprache finden
            if (zeile.contains(KEY_WORD_SEARCH_RESULTS_1)) {

                // Serienfilter
                if (seitenScanner.nextLine().contains(KEY_WORD_SEARCH_RSULTS_2)) {
                    
                    EditedStringPair res = cutString(seitenScanner.nextLine(),
                            LINK_PATTERN_START, LINK_PATTERN_STOPP);
                    String link = res.getSearched();
                    
                    res = cutString(res.getEdited(), TAB_CLOSE, TAB_OPEN);
                    String titel = res.getSearched();
                    
                    // filter = "class=\"Year\">";
                    // zeile = zeile.substring(zeile.indexOf(filter)
                    // + filter.length());
                    // filter = "<";
                    // String jahr = zeile.substring(0,
                    // zeile.indexOf(filter));
                    // zeile = zeile.substring(zeile.indexOf(filter)
                    // + filter.length());
                    
                    // ADVERTISEMENT-FILTER
                    if (!link.contains(FILTER_ADVERTISEMENT)) {
                        searchResults.add(new SearchResult(this
                                .identifyLanguage(zeile), titel, Link.HTTP
                                + NAME + link, this.getProviderID()));
                    }
                    
                    // DEBUG closed
                    // System.out
                    // .println(searchResults.get(searchResults.size() -
                    // 1));
                }
            }
        }
        
        seitenScanner.close();
        WorkingInProgress.getInstance().removeChild();
        
        return searchResults;
    }
    
    @Override
    public int updateSerie(Series serie) {
        int neu = 0;
        
        for (Link link : serie.getLinks()) {
            if (link.getProviderID() == this.getProviderID()) {
                
                // Seite mit Staffelinformationen auslesen
                Scanner seitenScanner = new Scanner(WebsiteGrabber.ladeSeite(
                        link.getLink(), STAFFEL_PATTERN_START,
                        STAFFEL_PATTERN_STOP));
                
                if (!seitenScanner.hasNextLine()) {
                    seitenScanner.close();
                    return neu;
                }
                
                // Hauptlink fuer alle Episoden auslesen
                EditedStringPair res = cutString(seitenScanner.nextLine(),
                        REL_LINK_PATTERN_START, REL_LINK_PATTERN_STOPP);
                String linkZurSeite = res.getSearched().replaceAll(
                        LINK_PATTERN_DELETE, ""); //$NON-NLS-1$
                
                // Durch alle Staffeln gehen
                while (seitenScanner.hasNextLine()) {
                    String zeile = seitenScanner.nextLine();
                    
                    // Filter
                    if (zeile.contains(EPISODE_FILTER)) {
                        
                        // Episoden auslesen
                        res = cutString(zeile, EPISODE_FILTER,
                                REL_LINK_PATTERN_STOPP);
                        int staffelNummer = Integer.parseInt(res.getSearched());
                        
                        res = cutString(res.getEdited(),
                                REL_LINK_PATTERN_START, REL_LINK_PATTERN_STOPP);
                        String[] episoden = res.getSearched().split(
                                EPISODE_SPLITTER);
                        // zeile = zeile.substring(zeile.indexOf(filter)
                        // + filter.length());
                        
                        // DEBUG closed
                        // System.out.println(linkZurSeite);
                        // System.out.println(staffelNummer);
                        // for (String string : episoden) {
                        // System.out.println(string);
                        // }
                        
                        Season neueStaffel = new Season(staffelNummer, serie);
                        
                        // Pr�fe auf neue Staffel
                        if (!serie.addSeason(neueStaffel)) {
                            neueStaffel = serie.getSeasons().get(
                                    serie.getSeasons().indexOf(neueStaffel));
                            
                        } else {
                            Console.getInstance()
                                    .write(Property.getString("Kinox.0") + serie.getTitel() + " - " //$NON-NLS-1$ //$NON-NLS-2$
                                            + neueStaffel.toString());
                            
                        }
                        
                        /*
                         * Episodenpr�fung ist sehr einfach, da nur die Nummer
                         * der Staffel und Episode notwendig ist, um den Link zu
                         * den Streams auslesen zu k�nnen
                         */
                        
                        // Pruefe, ob neue Episoden hinzugekommen sind
                        for (String string : episoden) {
                            Episode neueEpisode = new Episode(
                                    Integer.parseInt(string), neueStaffel);
                            
                            // Links der Episode eintragen
                            neueEpisode.addProviderLink(this
                                    .getProviderLinksToStreams(linkZurSeite,
                                            neueStaffel.getSeasonNumber(),
                                            neueEpisode.getEpisodeNumber()));
                            
                            // Pr�fe, ob Episode schon vorhanden
                            if (neueStaffel.addEpisode(neueEpisode)) {
                                Console.getInstance().write(
                                        Property.getString("Kinox.0") //$NON-NLS-1$
                                                + serie.getTitel()
                                                + " - " //$NON-NLS-1$
                                                + neueStaffel.toString()
                                                + " - " //$NON-NLS-1$
                                                + neueEpisode.toString());
                                
                                neu++;
                                
                                this.updateStreamLinks(neueEpisode);
                            }
                        }
                        
                        neueStaffel.setSeasonUpdate(new Date());
                    }
                }
                seitenScanner.close();
            }
        }
        
        serie.setSeriesUpdate(new Date());
        return neu;
    }
    
    @Override
    public void updateStreamLinks(Episode episode) {
        if (episode == null) {
            return;
        }
        
        for (Link link : episode.getProvider()) {
            if (link.getProviderID() == this.getProviderID()) {
                
                // Starte Verbindung
                Scanner seitenScanner = new Scanner(WebsiteGrabber.ladeSeite(
                        link.getLink(), STREAM_LIST_PATTERN_START,
                        STREAM_LIST_PATTERN_STOPP));
                
                while (seitenScanner.hasNextLine()) {
                    String zeile = seitenScanner.nextLine();
                    
                    // Filter
                    if (zeile.contains(STREAM_LIST_FILTER)) {
                        // Link zur Seite mit dem link zum Stream filtern
                        EditedStringPair res = cutString(zeile,
                                REL_LINK_PATTERN_START, REL_LINK_PATTERN_STOPP);
                        String linkZurSeite = res.getSearched().replaceAll(
                                LINK_PATTERN_DELETE, ""); //$NON-NLS-1$
                        
                        // filter = "\"Named\">";
                        // zeile = zeile.substring(zeile.indexOf(filter)
                        // + filter.length());
                        // filter = "<";
                        // String provider = zeile.substring(0,
                        // zeile.indexOf(filter));
                        // zeile = zeile.substring(zeile.indexOf(filter)
                        // + filter.length());
                        
                        // Anzahl der Mirrors filtern
                        res = cutString(res.getEdited(), null,
                                STREAM_LIST_MIRROR_FILTER);
                        
                        res = cutString(res.getEdited(),
                                STREAM_LIST_MIRROR_COUNTER_FILTER, TAB_OPEN);
                        int mirrors = Integer.parseInt(res.getSearched());
                        
                        // Erstellung des Links auslesen
                        res = cutString(res.getEdited(),
                                STREAM_LIST_DATE_FILTER, TAB_OPEN);
                        Date datum = new Date();
                        try {
                            datum = formatter.parse(res.getSearched());
                        } catch (Exception e) {
                            BugReport.recordBug(e, false);
                            BugReport.recordBug(new Exception("Link: "
                                    + linkZurSeite), false);
                        }
                        
                        // DEBUG closed
                        // System.out.println("LINK: " + linkZurSeite);
                        // System.out.println("PROVIDER: " + provider);
                        // System.out.println("MIRRORS: " + mirrors);
                        // System.out.println("DATUM: " + datum);
                        
                        // Sammle links zu Streams
                        ArrayList<String> episodeStreamLinks = this
                                .createEpisodeLinksToStreamLinks(mirrors,
                                        linkZurSeite);
                        
                        // Alle Streamlinks auslesen und eintragen
                        for (String episodeStreamLink : episodeStreamLinks) {
                            Link streamLink = this
                                    .getEpisodeStreamLink(episodeStreamLink);
                            
                            // Doppelte und offline Links vermeiden
                            if ((streamLink != null)
                                    && !episode.addStreamLink(streamLink)) {
                                
                                streamLink.setLinkCreated(datum);
                            }
                            
                        }
                    }
                    episode.setEpisodeUpdate(new Date());
                }
                
                seitenScanner.close();
                
                break;
            }
        }
        
    }
}

package de.etern.it.jserienkollar.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.ParseException;

import de.etern.it.jserienkollar.gui.StatistikPanel;
import de.etern.it.jserienkollar.implement.BugReport;

public class WebsiteGrabber {
	private static final int TIMEOUT = 1000;

	/**
	 * Liest den Inhalt der �bergebenen Seite aus.
	 *
	 * @param is
	 *            InputStream zur Seite
	 * @param startKey
	 *            String, ab dem gelesen werden soll
	 * @param stopKey
	 *            String, bis zu dem gelesen werden soll
	 * @return Inhalt der Seite
	 * @throws IOException
	 */
	private static String getContent(InputStream is, String startKey, String stopKey) throws IOException {

		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		BufferedReader reader = new BufferedReader(new InputStreamReader(is));

		StringBuilder seite = new StringBuilder();
		String line;
		boolean save = (startKey == null);
		while ((line = reader.readLine()) != null) {

			if (!save && line.contains(startKey)) {
				line = line.substring(line.indexOf(startKey) + startKey.length());
				save = true;
			}

			if (save && (stopKey != null) && line.contains(stopKey)) {
				seite.append(line.substring(0, line.indexOf(stopKey)));
				break;
			}

			if (save) {
				seite.append(line).append("\n");
			}
		}

		reader.close();

		return seite.toString();

	}

	private static boolean isConnectionError(Exception e) {
		// e.printStackTrace();
		return e.getMessage().contains("502") || e.getMessage().contains("504")
				|| e.getMessage().contains("Read timed out") || e.getMessage().contains("No route to host: connect")
				|| e.getMessage().contains("connect timed out");
	}

	public static String ladeSeite(String link) {
		return ladeSeite(link, null, null);
	}

	public static String ladeSeite(String link, String startKey, String stopKey) {
		// DEBUG closed
		// System.out.println("�ffne Link: " + link);
		try {
			URL url = new URL(link);
			URLConnection connection = url.openConnection();
			connection.setConnectTimeout(TIMEOUT);
			connection.setReadTimeout(TIMEOUT);
			connection.addRequestProperty("User-Agent",
					"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0");
			StatistikPanel.getInstance().setStatus(true);

			try {
				return getContent(connection.getInputStream(), startKey, stopKey);
			} catch (Exception e) {
				// e.printStackTrace();
				// System.err.println("Link: " + link);
				StatistikPanel.getInstance().setStatus(false);

				return "";
			}

		} catch (MalformedURLException e) {
			BugReport.recordBug(e, false);

		} catch (IOException e) {

			e.printStackTrace();
			StatistikPanel.getInstance().setStatus(false);
		}

		return "";
	}

	public static void main(String[] args) throws ParseException {
		// try {
		// Scanner seitenScanner = new Scanner(ladeSeite(
		// "http://www.movie4k.to/Alphas-online-serie-1383691.html",
		// "tablemoviesindex2", "</table>"));
		//
		// String basicLink = "";
		// while (seitenScanner.hasNextLine()) {
		// String zeile = seitenScanner.nextLine();
		//
		// // Filter
		// if (zeile.contains("tablemoviesindex2")) {
		// if (basicLink.length() == 0) {
		// EditedStringPair res = LinkProvider.cutString(
		// seitenScanner.nextLine(), "tvshows-", "'");
		// basicLink = res.getSearched().replaceAll("[0-9]", "");
		// }
		//
		// int linkNummer;
		// if (zeile.startsWith("links[")) {
		// EditedStringPair res = LinkProvider.cutString(zeile,
		// "links[", "]");
		// linkNummer = Integer.parseInt(res.getSearched());
		//
		// } else {
		// EditedStringPair res = LinkProvider.cutString(zeile,
		// "tvshows-", "-");
		// linkNummer = Integer.parseInt(res.getSearched());
		//
		// }
		//
		// String linkZuStreamLink = "http://www.movie4k.to/tvshows-"
		// + linkNummer + basicLink;
		//
		// String streamlink = ladeSeite(linkZuStreamLink,
		// "<a target=\"_blank\" href=\"", "\"");
		//
		// if (streamlink.length() == 0) {
		// Scanner linkScanner = new Scanner(ladeSeite(
		// linkZuStreamLink, "<iframe src=\"",
		// "underplayer"));
		//
		// while (linkScanner.hasNextLine()) {
		// String linkzeile = linkScanner.nextLine();
		// if (!linkScanner.hasNextLine()) {
		// EditedStringPair res = LinkProvider.cutString(
		// linkzeile, "<iframe src=\"", "\"");
		// streamlink = res.getSearched();
		// break;
		// }
		// }
		// }
		//
		// System.out.println(streamlink);
		// }
		//
		// }
		// seitenScanner.close();
		//
		// } catch (Exception e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		//

		try {
			System.out.println(ladeSeite("http://www.movie4k.to/Perception-online-serie-4997583.html"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Sendet einen POST request an eine Seite und gibt das Ergebnis zurueck.
	 *
	 * @param link
	 *            Link zur Seite
	 * @param command
	 *            zu sendendes Kommando
	 * @param value
	 *            zu sendender Wert
	 * @return Ergebnisseite
	 * @throws IOException
	 */
	public static String sendeGetRequest(String link, String command, String value) {
		return sendeGetRequest(link, command, value, null, null);
	}

	/**
	 * Sendet einen POST request an eine Seite und gibt einen gefilterten Teil
	 * des Ergebnisses zurueck.
	 *
	 * @param link
	 *            Link zur Seite
	 * @param command
	 *            zu sendendes Kommando
	 * @param value
	 *            zu sendender Wert
	 * @param startKey
	 *            String, ab dem die Seite �bertragen werden soll
	 * @param stopKey
	 *            String, bis zu dem �bertragen wird
	 * @return Ergebnisseite
	 * @throws IOException
	 */
	public static String sendeGetRequest(String link, String command, String value, String startKey, String stopKey) {
		String[] commands = new String[1];
		String[] values = new String[1];

		commands[0] = command;
		values[0] = value;

		return sendeGetRequest(link, commands, values, startKey, stopKey);

	}

	/**
	 * Sendet einen POST request an eine Seite und gibt das Ergebnis zurueck.
	 *
	 * @param link
	 *            Link zur Seite
	 * @param commands
	 *            zu sendende Kommandos
	 * @param values
	 *            zu sendende Werte (muss synchronisiert sein mit commands)
	 * @return Ergebnisseite
	 * @throws IOException
	 */
	public static String sendeGetRequest(String link, String[] commands, String[] values) {
		return sendeGetRequest(link, commands, values, null, null);
	}

	/**
	 * Sendet einen GET request an eine Seite und gibt einen gefilterten Teil
	 * des Ergebnisses zurueck.
	 *
	 * @param link
	 *            Link zur Seite
	 * @param commands
	 *            zu sendende Kommandos
	 * @param values
	 *            zu sendende Werte (muss synchronisiert sein mit commands)
	 * @param startKey
	 *            String, ab dem die Seite �bertragen werden soll
	 * @param stopKey
	 *            String, bis zu dem �bertragen wird
	 * @return Ergebnisseite
	 * @throws IOException
	 */
	public static String sendeGetRequest(String link, String[] commands, String[] values, String startKey,
			String stopKey) {
		if (commands.length != values.length) {
			return "";
		}

		String body = "";
		for (int i = 0; i < commands.length; i++) {
			if (body.length() > 0) {
				body += "&";
			}

			try {
				body += commands[i] + "=" + URLEncoder.encode(values[i], "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}

		return ladeSeite(link + "?" + body, startKey, stopKey);
	}

	/**
	 * Sendet einen POST request an eine Seite und gibt das Ergebnis zurueck.
	 *
	 * @param link
	 *            Link zur Seite
	 * @param command
	 *            zu sendendes Kommando
	 * @param value
	 *            zu sendender Wert
	 * @return Ergebnisseite
	 * @throws IOException
	 */
	public static String sendePostRequest(String link, String command, String value) {
		return sendePostRequest(link, command, value, null, null);
	}

	/**
	 * Sendet einen POST request an eine Seite und gibt einen gefilterten Teil
	 * des Ergebnisses zurueck.
	 *
	 * @param link
	 *            Link zur Seite
	 * @param command
	 *            zu sendendes Kommando
	 * @param value
	 *            zu sendender Wert
	 * @param startKey
	 *            String, ab dem die Seite �bertragen werden soll
	 * @param stopKey
	 *            String, bis zu dem �bertragen wird
	 * @return Ergebnisseite
	 * @throws IOException
	 */
	public static String sendePostRequest(String link, String command, String value, String startKey, String stopKey) {
		String[] commands = new String[1];
		String[] values = new String[1];

		commands[0] = command;
		values[0] = value;

		return sendePostRequest(link, commands, values, startKey, stopKey);

	}

	/**
	 * Sendet einen POST request an eine Seite und gibt das Ergebnis zurueck.
	 *
	 * @param link
	 *            Link zur Seite
	 * @param commands
	 *            zu sendende Kommandos
	 * @param values
	 *            zu sendende Werte (muss synchronisiert sein mit commands)
	 * @return Ergebnisseite
	 * @throws IOException
	 */
	public static String sendePostRequest(String link, String[] commands, String[] values) {
		return sendePostRequest(link, commands, values, null, null);
	}

	/**
	 * Sendet einen POST request an eine Seite und gibt einen gefilterten Teil
	 * des Ergebnisses zurueck.
	 *
	 * @param link
	 *            Link zur Seite
	 * @param commands
	 *            zu sendende Kommandos
	 * @param values
	 *            zu sendende Werte (muss synchronisiert sein mit commands)
	 * @param startKey
	 *            String, ab dem die Seite �bertragen werden soll
	 * @param stopKey
	 *            String, bis zu dem �bertragen wird
	 * @return Ergebnisseite
	 * @throws IOException
	 */
	public static String sendePostRequest(String link, String[] commands, String[] values, String startKey,
			String stopKey) {
		if (commands.length != values.length) {
			return "";
		}

		// DEBUG closed
		// System.out.println("�ffne link: " + link);

		try {
			String body = "";
			for (int i = 0; i < commands.length; i++) {
				if (body.length() > 0) {
					body += "&";
				}

				body += commands[i] + "=" + URLEncoder.encode(values[i], "UTF-8");
			}

			URL url = new URL(link);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setConnectTimeout(TIMEOUT);
			connection.setReadTimeout(TIMEOUT);
			connection.setRequestMethod("POST");
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setUseCaches(false);
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			connection.setRequestProperty("Content-Length", String.valueOf(body.length()));

			OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
			writer.write(body);
			writer.flush();

			StatistikPanel.getInstance().setStatus(true);

			String seite = getContent(connection.getInputStream(), startKey, stopKey);

			writer.close();

			return seite;

		} catch (MalformedURLException | UnsupportedEncodingException | ProtocolException e) {
			BugReport.recordBug(e, false);

		} catch (IOException e) {
			if (!isConnectionError(e)) {
				BugReport.recordBug(e, false);
			} else {
				StatistikPanel.getInstance().setStatus(false);
			}
		}
		return "";
	}
}

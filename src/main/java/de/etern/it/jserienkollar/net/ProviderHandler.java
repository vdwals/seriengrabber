/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.etern.it.jserienkollar.net;

import java.io.File;
import java.nio.file.FileSystems;
import java.util.ArrayList;

import de.etern.it.jserienkollar.datenbank.importobjekte.Link;
import de.etern.it.jserienkollar.jaxb.StreamProviderJ;
import de.etern.it.jserienkollar.implement.Console;
import de.etern.it.properties.Property;
import de.etern.it.jserienkollar.net.linkprovider.Kinox;
import de.etern.it.jserienkollar.net.linkprovider.Movie4k;

import javax.xml.bind.JAXB;

/**
 * Klasse zum initialisieren aller Provider.
 *
 * @author Dennis
 *
 */
public class ProviderHandler {
    private static final ProviderHandler INSTANCE = new ProviderHandler();
    private static final String PROP_VERZEICHNIS = "streamProviderFolder";
    private static final String DEFAULT_VERZECHNIS = "streamprovider";
    
    /**
     * Gibt die einzige Instanz zurueck
     *
     * @return Instanz
     */
    public static ProviderHandler getInstance() {
        return INSTANCE;
    }
    
    private final ArrayList<LinkProvider>   linkProvider;
    private final ArrayList<de.etern.it.jserienkollar.net.StreamProvider> streamProvider;
    
    /** Standardkonstruktor **/
    private ProviderHandler() {
        int idIndex = 1;
        this.linkProvider = new ArrayList<>();
        this.linkProvider.add(new Kinox(idIndex++));
        this.linkProvider.add(new Movie4k(idIndex++));

        this.streamProvider = new ArrayList<>();

        String verzeichnis = Property.getConfig(PROP_VERZEICHNIS);

        if (verzeichnis == null || verzeichnis.equals("!" + PROP_VERZEICHNIS + "!")) {
            verzeichnis = new File("tmp").getAbsolutePath(); //$NON-NLS-1$
            verzeichnis = verzeichnis.substring(0,
                    verzeichnis.length() - "tmp".length()) + DEFAULT_VERZECHNIS + FileSystems.getDefault() //$NON-NLS-1$
                    .getSeparator();
            new File(verzeichnis).mkdirs();

            Property.setConfig(PROP_VERZEICHNIS, verzeichnis);

        } else {
            File providerVerzeichnis = new File(verzeichnis);

            if (providerVerzeichnis.isDirectory()) {
                for (File providerFile : providerVerzeichnis.listFiles()) {
                    StreamProviderJ unmarshal = null;
                    try {
                        unmarshal = JAXB.unmarshal(providerFile, StreamProviderJ.class);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (unmarshal != null) {
                        this.streamProvider.add(unmarshal.getStreamProvider());
                    }
                }

            }
        }
    }
    
    /**
     * @return the linkProvider
     */
    public ArrayList<LinkProvider> getLinkProvider() {
        return this.linkProvider;
    }
    
    /**
     * @return the streamProvider
     */
    public ArrayList<de.etern.it.jserienkollar.net.StreamProvider> getStreamProvider() {
        return this.streamProvider;
    }
    
    /**
     * Identifiziert den Linkprovider und gibt ihn zurueck.
     *
     * @param providerID
     *            ID
     * @return Provider
     */
    public LinkProvider identifyLinkProvider(int providerID) {
        return this.linkProvider.get(providerID - 1);
    }
    
    /**
     * Identifiziert den Streamprovider und gibt ihn zurueck.
     *
     * @param providerID
     *            ID
     * @return Provider
     */
    public de.etern.it.jserienkollar.net.StreamProvider identifyStreamProvider(int providerID) {
        if (providerID == 0)
            return null;
        
        return this.streamProvider.get(providerID - 1);
    }
    
    /**
     * Identifiziert den Streamprovider anhand des uebergebenen Links.
     * Funktioniert nur wenn Provider nicht schon gesetzt, ansonsten kommt
     * selbiger im Link nicht mehr vor.
     *
     * @param link
     *            Link
     * @return Streamprovider
     */
    public de.etern.it.jserienkollar.net.StreamProvider identifyStreamProvider(Link link) {
        if (link.getProviderID() != 0) {
            return this.identifyStreamProvider(link.getProviderID());
        }
        
        de.etern.it.jserienkollar.net.StreamProvider p = this.identifyStreamProvider(link.getLink());
        if (p != null) {
            link.setProviderID(p.getProviderID());
        }
        return p;
    }
    
    /**
     * Identifiziert den Streamprovider und gibt ihn zurueck.
     *
     * @return Provider
     */
    public de.etern.it.jserienkollar.net.StreamProvider identifyStreamProvider(String providerKey) {
        for (de.etern.it.jserienkollar.net.StreamProvider provider : this.streamProvider) {
            if (providerKey.contains(provider.getName())) {
                return provider;
            }
        }
        
        if (Property.getBoolean("log_unknown_hosts"))
            Console.writeLog(providerKey + System.lineSeparator());
        
        return null;
    }
    
}

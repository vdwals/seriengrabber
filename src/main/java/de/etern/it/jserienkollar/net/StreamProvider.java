/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.etern.it.jserienkollar.net;

import de.etern.it.jserienkollar.datenbank.importobjekte.Link;

/**
 * Schnittstelle zu verschiedenen Streamanbietern.
 *
 * @author Dennis van der Wals
 */
public abstract class StreamProvider extends Provider {
    private int priority;

    public StreamProvider(String name, String basicLink, int id, int priority) {
        super(name, basicLink, id);
        this.priority = priority;
    }

    @Deprecated
    public StreamProvider(String name, String basicLink, int id) {
        super(name, basicLink, id);
        this.priority = 0;
    }

    /**
     * Prüft ob hinter einem Link ein Stream steht. Wandelt den Link um, wenn er
     * in eingebetteter Form vorliegt.
     *
     * @param link zu testender Link
     * @return Wahrheitswert, ob Stream vorhanden.
     */
    public abstract boolean checkLink(Link link);

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }
}

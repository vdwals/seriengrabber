/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.etern.it.jserienkollar.implement;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Scanner;
import java.util.regex.Pattern;
import de.etern.it.properties.Property;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

/**
 * Klasse zum Erstellen eines Feedbacks, welches zum Server �bermittelt wird.
 *
 * @author Dennis van der Wals
 *
 */
public class FeedBack extends JFrame implements ActionListener {
    private final String          KATEGORIEN[]     = {
            Property.getString("FeedBack.0"), Property.getString("FeedBack.1"), //$NON-NLS-1$ //$NON-NLS-2$
            Property.getString("FeedBack.2"), Property.getString("FeedBack.3") };     //$NON-NLS-1$ //$NON-NLS-2$
    private static final FeedBack INSTANCE         = new FeedBack();

    // Komponenten
    private JComboBox<String>     kategorie;
    private JTextArea             message;
    private JScrollPane           messageBox;
    private JTextField            name, mail;
    private JButton               send, abort;

    /**
     * Gibt die einzige Instanz zurueck
     *
     * @return FeedBack
     */
    public static FeedBack getInstance() {
        return INSTANCE;
    }

    /** Standardkonstruktor **/
    private FeedBack() {

        super(Property.getString("FeedBack.5")); //$NON-NLS-1$

        // Layoutmanager setzen
        this.setLayout(new BorderLayout(5, 5));
        JPanel top = new JPanel(new GridLayout(2, 1, 5, 5));
        JPanel firstLine = new JPanel(new GridLayout(1, 2, 5, 5));
        JPanel name = new JPanel(new BorderLayout(5, 5));
        JPanel mail = new JPanel(new BorderLayout(5, 5));
        JPanel bottom = new JPanel(new BorderLayout());
        JPanel buttons = new JPanel(new GridLayout(1, 2, 5, 5));

        // Panels fuellen
        name.add(
                new JLabel(Property.getString("FeedBack.6")), BorderLayout.WEST); //$NON-NLS-1$
        name.add(this.getNamen(), BorderLayout.CENTER);

        firstLine.add(name);
        firstLine.add(this.getKategorie());

        mail.add(
                new JLabel(Property.getString("FeedBack.7")), BorderLayout.WEST); //$NON-NLS-1$
        mail.add(this.getMail(), BorderLayout.CENTER);

        top.add(firstLine);
        top.add(mail);

        buttons.add(this.getSend());
        buttons.add(this.getAbort());

        bottom.add(buttons, BorderLayout.EAST);

        this.add(top, BorderLayout.NORTH);
        this.add(this.getMessageBox(), BorderLayout.CENTER);
        this.add(bottom, BorderLayout.SOUTH);

        this.pack();
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {

        if (arg0.getSource() == this.getAbort()) {
            // Fenster schlie�en
            this.setVisible(false);

        } else if (arg0.getSource() == this.getSend()) {
            // E-Mail validieren
            if (Pattern.matches(Meta.EMAIL_PATTERN, this.getMail().getText())) {
                this.sendFeedBack();
                this.setVisible(false);

            } else {
                JOptionPane.showMessageDialog(
                        null,
                        Property.getString("FeedBack.22"), //$NON-NLS-1$
                        Property.getString("FeedBack.5"),
                        JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    private JButton getAbort() {
        if (this.abort == null) {
            this.abort = new JButton(Property.getString("FeedBack.24")); //$NON-NLS-1$
            this.abort.addActionListener(this);
        }
        return this.abort;
    }

    /**
     * Gibt die Kategorieauswahl zurueck.
     *
     * @return Kategorieauswahl
     */
    private JComboBox<String> getKategorie() {
        if (this.kategorie == null) {
            this.kategorie = new JComboBox<>(this.KATEGORIEN);
        }
        return this.kategorie;
    }

    private JTextField getMail() {
        if (this.mail == null) {
            this.mail = new JTextField(35);
        }
        return this.mail;
    }

    /**
     * Gibt das Texteingabefeld zurueck.
     *
     * @return Texteingabefeld
     */
    private JTextArea getMessage() {
        if (this.message == null) {
            this.message = new JTextArea(10, 50);
            this.message.setLineWrap(true);
            this.message.setWrapStyleWord(true);
        }
        return this.message;
    }

    /**
     * Gibt das Texteingabefeld mit Scrollbalken zurueck.
     *
     * @return Texteingabefeld mit Scrollbalken
     */
    private JScrollPane getMessageBox() {
        if (this.messageBox == null) {
            this.messageBox = new JScrollPane(this.getMessage());
            this.messageBox
            .setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        }
        return this.messageBox;
    }

    private JTextField getNamen() {
        if (this.name == null) {
            this.name = new JTextField(20);
        }
        return this.name;
    }

    private JButton getSend() {
        if (this.send == null) {
            this.send = new JButton(Property.getString("FeedBack.25")); //$NON-NLS-1$
            this.send.addActionListener(this);
        }
        return this.send;
    }

    /**
     * Uebertraegt das FeedBack an den Server
     */
    private void sendFeedBack() {
        StringBuilder nachricht = new StringBuilder();

        nachricht.append("************Neuer Eintrag************\n"); //$NON-NLS-1$
        nachricht.append(new SimpleDateFormat().format(Calendar.getInstance().getTime())).append("\n"); //$NON-NLS-1$
        nachricht.append("Von: ").append(this.getNamen().getText()).append("\n"); //$NON-NLS-1$ //$NON-NLS-2$
        nachricht.append("Mail: ").append(this.getMail().getText()).append("\n"); //$NON-NLS-1$ //$NON-NLS-2$

        Scanner scan = new Scanner(this.getMessage().getText());

        // Nachricht in kleinere Zeilen zerlegen, falls noetig
        while (scan.hasNextLine()) {
            String zeile = scan.nextLine();

            while (zeile.length() > Meta.UPLOAD_LIMIT) {
                nachricht.append(zeile.substring(0, Meta.UPLOAD_LIMIT)).append("\n"); //$NON-NLS-1$
                zeile = zeile.substring(Meta.UPLOAD_LIMIT);
            }
            nachricht.append(zeile);
        }
        scan.close();

        scan = new Scanner(nachricht.toString());

        String show = Property.getString("FeedBack.15"); //$NON-NLS-1$

        // Zeile fuer Zeile versenden
        while (scan.hasNextLine()) {
            try {
                new URL(
                        Meta.FDB_ADD
                        + "?app=" //$NON-NLS-1$
                        + Meta.APP_NAME
                        + "&cat=" //$NON-NLS-1$
                        + Meta.KAT_SHORT[this.getKategorie()
                                         .getSelectedIndex()] + "&text=" //$NON-NLS-1$
                                         + URLEncoder.encode(scan.nextLine(), "UTF-8")) //$NON-NLS-1$
                .openStream();
            } catch (Exception e) {
                e.printStackTrace();
                show = Property.getString("FeedBack.20"); //$NON-NLS-1$
            }
        }

        scan.close();

        JOptionPane.showMessageDialog(null, show,
                Property.getString("FeedBack.5"),
                JOptionPane.INFORMATION_MESSAGE);
    }
}

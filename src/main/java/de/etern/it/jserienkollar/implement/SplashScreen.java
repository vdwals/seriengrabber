/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.etern.it.jserienkollar.implement;

import de.etern.it.properties.Property;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;

/**
 * Erzeugt einen Splashscreen.
 *
 * @author Dennis van der Wals
 */
public class SplashScreen extends JPanel {
    // private static final int SIZE = 15;
    private Image bild;
    private JWindow window;

    private final static SplashScreen INSTANCE = new SplashScreen();

    public static SplashScreen getInstance() {
        return INSTANCE;
    }

    /**
     * Erzeugt den SplashScreen.
     */
    public SplashScreen() {
        super();

        // Bild laden
        this.bild = null;
        try {
            // Bild in .jar
            this.bild = ImageIO.read(ClassLoader.getSystemResource("de/etern/it/jserienkollar/implement/splash.xrt"));//$NON-NLS-1$

            // Bild nicht in .jar
            // this.bild = ImageIO.read(new File("splash.xrt"));
        } catch (IOException e) {
            BugReport.recordBug(e, false);
        }

        // Fenstergroesse setzen
        if (this.bild != null) {
            this.setPreferredSize(new Dimension(this.bild.getWidth(this), this.bild.getHeight(this)));

            this.setLayout(null);

            // Neues Fenster erzeugen
            this.window = new JWindow();

            // Konsole vorbereiten
            JScrollPane s = new JScrollPane(Console.getInstance().getOutput());
            s.setPreferredSize(new Dimension(this.bild.getWidth(this), 100));

            // Bild und Console zusammenpacken
            JPanel p = new JPanel(new BorderLayout());
            p.setBackground(Color.WHITE);
            p.add(this, BorderLayout.CENTER);
            p.add(s, BorderLayout.SOUTH);

            // Anzeigen
            this.window.add(p);
            this.window.setBackground(Color.WHITE);
            this.window.pack();
            this.window.toFront();
            this.window.setLocation((Toolkit.getDefaultToolkit().getScreenSize().width - this.window.getWidth()) / 2, (Toolkit.getDefaultToolkit().getScreenSize().height - this.window.getHeight()) / 2);
        }
    }

    public void hideSplash() {
        Thread T = new Thread(() -> {
            if (SplashScreen.this.window != null) {
                SplashScreen.this.window.setVisible(false);
            }
        });
        T.start();
    }

    @Override
    public void paint(Graphics g) {
        // Vollbild
        g.drawImage(this.bild, 0, 0, this.getWidth(), this.getHeight(), this);

        // Kreuz in die Ecke (zum schlie�en)
        g.setColor(Color.WHITE);
        g.setFont(new Font(g.getFont().getFontName(), g.getFont().getStyle(), 22));

        g.drawString("Version: " + Property.getConfig("version"), 10, this.getHeight() - 40);
        g.drawString("Autor:    Dennis van der Wals", 10, this.getHeight() - 15);
    }

    /**
     * Erzeugt und zeigt einen Start-SplashScreen.
     */
    public void showSplash() {
        Thread T = new Thread(() -> {
            if (SplashScreen.this.window != null) {
                SplashScreen.this.window.setVisible(true);
            }
        });
        T.start();
    }
}

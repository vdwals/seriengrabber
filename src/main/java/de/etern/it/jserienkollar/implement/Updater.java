package de.etern.it.jserienkollar.implement;

/* ********************************************************************
 * AutoUpdate *
 * Copyright (C) 2010-2017 Dionysoft - http://www.dionysoft.kilu.de *
 * *
 * This library is free software; you can redistribute it and/or *
 * modify it under the terms of the GNU Lesser General Public *
 * License as published by the Free Software Foundation; either *
 * version 2.1 of the License, or (at your option) any later version. *
 * *
 * This library is distributed in the hope that it will be useful, *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU *
 * Lesser General Public License for more details. *
 * *
 * You should have received a copy of the GNU Lesser General Public *
 * License along with this library; if not, write to the *
 * Free Software Foundation, Inc., *
 * 51 Franklin St, Fifth Floor, *
 * Boston, MA 02110-1301 USA *
 * *
 * Or get it online: *
 * http://www.gnu.org/copyleft/lesser.html *
 * ********************************************************************
 */

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Scanner;
import de.etern.it.properties.Property;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

/**
 * Klasse zum Updaten des Programms.
 *
 * @author Dennis van der Wals
 *
 */
public class Updater {
    private final Version      version;
    private final String       source;
    private final String name;
    private JProgressBar download;
    int                  i;
    
    public Updater() {
        this.version = new Version(Property.getConfig("version")); //$NON-NLS-1$
        this.source = Meta.SERVER + Meta.SUB_ADD + "/dateien/"; //$NON-NLS-1$
        this.name = Meta.APP_NAME + ".jar"; //$NON-NLS-1$
    }
    
    /**
     * Standardkonstruktor, der sich alle Informationen aus der server-Klasse
     * holt.
     */
    public Updater(Version version) {
        this.version = version;
        this.source = Meta.SERVER + Meta.SUB_ADD + "/dateien/"; //$NON-NLS-1$
        this.name = Meta.APP_NAME + ".jar"; //$NON-NLS-1$
    }
    
    /**
     * Standardkonstruktor.
     *
     * @param version
     *            Softwareversion
     * @param source
     *            Internetaddresse bsp: "http://dionysios.di.ohost.de/test/"
     *            oder Unterverzeichnis des Standardadresse: "test/"
     * @param name
     *            Dateiname bsp: "Reaktor" ohne ".jar"
     */
    public Updater(Version version, String source, String name) {
        this.version = version;
        if (source.contains("http://")) { //$NON-NLS-1$
            this.source = source;
        } else {
            this.source = Meta.SERVER + source;
        }
        this.name = name + ".jar"; //$NON-NLS-1$
    }
    
    /**
     * Laedt eine Datei von der angegebenen URL auf die angegebene Datei.
     *
     * @param url
     *            Quelldatei
     * @param os
     *            Zieldatei
     * @throws IllegalStateException
     * @throws MalformedURLException
     * @throws ProtocolException
     * @throws IOException
     */
    public void downloadFile(URL url, OutputStream os)
            throws IllegalStateException, IOException {
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET"); //$NON-NLS-1$
        conn.connect();
        int responseCode = conn.getResponseCode();
        if (responseCode == HttpURLConnection.HTTP_OK) {
            byte tmp_buffer[] = new byte[4096];
            InputStream is = conn.getInputStream();
            int n;
            while ((n = is.read(tmp_buffer)) > 0) {
                os.write(tmp_buffer, 0, n);
                os.flush();
                this.download.setValue(this.download.getValue() + n);
            }
        } else {
            throw new IllegalStateException("HTTP response: " + responseCode); //$NON-NLS-1$
        }
    }
    
    /**
     * Sucht nach Updates auf dem Server und laedt diese herunter.
     *
     * @throws IOException
     */
    public void getUpdate() throws IOException {
        URL url = new URL(this.source + Meta.VERSIONSDATEI);
        Scanner scan = new Scanner(url.openStream());
        // double v = Double.parseDouble(scan.next());
        Version v = new Version(scan.nextLine());
        
        // Update laden
        // if (this.version < v) {
        if (this.version.compareTo(v) == -1) {
            
            // Updatefrage
            String[] options = {
                    Property.getString("Updater.8"), Property.getString("Updater.9") }; //$NON-NLS-1$ //$NON-NLS-2$
            int state = JOptionPane
                    .showOptionDialog(
                            null,
                            Property.getString("Updater.10"), //$NON-NLS-1$
                            Property.getString("Updater.11"), JOptionPane.OK_CANCEL_OPTION, //$NON-NLS-1$
                            JOptionPane.QUESTION_MESSAGE, null, options,
                            options[0]);
            
            if (state == 0) {
                
                // Fortschrittsbalken erzeugen
                this.download = new JProgressBar(0, new URL(this.source
                        + this.name).openConnection().getContentLength());
                this.download.setStringPainted(true);
                
                // Download starten
                new Thread(() -> {
                    try {
                        Updater.this.downloadFile(new URL(
                                Updater.this.source + Updater.this.name),
                                new FileOutputStream(Updater.this.name));
                    } catch (Exception e) {
                        de.etern.it.bugReport.BugReport.recordBug(e, false);
                    }
                }).start();
                
                // Fortschritt anzeigen
                String[] option = { Property.getString("Updater.12") }; //$NON-NLS-1$
                while (this.download.getValue() != this.download.getMaximum()) {
                    JOptionPane.showOptionDialog(
                            null,
                            this.readVersionHistory(scan),
                            Property.getString("Updater.13"), //$NON-NLS-1$
                            JOptionPane.OK_OPTION, JOptionPane.PLAIN_MESSAGE,
                            null, option, option[0]);
                }
                
                // Neustart der Anwendung
                if (!System.getProperty("os name").contains("linux")) { //$NON-NLS-1$ //$NON-NLS-2$
                    Runtime.getRuntime().exec("javaw -jar " //$NON-NLS-1$
                            + new File(this.name).getAbsolutePath());
                    System.out.println("nich linux"); //$NON-NLS-1$
                } else {
                    Runtime.getRuntime().exec("java -jar " //$NON-NLS-1$
                            + new File(this.name).getAbsolutePath());
                    System.out.println("linux"); //$NON-NLS-1$
                }
                
                // Programm beenden
                System.exit(0);
            }
        }
    }
    
    /**
     * Liest die letzte Versionshistory aus.
     *
     * @return JPanel fuer Updatefenster
     * @throws IllegalStateException
     * @throws MalformedURLException
     * @throws ProtocolException
     * @throws FileNotFoundException
     * @throws IOException
     */
    private JPanel readVersionHistory(Scanner scan)
            throws IllegalStateException {
        
        // Die Versionshistory wird gelesen
        String s = ""; //$NON-NLS-1$
        scan.nextLine();
        while (scan.hasNextLine()) {
            s += scan.nextLine() + "\n"; //$NON-NLS-1$
        }
        
        // Ein scrollbares Textfeld wird damit gef�llt
        JTextArea history = new JTextArea();
        history.setLineWrap(true);
        history.setWrapStyleWord(true);
        history.setText(s);
        
        JScrollPane scrollPane = new JScrollPane(history);
        scrollPane
                .setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        
        // JLabel info = new JLabel(
        // "<html>Ein Update wurde geladen.<br>Bitte starten Sie das Programm neu. </html>");
        
        JLabel info = new JLabel(Property.getString("Updater.22")); //$NON-NLS-1$
        
        // Komponenten in Container packen
        JPanel container = new JPanel();
        container.add(info);
        container.add(scrollPane);
        container.add(this.download);
        
        // Layout anlegen
        GridBagLayout gbl = new GridBagLayout();
        this.setGridBagConstraints(gbl, this.download, 0, 0.10,
                GridBagConstraints.HORIZONTAL);
        this.setGridBagConstraints(gbl, info, 1, 0.10,
                GridBagConstraints.HORIZONTAL);
        this.setGridBagConstraints(gbl, scrollPane, 2, 0.80,
                GridBagConstraints.BOTH);
        container.setLayout(gbl);
        container.setPreferredSize(new Dimension(300, 200));
        
        return container;
    }
    
    /**
     * Standardfunktion fuer das GridbagLayout.
     *
     * @param gbl
     *            Gridbaglayout
     * @param c
     *            Compnent
     * @param gridy
     *            Zeile
     * @param weighty
     *            Zeilengewicht
     * @param fill
     *            Verbreiterung
     * @param ipadx
     *            mindestbreite
     */
    private void setGridBagConstraints(GridBagLayout gbl, Component c,
            int gridy, double weighty, int fill) {
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridy = gridy;
        gbc.gridwidth = 1;
        gbc.weightx = 1.0;
        gbc.weighty = weighty;
        gbc.fill = fill;
        gbl.setConstraints(c, gbc);
    }
    
}

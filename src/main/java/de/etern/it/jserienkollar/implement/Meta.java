/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.etern.it.jserienkollar.implement;

/**
 * Sammelklasse fuer Server- und Verzeichnisbezogene Daten.
 *
 * @author Dennis van der Wals
 *
 */
public class Meta {
    public final static String SERVER        = "http://dvdw.dlinkddns.com/apps/";
    public final static String VERSIONSDATEI = "version";
    public final static String SUB_ADD       = "kettenreaktion";
    public final static String BUG_ADD       = SERVER + "bugreport/catch.php";
    public final static String APP_NAME      = "Reaktor";
    public final static String FDB_ADD       = SERVER + "feedback/catch.php";
    public final static int    UPLOAD_LIMIT  = 50;
    /**
     * Reihenfolge mit Feedbackkategorieauswahl synchron halten!
     */
    public final static String KAT_SHORT[]   = { "k", "l", "v", "f" };
    public static final String EMAIL_PATTERN = "^([a-zA-Z0-9])+([\\.a-zA-Z0-9_-])*@([a-zA-Z0-9_-])+(\\.[a-zA-Z0-9_-]+)+";
}

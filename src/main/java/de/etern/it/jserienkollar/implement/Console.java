/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.etern.it.jserienkollar.implement;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import de.etern.it.properties.Property;

import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 * Abstrakte Klasse zur Konsolenausgabe in einer graphischen Oberflaeche.
 *
 * @author Dennis van der Wals
 *
 */
public class Console extends JPanel {
    /** Logdatei */
    private static final File       log              = new File("log.txt");  //$NON-NLS-1$

    private static final Console    INSTANCE         = new Console();
    
    /**
     * Gibt die einzige Instanz der Console zurueck.
     *
     * @return Console
     */
    public static Console getInstance() {
        return INSTANCE;
    }
    
    /**
     * Schreibt den String s in die Logdatei.
     *
     * @param s
     *            String
     */
    public static void writeLog(String s) {
        try {
            
            if(log.createNewFile()) {
                FileWriter fw = new FileWriter(log, true);
                fw.append(s);
                fw.close();
            }
            
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        
    }
    
    private boolean   logging;
    
    /* GUI-Komponente */
    private final JTextArea output;
    
    /**
     * Erzeugt eine neue Console mit leerem Textfeld
     */
    private Console() {
        this.output = new JTextArea();
        this.output.setLineWrap(true);
        this.output.setEditable(false);
        this.logging = Property.getBoolean("logging");
    }
    
    /**
     * @return the output
     */
    public JTextArea getOutput() {
        return this.output;
    }
    
    /**
     * @return the logging
     */
    public boolean isLogging() {
        return this.logging;
    }
    
    /**
     * @param logging
     *            the logging to set
     */
    public void setLogging(boolean logging) {
        this.logging = logging;
    }
    
    /**
     * Schreibt einen String in die Ausgabe der Oberflaeche.
     *
     * @param ausgabe
     *            Auszugebender Text
     */
    public void write(String ausgabe) {
        // Erstellt den Ausgabestring.
        String s = "(" //$NON-NLS-1$
                + new SimpleDateFormat("dd.MM.yyyy HH:mm:ss") //$NON-NLS-1$
                        .format(new Date()) + "): " + ausgabe //$NON-NLS-1$
                + System.lineSeparator();
        
        // Schreibt ihn in die Ausgabe
        this.output.append(s);
        
        // Ausgabe auf normale Konsole
        System.out.print(s);
        
        // Schreibt ihn in die Logdatei
        if (this.logging) {
            writeLog(s);
        }
        
        // Setzt den Textzeiger auf die neue Zeile
        this.output.setCaretPosition(this.output.getText().length());
    }
    
}

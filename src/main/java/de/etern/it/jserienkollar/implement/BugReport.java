/* ********************************************************************
 * BugReport *
 * Copyright (C) 2010-2017 Dionysoft - http://www.dionysoft.kilu.de *
 * *
 * This library is free software; you can redistribute it and/or *
 * modify it under the terms of the GNU Lesser General Public *
 * License as published by the Free Software Foundation; either *
 * version 2.1 of the License, or (at your option) any later version. *
 * *
 * This library is distributed in the hope that it will be useful, *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU *
 * Lesser General Public License for more details. *
 * *
 * You should have received a copy of the GNU Lesser General Public *
 * License along with this library; if not, write to the *
 * Free Software Foundation, Inc., *
 * 51 Franklin St, Fifth Floor, *
 * Boston, MA 02110-1301 USA *
 * *
 * Or get it online: *
 * http://www.gnu.org/copyleft/lesser.html *
 * ********************************************************************
 */
package de.etern.it.jserienkollar.implement;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Scanner;
import de.etern.it.properties.Property;

/**
 * Klasse zum Aufnehmen und Abspeichern von Fehlermeldungen.
 *
 * @author Dennis van der Wals
 *
 */
public class BugReport {
    private static final File report = new File("stack.rep"); //$NON-NLS-1$
                                                        
    /**
     * Speichert alle relevanten Daten zu einer Exception und versucht diese an
     * den Server zu uebermitteln.
     *
     * @param e
     *            Exception
     */
    public static void recordBug(Exception e, boolean simple) {
        // e.printStackTrace();
        
        if (!Property.getBoolean("bug_tracking"))
            return;
        
        // Fehlermeldung erzeugen
        StringBuilder error = new StringBuilder();
        error.append("**************Neuer Fehler**************\n"); //$NON-NLS-1$
        error.append(new SimpleDateFormat().format(Calendar.getInstance().getTime())).append("\n"); //$NON-NLS-1$
        error.append("Fehler: \t").append(e.toString()).append("\n"); //$NON-NLS-1$ //$NON-NLS-2$
        error.append("Nachricht: \t").append(e.getMessage()).append("\n"); //$NON-NLS-1$ //$NON-NLS-2$
        error.append("Ausloeser: \t").append(e.getCause()).append("\n"); //$NON-NLS-1$ //$NON-NLS-2$
        
        if (!simple) {
            error.append("-------Details-------\n"); //$NON-NLS-1$
            
            // Stack hinzufuegen
            int counter = 1;
            for (StackTraceElement element : e.getStackTrace()) {
                error.append("Element: \t").append(counter++).append("\n"); //$NON-NLS-1$ //$NON-NLS-2$
                error.append("Datei: \t\t").append(element.getFileName()).append("\n"); //$NON-NLS-1$ //$NON-NLS-2$
                error.append("Klasse: \t").append(element.getClassName()).append("\n"); //$NON-NLS-1$ //$NON-NLS-2$
                error.append("Methode: \t").append(element.getMethodName()).append("\n"); //$NON-NLS-1$ //$NON-NLS-2$
                error.append("Zeile: \t\t").append(element.getLineNumber()).append("\n"); //$NON-NLS-1$ //$NON-NLS-2$
                error.append("-------\n"); //$NON-NLS-1$
            }
        }
        
        try {
            
            // Fehlermeldung hochladen
            upload(new Scanner(error.toString()));
            
            /*
             * Wenn eine Fehlermeldungsdatei existiert und Fehler beinhaltet,
             * lade diese auch hoch
             */
            if (!report.createNewFile() && (report.length() > 0)) {
                upload();
            }
            
        } catch (Exception e2) {
            
            // Wenn Server nicht erreichbar, Fehler in Datei speichern
            try {
                
                if (report.createNewFile()) {
                    FileWriter fw = new FileWriter(report, true);
                    fw.append(error.toString());
                    fw.close();
                }
                
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        
    }
    
    /**
     * Laedt die Fehlerdatei hoch
     *
     * @return erfolgreicher Upload, oder nicht
     */
    private static boolean upload() {
        try {
            // Lade Datei hoch
            upload(new Scanner(report));
            
            // Loesche den Inhalt der Datei
            FileWriter fw = new FileWriter(report, false);
            fw.write(""); //$NON-NLS-1$
            fw.close();

            // Gib Erfolgsmeldung zurueck.
            return report.delete();
            
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
    /**
     * Uebermittelt den Dateiinhalt an den Server
     *
     * @throws IOException
     * @throws UnsupportedEncodingException
     * @throws MalformedURLException
     */
    private static void upload(Scanner scan) throws IOException {
        
        // Zeile fuer Zeile versenden
        while (scan.hasNextLine()) {
            new URL(Meta.BUG_ADD + "?app=" + Meta.APP_NAME + "&text=" //$NON-NLS-1$ //$NON-NLS-2$
                    + URLEncoder.encode(scan.nextLine(), "UTF-8")).openStream(); //$NON-NLS-1$
        }
        scan.close();
    }
}

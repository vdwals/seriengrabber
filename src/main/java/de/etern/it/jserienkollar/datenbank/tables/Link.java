/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.etern.it.jserienkollar.datenbank.tables;

import java.time.LocalDateTime;
import java.util.Date;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import com.sun.istack.internal.NotNull;

import de.etern.it.jserienkollar.datenbank.LinkType;

/**
 * Created by Dennis on 23.05.2016.
 */
public class Link extends Table implements Comparable<Link> {
	public static final String ID = "id_link", TYPE = "type", HTTP = "http://", ID_PARENT = "id_episode",
			ID_PROVIDER = "id_provider", OFFLINE = "offline", LINK = "link";

	/**
	 * Bereinigt den Link; entfernt alle HTML-Zeichen
	 *
	 * @param link
	 *            zu bereiniginder Link
	 * @return bereinigder Link
	 */
	public static String clearLink(String link) {
		return link.replaceAll("&amp;", "&");
	}

	/** Standardkonstruktor **/
	public Link() {
	}

	/**
	 * Konstruktor.
	 *
	 * @param link
	 *            Zu speichernder Link.
	 * @param provider
	 *            Anbieter
	 * @param streamLink
	 *            Legt fest, ob es ein Stream oder ein Provider Link ist.
	 */
	public Link(String link, int provider, boolean streamLink) {
		this.setProviderID(provider);
		if (streamLink) {
			this.setType(LinkType.STREAM);
		} else {
			this.setType(LinkType.PROVIDER);
		}
		this.setLink(link);
	}

	public void setType(LinkType type) {
		this.setString(TYPE, type.toString());
	}

	public LinkType getType() {
		String type = this.getString(TYPE);
		switch (type) {
		case "stream":
			return LinkType.STREAM;
		case "provider":
			return LinkType.PROVIDER;
		default:
			break;
		}
		return null;
	}

	/**
	 * Konstruktor.
	 *
	 * @param link
	 *            Zu speichernder Link.
	 * @param provider
	 *            Anbieter
	 * @param streamLink
	 *            Legt fest, ob es ein Stream oder ein Provider Link ist (1 =
	 *            Wahr, 0 = falsch).
	 */
	public Link(String link, int provider, int streamLink) {
		this.setProviderID(provider);
		if (streamLink == 1) {
			this.setType(LinkType.STREAM);
		} else {
			this.setType(LinkType.PROVIDER);
		}
		this.setLink(link);
	}

	@Override
	public boolean equals(Object compare) {
		return compare instanceof Link && ((Link) compare).getLink().equals(this.getLink())
				&& (((Link) compare).getProviderID() == this.getProviderID());
	}

	/**
	 * Gibt die Linkinformationen zurueck,
	 *
	 * @return
	 */
	@XmlTransient
	public String getInfo() {
		String out = "Link: " + this.toString() + "\n" + "Provider: " //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				+ this.getProviderID() + "\n" + "Created: " + this.getLinkCreated(); //$NON-NLS-1$ //$NON-NLS-2$

		if (this.getLinkUpdated() != null) {
			out += "\n" + "Updated: " + this.getLinkUpdated(); //$NON-NLS-1$ //$NON-NLS-2$
		}

		out += "\n" + "Offline: " + this.isOffline(); //$NON-NLS-1$ //$NON-NLS-2$

		return out;
	}

	/**
	 * @return the complete link
	 */
	@XmlElement(name = "link2")
	public String getLink() {
		return this.getString(LINK);
	}

	/**
	 * @return the linkCreated Date
	 */
	@XmlElement(name = "cDat")
	public LocalDateTime getLinkCreated() {
		return this.getTimestamp(CREATED).toLocalDateTime();
	}

	/**
	 * @return the linkUpdated
	 */
	@XmlElement(name = "uDat")
	public LocalDateTime getLinkUpdated() {
		return this.getTimestamp(UPDATED).toLocalDateTime();
	}

	/**
	 * @return the offline
	 */
	@XmlAttribute(name = "off")
	public int getOffline() {
		return this.getBoolean(OFFLINE) ? 1 : 0;
	}

	/**
	 * @return the providerID
	 */
	@XmlAttribute(name = "pID")
	public int getProviderID() {
		return this.getInteger(ID_PROVIDER);
	}

	/**
	 * @return the streamLink
	 */
	@XmlAttribute(name = "s")
	public int getStreamLink() {
		return this.getType().getValue();
	}

	/**
	 * @return the offline
	 */
	@XmlTransient
	public boolean isOffline() {
		return this.getBoolean(OFFLINE);
	}

	/**
	 * Setzt den unbereinigten Link.
	 *
	 * @param link
	 *            unbereinigter Link.
	 */
	public void setLink(String link) {
		this.setString(LINK, clearLink(link));
	}

	/**
	 * @param linkCreated
	 *            the linkCreated to set
	 */
	public void setLinkCreated(LocalDateTime linkCreated) {
		this.setTimestamp(CREATED, linkCreated);
	}

	/**
	 * @param linkUpdated
	 *            the linkUpdated to set
	 */
	public void setLinkUpdated(LocalDateTime linkUpdated) {
		this.setTimestamp(UPDATED, linkUpdated);
	}

	/**
	 * @param offline
	 *            the offline to sset
	 */
	public void setOffline(boolean offline) {
		this.setBoolean(OFFLINE, offline);
	}

	/**
	 * @param offline
	 *            the offline to sset
	 */
	public void setOffline(int offline) {
		setOffline(offline == 0);
	}

	/**
	 * @param providerID
	 *            the providerID to set
	 */
	public void setProviderID(int providerID) {
		this.setInteger(ID_PROVIDER, providerID);
	}

	public void setStreamLink(int i) {
		this.streamLink = i;
	}

	@Override
	public String toString() {
		return this.link;
	}

	@Override
	@NotNull
	public int compareTo(Link o) {
		if (this.linkUpdated == null)
			if (o.linkUpdated == null)
				return 0;
			else
				return -1;

		else if (o.linkUpdated == null)
			return 1;

		return this.linkUpdated.compareTo(o.linkUpdated);
	}
}

/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.etern.it.jserienkollar.datenbank;

/**
 * Klasse zum Speichern der Suchergebnisse.
 *
 * @author Dennis van der Wals
 *
 */
public class SearchResult {
    private final String   title;
    private final String link;
    private final int      provider;
    private final Language language;
    
    /**
     * Konstruktor
     *
     * @param language
     *            Sprache des Ergebnisses
     * @param title
     *            Titel des Ergebnisses
     * @param link
     *            Link zum Ergebnis
     * @param provider
     *            Anbieter des Links
     */
    public SearchResult(Language language, String title, String link,
            int provider) {
        if (language != null) {
            this.language = language;
        } else {
            this.language = Language.UNKNOWN;
        }
        
        this.title = title;
        this.link = link;
        this.provider = provider;
    }
    
    /**
     * @return the language
     */
    public Language getLanguage() {
        return this.language;
    }
    
    /**
     * @return the link
     */
    public String getLink() {
        return this.link;
    }
    
    /**
     * @return the provider
     */
    public int getProvider() {
        return this.provider;
    }
    
    /**
     * @return the title
     */
    public String getTitle() {
        return this.title;
    }
    
    @Override
    public String toString() {
        return this.title + " " + this.language + " " + this.link; //$NON-NLS-1$ //$NON-NLS-2$
    }
    
}

/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
/**
 *
 */
package de.etern.it.jserienkollar.datenbank;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import javax.swing.JFileChooser;
import javax.xml.bind.JAXB;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import de.etern.it.progressMonitor.CommonProgressMonitor;
import de.etern.it.progressMonitor.Progress;
import de.etern.it.properties.Property;
import main.java.de.etern.it.jserienkollar.DatabaseChecker;
import main.java.de.etern.it.jserienkollar.datenbank.importobjekte.Season;
import main.java.de.etern.it.jserienkollar.gui.TreePanel;

/**
 * Klasse zum Verwalten der Serien und Episoden
 *
 * @author Dennis
 *
 */
@XmlRootElement(namespace = "serienliste")
public class SeriesHandler {
	private static final String DATENBANK = "episodenListe.dat", //$NON-NLS-1$
			PROP_LOCATION = "db_verzeichnis"; //$NON-NLS-1$
	public static final int DB_VERSION = 1;

	private static SeriesHandler INSTANCE = getHandler();

	/**
	 * Wechselt in ein anderes Verzeichnis
	 */
	public static void changePath() {
		JFileChooser fc = new JFileChooser(INSTANCE.getVerzeichnis());
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

		int val = fc.showOpenDialog(null);
		if (val == JFileChooser.APPROVE_OPTION) {
			Property.setConfig(PROP_LOCATION, fc.getSelectedFile().getAbsolutePath() + "\\"); //$NON-NLS-1$

			INSTANCE.save();

			INSTANCE = getHandler();
			TreePanel.getInstance().update();
		}
	}

	/**
	 * Erstellt oder laedt eine Seriendatenbank.
	 *
	 * @return SeriesHandler
	 */
	private static SeriesHandler getHandler() {

		SeriesHandler serien = new SeriesHandler();

		try {
			// Datenbanken erstellen, wenn nicht vorhanden
			if (!serien.getDB().createNewFile()) {

				try {
					Console.getInstance().write(Property.getString("SeriesHandler.1")); //$NON-NLS-1$

					InputStream is = new GZIPInputStream(new FileInputStream(serien.getDB()));

					serien = JAXB.unmarshal(is, SeriesHandler.class);

					is.close();

					serien.afterUnmarshall();

				} catch (Exception e) {
					Console.getInstance().write(Property.getString("SeriesHandler.2")); //$NON-NLS-1$
					serien.save();
				}
				Collections.sort(serien.getSeries());

			} else {
				Console.getInstance().write(Property.getString("SeriesHandler.4")); //$NON-NLS-1$
				serien.save();
			}

		} catch (IOException e) {
			serien.getDB().renameTo(new File("backup")); //$NON-NLS-1$
			BugReport.recordBug(e, false);
		}

		return serien;
	}

	/**
	 * Gibt die einzige Instanz der Datenbank zurueck.
	 *
	 * @return SeriesHandler
	 */
	public static SeriesHandler getInstance() {
		return INSTANCE;
	}

	/** Testmethode **/
	public static void main(String[] args) {
		// Reinigen der Datenbank

		// SeriesHandler.getInstance();
		//
		// int counter = 0;
		//
		// for (Series serie : SeriesHandler.getInstance().getSeries()) {
		// for (Season season : serie.getSeasons()) {
		// for (Episode episode : season.getEpisodes()) {
		// for (int i = 0; i < episode.getStreams().size(); i++) {
		// if (episode.getStreams().get(i).getLink()
		// .contains("<div id=")) {
		// episode.getStreams().remove(i);
		// counter++;
		// }
		// }
		// }
		// }
		// }
		//
		// System.out.println(counter);
		//
		// SeriesHandler.getInstance().save();

		String t = "bla";
		String o = "axe";

		LinkedList<String> liste = new LinkedList<>();
		liste.add(t);
		liste.add(o);
		liste.add(t);
		liste.add(t);

		System.out.println(liste.size());

		DatabaseChecker.removeDoubleEntries(liste);

		System.out.println(liste.size());

	}

	/**
	 * Setzt "Gesehen"-Status aller Episoden einer Staffel.
	 *
	 * @param staffel
	 *            Staffel
	 * @param seen
	 *            Wahrheitswert
	 */
	public static void setSeasonAsSeen(Season staffel, boolean seen) {
		for (Episode episode : staffel.getEpisodes()) {
			episode.setSeen(seen);
		}
	}

	/**
	 * Setzt "Gesehen"-Status aller Episoden einer Serie.
	 *
	 * @param serie
	 *            Serie
	 * @param seen
	 *            Wahrheitswert
	 */
	public static void setSeriesAsSeen(Series serie, boolean seen) {
		for (Season staffel : serie.getSeasons()) {
			setSeasonAsSeen(staffel, seen);
		}
	}

	private final File db;

	private int dbVersion;

	private ArrayList<Series> series;

	@XmlTransient
	private String verzeichnis;

	/** Standardkonstruktur **/
	public SeriesHandler() {
		this.series = new ArrayList<>();

		// Datenbankpfad lesen, bei Bedarf setzen
		this.verzeichnis = Property.getConfig(PROP_LOCATION);

		if (this.verzeichnis == null) {
			this.verzeichnis = new File("tmp").getAbsolutePath(); //$NON-NLS-1$
			this.verzeichnis = this.verzeichnis.substring(0, this.verzeichnis.length() - "tmp".length()); //$NON-NLS-1$

			Property.setConfig(PROP_LOCATION, this.verzeichnis);
		}

		// Dateien erstellen
		this.db = new File(this.verzeichnis + DATENBANK);

		this.dbVersion = 0;
	}

	/**
	 * Fuegt eine Serie hinzu.
	 *
	 * @param serie
	 */
	public synchronized void addSerie(Series serie) {
		this.series.add(serie);
	}

	/**
	 * Sorgt fuer eine bidirektionale Verkettung der Datenbankelemente.
	 */
	private void afterUnmarshall() {
		for (Series serie : this.series) {
			link.setLink(link.getLink().replace("movie4k.to", "movie4k.tv"));
			for (Season staffel : serie.getSeasons()) {
				for (Episode episode : staffel.getEpisodes()) {
					episode.setSeason(staffel);

				}
				staffel.setSerie(serie);
			}
		}
	}

	public void delete(Series s) {
		if (this.series.remove(s)) {
			save();
			TreePanel.getInstance().update();
		}
	}

	/**
	 * Sucht eine Serie anhand des Titels
	 *
	 * @param titel
	 *            Titel der Serie
	 * @return Serie oder null
	 */
	public Series findeSerie(String titel) {
		for (Series serie : this.series) {
			if (serie.getTitel().equals(titel)) {
				return serie;
			}
		}
		return null;
	}

	/**
	 * @return the dB
	 */
	public File getDB() {
		return this.db;
	}

	/**
	 * @return the dbVersion
	 */
	@XmlAttribute(name = "version")
	public int getDbVersion() {
		return this.dbVersion;
	}

	/**
	 * Gibt die Anzahl der bekannten offline-Links zurueck.
	 *
	 * @return Anzahl offline-Links
	 */
	public int getOfflineLinksSize() {
		int offline = 0;
		for (Series serie : getSeries()) {
			for (Season staffel : serie.getSeasons()) {
				for (Episode episode : staffel.getEpisodes()) {
					for (Link link : episode.getStreams()) {
						if (link.isOffline()) {
							offline++;
						}
					}
				}
			}
		}
		return offline;
	}

	/**
	 * @return the series
	 */
	@XmlElement(name = "se")
	public ArrayList<Series> getSeries() {
		Collections.sort(this.series);
		return this.series;
	}

	/**
	 * @return the verzeichnis
	 */
	public String getVerzeichnis() {
		return this.verzeichnis;
	}

	/**
	 * Speichert die Datenbank
	 */
	public void save() {
		Progress save = CommonProgressMonitor.INSTANCE.createProgress(Property.getString("SeriesHandler.5"), true, //$NON-NLS-1$
				true, false, false);
		save.start();
		Console.getInstance().write(Property.getString("SeriesHandler.5")); //$NON-NLS-1$

		this.dbVersion = DB_VERSION;

		Collections.sort(this.series);
		// Ohne Kompression speichern
		// JAXB.marshal(this, new File("test.xml"));

		try {
			OutputStream os = new GZIPOutputStream(new FileOutputStream(this.db));

			JAXB.marshal(this, os);
			os.close();

		} catch (IOException e) {
			BugReport.recordBug(e, false);
		}

		save.finish();

	}

	/**
	 * @param dbVersion
	 *            the dbVersion to set
	 */
	public void setDbVersion(int dbVersion) {
		this.dbVersion = dbVersion;
	}

	/**
	 * @param series
	 *            the series to set
	 */
	public void setSeries(ArrayList<Series> series) {
		this.series = series;
	}
}

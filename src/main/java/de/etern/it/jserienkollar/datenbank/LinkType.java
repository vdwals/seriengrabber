package de.etern.it.jserienkollar.datenbank;

public enum LinkType {
	STREAM(1, "stream"), PROVIDER(0, "provider");

	private Integer value;
	private String name;

	private LinkType(Integer value, String c) {
		this.value = value;
		this.name = c;
	}

	/**
	 * Get value of test case status.
	 *
	 * @return value of test case status
	 */
	public Integer getValue() {
		return this.value;
	}

	/**
	 * Print value of test case status.
	 *
	 * @return value of test case status
	 */
	@Override
	public String toString() {
		return this.name;
	}

}

/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.etern.it.jserienkollar.datenbank.importobjekte;

import com.sun.istack.internal.NotNull;
import de.etern.it.properties.Property;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.ArrayList;
import java.util.Date;

/**
 * Klasse zum Verwalten der Episoden.
 *
 * @author Dennis van der Wals
 */
public class Episode implements Comparable<Episode> {
    private ArrayList<Link> streams, provider;
    private int epsiodeNumber;
    private int seen;
    private int downloaded;
    private Date episodeCreation, episodeUpdate;

    @XmlTransient
    private Season staffel;

    /** Standardkonstruktor **/
    public Episode() {
        this.streams = new ArrayList<>();
        this.provider = new ArrayList<>();
        this.episodeCreation = new Date();
        this.seen = 0;
    }

    /**
     * Konstruktor.
     *
     * @param episodeNumber Nummer der Episode
     * @param staffel       Staffel, deren Teil die Episode ist
     */
    public Episode(int episodeNumber, Season staffel) {
        this.streams = new ArrayList<>();
        this.provider = new ArrayList<>();
        this.episodeCreation = new Date();
        this.seen = 0;

        this.epsiodeNumber = episodeNumber;
        this.staffel = staffel;
    }

    /**
     * Fuegt einen Link hinzu, wenn dieser nicht offline ist und noch nicht
     * verzeichnet.
     *
     * @param streamLink Hinzuzufuegender Link
     * @return Wahrheitswert ueber Erfolg
     */
    public synchronized boolean addStreamLink(Link streamLink) {
        if (!this.streams.contains(streamLink)) {
            this.streams.add(streamLink);
            return true;
        }
        return false;
    }

    /**
     * Fuegt einen Provider hinzu, wenn dieser noch nicht
     * verzeichnet ist.
     *
     * @return Wahrheitswert ueber Erfolg
     */
    public synchronized boolean addProviderLink(Link providerLink) {
        if (!this.provider.contains(providerLink)) {

            this.provider.add(providerLink);
            return true;
        }
        return false;
    }

    @Override
    @NotNull
    public int compareTo(Episode o) {
        return new Integer(this.getEpisodeNumber()).compareTo(o.getEpisodeNumber());
    }

    @Override
    public boolean equals(Object e) {
        return (e instanceof Episode) && (((Episode) e).getEpisodeNumber() == this.getEpisodeNumber()) && (((Episode) e).staffel.equals(this.staffel));
    }

    /**
     * @return the creation
     */
    @XmlElement(name = "cDat")
    public Date getCreation() {
        return this.episodeCreation;
    }

    /**
     * @return the number
     */
    @XmlAttribute(name = "eNum")
    public int getEpisodeNumber() {
        return this.epsiodeNumber;
    }

    /**
     * @return the update
     */
    @XmlAttribute(name = "uDat")
    public Date getEpisodeUpdate() {
        return this.episodeUpdate;
    }

    /**
     * @return ArrayList<Link> the provider
     */
    @XmlElement(name = "p")
    public ArrayList<Link> getProvider() {
        return this.provider;
    }

    /**
     * @return the seen
     */
    @XmlAttribute(name = "seen")
    public int getSeen() {
        return this.seen;
    }

    /**
     * @return ArrayList<Link> the streams
     */
    @XmlElement(name = "s")
    public ArrayList<Link> getStreams() {
        return this.streams;
    }

    /**
     * @return the seen
     */
    public boolean hasBeenSeen() {
        return this.seen == 1;
    }

    /**
     * @param creation the creation to set
     */
    public void setCreation(Date creation) {
        this.episodeCreation = creation;
    }

    /**
     * @param number the number to set
     */
    public void setEpisodeNumber(int number) {
        this.epsiodeNumber = number;
    }

    /**
     * @param update the update to set
     */
    public void setEpisodeUpdate(Date update) {
        this.episodeUpdate = update;
    }

    public void setProvider(ArrayList<Link> provider) {
        this.provider = provider;
    }

    /**
     * @param staffel the staffel to set
     */
    public void setSeason(Season staffel) {
        this.staffel = staffel;
    }

    /**
     * @param seen the seen to set
     */
    public void setSeen(boolean seen) {
        this.seen = seen ? 1 : 0;
    }

    /**
     * @param seen the seen to set
     */
    public void setSeen(int seen) {
        this.seen = seen;
    }

    /**
     * @param streams the streams to set
     */
    public void setStreams(ArrayList<Link> streams) {
        this.streams = streams;
    }

    @Override
    public String toString() {
        return Property.getString("Episode.0") + " " + this.getEpisodeNumber(); //$NON-NLS-1$
    }

    /**
     * @return the staffel
     */
    public Season getStaffel() {
        return staffel;
    }

    public int getDownloaded() {
        return downloaded;
    }

    public void setDownloaded(int downloaded) {
        this.downloaded = downloaded;
    }

    public void setDownloaded(boolean downloaded) {
        this.downloaded = downloaded ? 1 : 0;
    }

    public boolean isDownloaded() {
        return getDownloaded() == 1;
    }
}

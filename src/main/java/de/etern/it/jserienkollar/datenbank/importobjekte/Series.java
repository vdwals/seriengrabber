/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.etern.it.jserienkollar.datenbank.importobjekte;

import com.sun.istack.internal.NotNull;
import de.etern.it.jserienkollar.gui.TreePanel;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

/**
 * Klasse zum Verwalten der Serien.
 *
 * @author Dennis van der Wals
 */
public class Series implements Comparable<Series> {
    private ArrayList<Season> seasons;
    private ArrayList<Link> links;
    private String titel;
    private int updated, download;
    private Date seriesCreation, seriesUpdate;

    /** Standardkonstruktor **/
    public Series() {
        this.titel = "";
        this.links = new ArrayList<>();
        this.seasons = new ArrayList<>();
        this.seriesCreation = new Date();
    }

    /**
     * Fuegt eine neue Staffel hinzu, wenn es eine neue ist. Gibt die
     * entsprechende Staffel zurueck, ob neu oder nicht.
     *
     * @param neueStaffel neue Staffel
     * @return Wahrheitswert
     */
    public synchronized boolean addSeason(Season neueStaffel) {
        // Pr�fe auf neue Staffel
        if (!this.seasons.contains(neueStaffel)) {
            this.seasons.add(neueStaffel);
            neueStaffel.setSerie(this);
            TreePanel.getInstance().update();

            return true;
        }

        return false;
    }

    @Override
    @NotNull
    public int compareTo(Series o) {
        return this.getTitel().compareTo(o.getTitel());
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof Series) && ((Series) o).titel.equals(this.titel);
    }

    /**
     * Sucht eine Staffel anhand ihrer Nummer.
     *
     * @param nummer Staffel nummer
     * @return Staffel oder Null
     */
    public Season findeStaffel(String nummer) {
        int n = Integer.parseInt(nummer);
        for (Season staffel : this.seasons) {
            if (staffel.getSeasonNumber() == n) {
                return staffel;
            }
        }
        return null;
    }

    /**
     * Gibt die letzte Folder der Serie zurueck.
     *
     * @return letzte Episode der Serie
     */
    public Episode getLastEpisode() {
        Episode last = null;
        Season letzteStaffel = null;

        // Letzte Staffel filtern
        for (Season staffel : this.seasons) {
            if ((letzteStaffel == null) || (letzteStaffel.getSeasonNumber() < staffel.getSeasonNumber())) {
                letzteStaffel = staffel;
            }
        }

        // Letzte Episode der letzten Staffel filtern
        if (letzteStaffel != null && letzteStaffel.getEpisodes() != null)
            for (Episode episode : letzteStaffel.getEpisodes()) {
                if ((last == null) || (last.getEpisodeNumber() < episode.getEpisodeNumber())) {
                    last = episode;
                }
            }

        return last;
    }

    /**
     * @return links
     */
    @XmlElement(name = "link")
    public ArrayList<Link> getLinks() {
        return this.links;
    }

    /**
     * @return seasons
     */
    @XmlElement(name = "st")
    public ArrayList<Season> getSeasons() {
        Collections.sort(this.seasons);
        return this.seasons;
    }

    /**
     * @return the creation
     */
    @XmlAttribute(name = "secD")
    public Date getSeriesCreation() {
        return this.seriesCreation;
    }

    /**
     * @return the update
     */
    @XmlAttribute(name = "seuD")
    public Date getSeriesUpdate() {
        return this.seriesUpdate;
    }

    /**
     * @return name
     */
    @XmlAttribute(name = "t")
    public String getTitel() {
        return this.titel;
    }

    /**
     * @return the updated
     */
    @XmlAttribute(name = "u")
    public int getUpdated() {
        return this.updated;
    }

    /**
     * @return the updated
     */
    public boolean hasBeenUpdated() {
        return this.updated == 1;
    }

    /**
     * Verkn�pft zwei Serien miteinander
     *
     * @param toJoin zu verkn�pfende Serie
     */
    public void joinSeries(Series toJoin) {

        this.getLinks().addAll(toJoin.getLinks());
        // Pr�fe alle Staffeln
        for (Season staffel : toJoin.getSeasons()) {
            staffel.setSerie(this);
            if (this.addSeason(staffel)) {
                // Wenn Staffel vorhanden, pr�fe alle Episoden...
                Season aktuelleStaffel = this.getSeasons().get(this.getSeasons().indexOf(staffel));

                for (Episode episode : staffel.getEpisodes()) {
                    Episode aktuelleEpisode = aktuelleStaffel.findeEpisode("" //$NON-NLS-1$
                            + episode.getEpisodeNumber());
                    // Pr�fe, ob Episode schon vorhanden
                    if (aktuelleEpisode == null) {
                        // F�ge hinzu...
                        aktuelleStaffel.addEpisode(episode);

                    } else {
                        // .. oder aktuallisiere Links
                        episode.getProvider().forEach(aktuelleEpisode::addProviderLink);
                        episode.getStreams().forEach(aktuelleEpisode::addStreamLink);
                    }
                }

            }
        }
    }

    /**
     * @param links
     */
    public void setLinks(ArrayList<Link> links) {
        this.links = links;
    }

    /**
     * @param seasons
     */
    public void setSeasons(ArrayList<Season> seasons) {
        this.seasons = seasons;

        for (Season season : seasons) {
            season.setSerie(this);
        }
    }

    /**
     * @param creation the creation to set
     */
    public void setSeriesCreation(Date creation) {
        this.seriesCreation = creation;
    }

    /**
     * @param update the update to set
     */
    public void setSeriesUpdate(Date update) {
        this.seriesUpdate = update;
    }

    /**
     * @param titel
     */
    public void setTitel(String titel) {
        this.titel = titel;
    }

    /**
     * @param updated the updated to set
     */
    public void setUpdated(boolean updated) {
        this.updated = updated ? 1 : 0;
    }

    /**
     * @param updated the updated to set
     */
    public void setUpdated(int updated) {
        this.updated = updated;
    }

    @Override
    public String toString() {
        return this.getTitel();
    }

    public int getDownload() {
        return download;
    }

    public void setDownload(int download) {
        this.download = download;
    }

    public boolean isDownload() {
        return (download == 1);
    }

    public void setDownload(boolean download) {
        this.download = download ? 1 : 0;
    }
}

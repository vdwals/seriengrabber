/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.etern.it.jserienkollar.datenbank.importobjekte;

import com.sun.istack.internal.NotNull;
import de.etern.it.jserienkollar.gui.TreePanel;
import de.etern.it.properties.Property;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

/**
 * Klasse zum Verwalten von Staffeln und Episoden
 *
 * @author Dennis van der Wals
 */
public class Season implements Comparable<Season> {
    private ArrayList<Episode> episodes;
    private int seasonNumber;
    private Date seasonCreation, seasonUpdate;

    @XmlTransient
    private Series serie;

    /** Standardkonstruktor **/
    public Season() {
        this.episodes = new ArrayList<>();
        this.seasonCreation = new Date();
    }

    /**
     * Konstruktor.
     *
     * @param seasonNumber Staffelnummer
     * @param serie        Serie, deren Teil die Staffel ist
     */
    public Season(int seasonNumber, Series serie) {
        this.seasonNumber = seasonNumber;
        this.episodes = new ArrayList<>();
        this.seasonCreation = new Date();
        this.serie = serie;
    }

    /**
     * Fuegt eine neue Episode hinzu, wenn sie nicht bereits vorhanden ist.
     * Gleicht ansonsten die Providerlisten der neuen und vorhandenen Episode
     * ab.
     *
     * @param neueEpisode hinzuzufuegende Episode
     * @return Wahrheitswert ob neu
     */
    public synchronized boolean addEpisode(Episode neueEpisode) {
        if (!this.episodes.contains(neueEpisode)) {
            this.episodes.add(neueEpisode);
            neueEpisode.setSeason(this);
            TreePanel.getInstance().update();
            return true;
        }

        // Fuege Provider hinzu
        Episode alt = this.findeEpisode(neueEpisode.getEpisodeNumber() + "");
        if (alt != null) {
            neueEpisode.getProvider().forEach(alt::addProviderLink);
        }
        return false;
    }

    @Override
    @NotNull
    public int compareTo(Season o) {
        return new Integer(this.getSeasonNumber()).compareTo(o.getSeasonNumber());
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof Season) && (this.seasonNumber == ((Season) o).getSeasonNumber()) && this.serie.equals(((Season) o).getSerie());
    }

    /**
     * Gibt die Episode mit der hinterlegten nummer zur�ck oder null
     *
     * @param nummer Episodennummer
     * @return Episode oder NULL
     */
    public Episode findeEpisode(String nummer) {
        int n = Integer.parseInt(nummer);
        for (Episode episode : this.episodes) {
            if (episode.getEpisodeNumber() == n) {
                return episode;
            }
        }
        return null;
    }

    /**
     * @return the episodes
     */
    @XmlElement(name = "e")
    public ArrayList<Episode> getEpisodes() {
        Collections.sort(this.episodes);
        return this.episodes;
    }

    /**
     * @return the creation
     */
    @XmlAttribute(name = "stcD")
    public Date getSeasonCreation() {
        return this.seasonCreation;
    }

    /**
     * @return the number
     */
    @XmlAttribute(name = "sNum")
    public int getSeasonNumber() {
        return this.seasonNumber;
    }

    /**
     * @return the update
     */
    @XmlAttribute(name = "stuD")
    public Date getSeasonUpdate() {
        return this.seasonUpdate;
    }

    /**
     * @return the serie
     */
    @XmlTransient
    public Series getSerie() {
        return this.serie;
    }

    /**
     * @param episodes the episodes to set
     */
    public void setEpisodes(ArrayList<Episode> episodes) {
        this.episodes = episodes;

        for (Episode episode : episodes) {
            episode.setSeason(this);
        }
    }

    /**
     * @param creation the creation to set
     */
    public void setSeasonCreation(Date creation) {
        this.seasonCreation = creation;
    }

    /**
     * @param number the number to set
     */
    public void setSeasonNumber(int number) {
        this.seasonNumber = number;
    }

    /**
     * @param update the update to set
     */
    public void setSeasonUpdate(Date update) {
        this.seasonUpdate = update;
    }

    /**
     * @param serie the serie to set
     */
    public void setSerie(Series serie) {
        this.serie = serie;
    }

    @Override
    public String toString() {
        return Property.getString("Season.0") + " " + this.getSeasonNumber(); //$NON-NLS-1$
    }
}

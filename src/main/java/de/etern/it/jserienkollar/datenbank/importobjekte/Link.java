/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.etern.it.jserienkollar.datenbank.importobjekte;

import com.sun.istack.internal.NotNull;

import java.util.Date;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Klasse zum Speichern eines Links.
 *
 * @author Dennis van der Wals
 *
 */
public class Link implements Comparable<Link> {
    public static final String HTTP = "http://";
    
    /**
     * Bereinigt den Link; entfernt alle HTML-Zeichen
     *
     * @param link
     *            zu bereiniginder Link
     * @return bereinigder Link
     */
    public static String clearLink(String link) {
        return link.replaceAll("&amp;", "&");
    }
    
    private String link, provider;
    private int    providerID, streamLink, offline;
    
    private Date   linkCreated, linkUpdated;
    
    /** Standardkonstruktor **/
    public Link() {
        this.linkCreated = new Date();
    }
    
    /**
     * Konstruktor.
     *
     * @param link
     *            Zu speichernder Link.
     * @param provider
     *            Anbieter
     * @param streamLink
     *            Legt fest, ob es ein Stream oder ein Provider Link ist.
     */
    public Link(String link, int provider, boolean streamLink) {
        this.providerID = provider;
        this.linkCreated = new Date();
        this.offline = 0;
        this.streamLink = streamLink ? 1 : 0;
        this.setLink(link);
    }
    
    /**
     * Konstruktor.
     *
     * @param link
     *            Zu speichernder Link.
     * @param provider
     *            Anbieter
     * @param streamLink
     *            Legt fest, ob es ein Stream oder ein Provider Link ist (1 =
     *            Wahr, 0 = falsch).
     */
    public Link(String link, int provider, int streamLink) {
        this.providerID = provider;
        this.linkCreated = new Date();
        this.offline = 0;
        this.streamLink = streamLink;
        this.setLink(link);
    }
    
    @Override
    public boolean equals(Object compare) {
        // DEBUG closed
        // System.out.println(compare);
        // System.out.println(link);
        // System.out.println(((Link) compare).getLink().equals(this.link));
        // System.out.println(((Link) compare).getLink()
        // .replaceAll("&amp;", "&")
        // .equals(this.link.replaceAll("&amp;", "&")));
        return compare instanceof Link && ((Link) compare).getLink().equals(this.getLink()) && (((Link) compare).getProviderID() == this.providerID);
    }
    
    /**
     * Gibt die Linkinformationen zurueck,
     *
     * @return
     */
    @XmlTransient
    public String getInfo() {
        String out = "Link: " + this.toString() + "\n" + "Provider: " //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                + this.providerID + "\n" + "Created: " + this.linkCreated; //$NON-NLS-1$ //$NON-NLS-2$
        
        if (this.linkUpdated != null) {
            out += "\n" + "Updated: " + this.linkUpdated; //$NON-NLS-1$ //$NON-NLS-2$
        }
        
        out += "\n" + "Offline: " + this.offline; //$NON-NLS-1$ //$NON-NLS-2$
        
        return out;
    }
    
    /**
     * @return the complete link
     */
    @XmlElement(name = "link2")
    public String getLink() {
        return this.link;
    }
    
    /**
     * @return the linkCreated Date
     */
    @XmlElement(name = "cDat")
    public Date getLinkCreated() {
        return this.linkCreated;
    }
    
    /**
     * @return the linkUpdated
     */
    @XmlElement(name = "uDat")
    public Date getLinkUpdated() {
        return this.linkUpdated;
    }
    
    /**
     * @return the offline
     */
    @XmlAttribute(name = "off")
    public int getOffline() {
        return this.offline;
    }
    
    /**
     * @return the providerID
     */
    @XmlAttribute(name = "pID")
    public int getProviderID() {
        return this.providerID;
    }
    
    /**
     * @return the streamLink
     */
    @XmlAttribute(name = "s")
    public int getStreamLink() {
        return this.streamLink;
    }
    
    /**
     * @return the offline
     */
    @XmlTransient
    public boolean isOffline() {
        return this.offline == 1;
    }

    /**
     * Setzt den unbereinigten Link.
     *
     * @param link
     *            unbereinigter Link.
     */
    public void setLink(String link) {
        this.link = clearLink(link);
    }
    
    /**
     * @param linkCreated
     *            the linkCreated to set
     */
    public void setLinkCreated(Date linkCreated) {
        this.linkCreated = linkCreated;
    }
    
    /**
     * @param linkUpdated
     *            the linkUpdated to set
     */
    public void setLinkUpdated(Date linkUpdated) {
        this.linkUpdated = linkUpdated;
    }
    
    /**
     * @param offline
     *            the offline to sset
     */
    public void setOffline(boolean offline) {
        this.offline = offline ? 1 : 0;
    }
    
    /**
     * @param offline
     *            the offline to sset
     */
    public void setOffline(int offline) {
        this.offline = offline;
    }
    
    /**
     * @param providerID
     *            the providerID to set
     */
    public void setProviderID(int providerID) {
        this.providerID = providerID;
    }

    @XmlAttribute(name = "provider")
    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public void setStreamLink(int i) {
        this.streamLink = i;
    }
    
    @Override
    public String toString() {
        return this.link;
    }
    
    @Override
    @NotNull
    public int compareTo(Link o) {
        if (this.linkUpdated == null)
            if (o.linkUpdated == null)
                return 0;
            else
                return -1;
        
        else if (o.linkUpdated == null)
            return 1;
        
        return this.linkUpdated.compareTo(o.linkUpdated);
    }
}

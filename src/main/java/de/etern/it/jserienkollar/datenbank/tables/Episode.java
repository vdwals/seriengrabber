/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.etern.it.jserienkollar.datenbank.tables;

import com.sun.istack.internal.NotNull;

import de.etern.it.jserienkollar.datenbank.LinkType;
import de.etern.it.properties.Property;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import org.javalite.activejdbc.annotations.BelongsTo;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * Created by Dennis on 23.05.2016.
 */
@BelongsTo(foreignKeyName = "id_season", parent = Staffel.class)
public class Episode extends Table implements Comparable<Episode> {
	public static final String ID = "id_episode", ID_PARENT = Staffel.ID, NUMBER = "number", SEEN = "seen",
			DOWNLOADED = "downloaded";

	/** Standardkonstruktor **/
	public Episode() {
	}

	/**
	 * Konstruktor.
	 *
	 * @param episodeNumber
	 *            Nummer der Episode
	 * @param staffel
	 *            Staffel, deren Teil die Episode ist
	 */
	public Episode(int episodeNumber) {
		this.setEpisodeNumber(episodeNumber);
	}

	/**
	 * Fuegt einen Link hinzu, wenn dieser nicht offline ist und noch nicht
	 * verzeichnet.
	 *
	 * @param streamLink
	 *            Hinzuzufuegender Link
	 * @return Wahrheitswert ueber Erfolg
	 */
	public synchronized boolean addStreamLink(Link streamLink) {
		if (!this.getStreams().contains(streamLink)) {
			this.add(streamLink);
			return true;
		}
		return false;
	}

	/**
	 * Fuegt einen Provider hinzu, wenn dieser noch nicht verzeichnet ist.
	 *
	 * @return Wahrheitswert ueber Erfolg
	 */
	public synchronized boolean addProviderLink(Link providerLink) {
		if (!this.getProvider().contains(providerLink)) {
			this.add(providerLink);
			return true;
		}
		return false;
	}

	@NotNull
	@Override
	public int compareTo(Episode o) {
		return new Integer(this.getEpisodeNumber()).compareTo(o.getEpisodeNumber());
	}

	@Override
	public boolean equals(Object e) {
		return (e instanceof Episode) && (((Episode) e).getEpisodeNumber() == this.getEpisodeNumber())
				&& (((Episode) e).parent(Staffel.class).equals(this.parent(Staffel.class)));
	}

	@Override
	public String toString() {
		return Property.getString("Episode.0") + " " + this.getEpisodeNumber(); //$NON-NLS-1$ //$NON-NLS-2$
	}

	@XmlElement(name = "stream")
	public List<Link> getStreams() {
		return this.get(Link.class, Link.TYPE + " = ?", LinkType.STREAM);
	}

	public void setStreams(List<Link> streams) {
		this.addModels(streams);
	}

	@XmlElement(name = "provider")
	public List<Link> getProvider() {
		return this.get(Link.class, Link.TYPE + " = ?", LinkType.PROVIDER);
	}

	public void setProvider(List<Link> provider) {
		this.addModels(provider);
	}

	@XmlElement(name = "number")
	public int getEpisodeNumber() {
		return this.getInteger(NUMBER);
	}

	public void setEpisodeNumber(int episodeNumber) {
		this.setInteger(NUMBER, episodeNumber);
	}

	@XmlAttribute(name = "seen")
	public boolean isSeen() {
		return this.getBoolean(SEEN);
	}

	public void setSeen(boolean seen) {
		this.setBoolean(SEEN, seen);
	}

	@XmlAttribute(name = "downloaded")
	public boolean isDownloaded() {
		return this.getBoolean(DOWNLOADED);
	}

	public void setDownloaded(boolean downloaded) {
		this.setBoolean(DOWNLOADED, downloaded);
	}

	@XmlElement(name = "created")
	public Date getEpisodeCreation() {
		return this.getTimestamp(CREATED);
	}

	public void setEpisodeCreation(LocalDateTime episodeCreation) {
		this.setTimestamp(CREATED, episodeCreation);
	}

	@XmlElement(name = "updated")
	public Date getEpisodeUpdate() {
		return this.getTimestamp(UPDATED);
	}

	public void setEpisodeUpdate(LocalDateTime episodeUpdate) {
		this.setTimestamp(UPDATED, episodeUpdate);
	}

	public Staffel getStaffel() {
		return this.parent(Staffel.class);
	}

	public void setStaffel(Staffel staffel) {
		this.setParent(staffel);
	}
}

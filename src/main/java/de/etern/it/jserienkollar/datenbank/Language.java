/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.etern.it.jserienkollar.datenbank;

import de.etern.it.properties.Property;

/**
 * Enumeration fuer verschiedene Sprachen.
 *
 * @author Dennis van der Wals
 *
 */
public enum Language {
    DEUTSCH(Property.getString("Language.0")), ENGLISCH(Property.getString("Language.1")), DEUTSCH_ENGLISCH( //$NON-NLS-1$ //$NON-NLS-2$
            Property.getString("Language.2")), TUERKISCH(Property.getString("Language.3")), SPANISCH(Property.getString("Language.4")), CHINESISCH( //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            Property.getString("Language.5")), FRANZOESISCH(Property.getString("Language.6")), JAPANISCH(Property.getString("Language.7")), SUERISCH( //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            Property.getString("Language.8")), ITALIENISCH(Property.getString("Language.9")), KROATISCH(Property.getString("Language.10")), RUSSISCH( //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            Property.getString("Language.11")), BOSSNISCH(Property.getString("Language.12")), NIEDERLAENDISCH( //$NON-NLS-1$ //$NON-NLS-2$
            Property.getString("Language.13")), KOREANISCH(Property.getString("Language.14")), ISLAENDISCH( //$NON-NLS-1$ //$NON-NLS-2$
            Property.getString("Language.15")), POLNISCH(Property.getString("Language.16")), FINNISCH(Property.getString("Language.17")), GRIECHISCH( //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            Property.getString("Language.18")), UNKNOWN(Property.getString("Language.19")); //$NON-NLS-1$ //$NON-NLS-2$
    
    private final String sprache;
    
    /**
     * Konstruktor.
     *
     * @param sprache
     *            Sprache die repraesentiert wird.
     */
    Language(String sprache) {
        this.sprache = sprache;
    }
    
    @Override
    public String toString() {
        return this.sprache;
    }
}

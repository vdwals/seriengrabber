/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.etern.it.jserienkollar.jaxb;

import de.etern.it.jserienkollar.implement.CommonUtils;
import de.etern.it.jserienkollar.net.GenericStreamProvider;
import de.etern.it.jserienkollar.net.StreamProvider;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Created by Dennis on 28.04.2016.
 */
@XmlRootElement(namespace = "streamprovider")//$NON-NLS-1$
public class StreamProviderJ {
    private String name;
    private String basicLink;

    private Pattern pattern;
    private Embedding embedding;


    private String offlineTestStringPattern;

    private int priority;

    public StreamProviderJ() {

    }

    public StreamProviderJ(String name, String basicLink, String startPattern, String endPattern, String basicEmbeddedLink, String embeddedPattern, String embeddedStartPattern, String embeddedEndPattern, String offlineTestStringPattern, boolean testWithStringLoad, boolean tranformEmbeddedLink, boolean testIfPatternIncluded, int priority) {
        this.name = name;
        this.basicLink = basicLink;
        this.priority = priority;
        this.offlineTestStringPattern = offlineTestStringPattern;

        this.pattern = new Pattern(startPattern, endPattern, testWithStringLoad, testIfPatternIncluded);
        this.embedding = new Embedding(basicEmbeddedLink, embeddedPattern, embeddedStartPattern, embeddedEndPattern, tranformEmbeddedLink);
    }

    @XmlTransient
    public de.etern.it.jserienkollar.net.StreamProvider getStreamProvider() {
        if (CommonUtils.isOneStringNullOrEmpty(name, basicLink, offlineTestStringPattern))
            return null;

        return new GenericStreamProvider(name, basicLink, pattern.getStartPattern(), pattern.getEndPattern(),
                embedding.getBasicEmbeddedLink(), embedding.getEmbeddedPattern(),
                embedding.getPattern().getStartPattern(), embedding.getPattern().getEndPattern(),
                offlineTestStringPattern, pattern.isTestWithStringLoad(), pattern.isTestIfPatternIncluded(), embedding.isTranformEmbeddedLink(), priority);
    }

    @XmlElement(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlElement(name = "basiclink")
    public String getBasicLink() {
        return basicLink;
    }

    public void setBasicLink(String basicLink) {
        this.basicLink = basicLink;
    }

    @XmlAttribute(name = "offlinePattern")
    public String getOfflineTestStringPattern() {
        return offlineTestStringPattern;
    }

    public void setOfflineTestStringPattern(String offlineTestStringPattern) {
        this.offlineTestStringPattern = offlineTestStringPattern;
    }

    @XmlAttribute
    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }
}

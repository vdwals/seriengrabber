/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.etern.it.jserienkollar.jaxb;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Dennis on 04.05.2016.
 */
public class Embedding {

    private String basicEmbeddedLink;
    private String embeddedPattern;
    private boolean tranformEmbeddedLink;

    private Pattern pattern;

    public Embedding() {

    }

    public Embedding(String basicEmbeddedLink, String embeddedPattern, String embeddedStartPattern, String embeddedEndPattern, boolean tranformEmbeddedLink) {
        this.basicEmbeddedLink = basicEmbeddedLink;
        this.embeddedPattern = embeddedPattern;
        this.tranformEmbeddedLink = tranformEmbeddedLink;

        this.pattern = new Pattern(embeddedStartPattern, embeddedEndPattern, false, false);
    }

    @XmlElement(name = "basiclink")
    public String getBasicEmbeddedLink() {
        return basicEmbeddedLink;
    }

    public void setBasicEmbeddedLink(String basicEmbeddedLink) {
        this.basicEmbeddedLink = basicEmbeddedLink;
    }

    @XmlElement(name = "pattern")
    public String getEmbeddedPattern() {
        return embeddedPattern;
    }

    public void setEmbeddedPattern(String embeddedPattern) {
        this.embeddedPattern = embeddedPattern;
    }

    @XmlAttribute(name = "transform")
    public boolean isTranformEmbeddedLink() {
        return tranformEmbeddedLink;
    }

    public void setTranformEmbeddedLink(boolean tranformEmbeddedLink) {
        this.tranformEmbeddedLink = tranformEmbeddedLink;
    }

    public Pattern getPattern() {
        return pattern;
    }

    public void setPattern(Pattern pattern) {
        this.pattern = pattern;
    }
}

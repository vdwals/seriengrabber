package de.etern.it.jserienkollar.gui;

import de.etern.it.jserienkollar.datenbank.tables.Link;

public class LinkCounter {
	private Link link;
	private String title;

	public LinkCounter(String title, Link link) {
		this.link = link;
		this.title = title;
	}

	public Link getLink() {
		return this.link;
	}

	public String getTitle() {
		return this.title;
	}

	@Override
	public String toString() {
		return this.title;
	}

}

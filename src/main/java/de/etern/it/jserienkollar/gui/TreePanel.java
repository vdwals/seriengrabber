/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.etern.it.jserienkollar.gui;

import de.etern.it.jserienkollar.SerienGrabberMain;
import de.etern.it.jserienkollar.datenbank.importobjekte.Episode;
import de.etern.it.jserienkollar.datenbank.importobjekte.Season;
import de.etern.it.jserienkollar.datenbank.importobjekte.Series;
import de.etern.it.jserienkollar.datenbank.SeriesHandler;
import de.etern.it.properties.Property;

import javax.swing.*;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import java.util.Vector;

/**
 * Klasse fuer die Baumanzeige
 *
 * @author Dennis van der Wals
 */
public class TreePanel {

    private static final TreePanel INSTANCE = new TreePanel();

    /**
     * Gibt die einzige Instanz zurueck
     *
     * @return TreePanel Instanz
     */
    public static TreePanel getInstance() {
        return INSTANCE;
    }

    private TreeSelectionListener treeAction;
    private JTree serienBaum;
    private JScrollPane scrollPane;

    private Vector<TreePath> expensionState;

    public boolean showSeen = true;

    /**
     * Erzeugt einen Baum zur Uebersicht der Anzeigeelemente.
     *
     * @return JTree
     */
    private DefaultMutableTreeNode buildTree() {
        DefaultMutableTreeNode root = new DefaultMutableTreeNode(Property.getString("SerienGrabberMain.0")); //$NON-NLS-1$

        // Listet alle Serien auf.
        for (Series serie : SeriesHandler.getInstance().getSeries()) {
            DefaultMutableTreeNode staffelKnoten = new DefaultMutableTreeNode(serie);
            // DEBUG closed
            // System.out.println(serie.getTitle());

            for (Season staffel : serie.getSeasons()) {
                DefaultMutableTreeNode episodenKnoten = new DefaultMutableTreeNode(staffel);

                // DEBUG closed
                // System.out.println(staffel.getSeasonNumber());

                staffel.getEpisodes().stream().filter(episode -> !episode.hasBeenSeen() || this.showSeen).forEach(episode -> {
                    DefaultMutableTreeNode node = new DefaultMutableTreeNode(episode);
                    episodenKnoten.add(node);
                });

                // Fuege Staffel nur hinzu, wenn nicht leer
                if (episodenKnoten.getChildCount() > 0) {
                    staffelKnoten.add(episodenKnoten);
                }
            }

            // F�ge Serie nur hinzu, wenn nicht leer
            if (staffelKnoten.getChildCount() > 0) {
                root.add(staffelKnoten);
            }
        }

        return root;
    }

    /**
     * @return scrollPane
     */
    public JScrollPane getScrollPane() {
        if (this.scrollPane == null) {
            this.scrollPane = new JScrollPane(this.getTree(), ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            // this.scrollPane.setPreferredSize(new Dimension(200, 100));
        }
        return this.scrollPane;
    }

    /**
     * @return JTree tree
     */
    private JTree getTree() {
        if (this.serienBaum == null) {
            this.serienBaum = new JTree(this.buildTree());
            this.serienBaum.addTreeSelectionListener(this.getTreeAction());
            this.serienBaum.setRootVisible(false);
        }
        return this.serienBaum;
    }

    /**
     * Erzeugt den Treeselection listener
     *
     * @return treeAction
     */
    private TreeSelectionListener getTreeAction() {
        if (this.treeAction == null) {
            this.treeAction = arg0 -> {
                if (arg0.getSource() == TreePanel.this.getTree()) {
                    saveExpansionState();
                }

                if ((arg0.getSource() == TreePanel.this.getTree()) && (TreePanel.this.getTree().getSelectionPath() != null)) {
                    // DEBUG closed
                    // System.out.println(getTree().getSelectionPath());

                    final DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) TreePanel.this.getTree().getSelectionPath().getLastPathComponent();

                    // Pr�fe, ob eine Episode markiert ist
                    if (selectedNode.getUserObject() instanceof Episode) {

                        SwingUtilities.invokeLater(() -> {
                            SerienGrabberMain.getInstance().getEpisodenPanel().setEpisode((Episode) selectedNode.getUserObject());
                            SerienGrabberMain.getInstance().setPanel(SerienGrabberMain.EPISODE_PANEL);
                            // SerienGrabberMain.getInstance().validate();
                            // SerienGrabberMain.getInstance().repaint();
                        });

                    } else if (selectedNode.getUserObject() instanceof Series) {

                        SwingUtilities.invokeLater(() -> {
                            SerienGrabberMain.getInstance().getSerienPanel().setSeries((Series) selectedNode.getUserObject());
                            SerienGrabberMain.getInstance().setPanel(SerienGrabberMain.SERIES_PANEL);
                            // SerienGrabberMain.getInstance().validate();
                            // SerienGrabberMain.getInstance().repaint();
                        });
                    }

                }
                TreePanel.this.getTree().validate();
            };
        }
        return this.treeAction;
    }

    /**
     * Ermittelt die im Baum ausgewaehlten Elemente und Unterelemente und setzt
     * ihren Gesehen-Status auf den uerbgebenen Wert.
     *
     * @param seen Wurden die markierten Elemente schon gesehen?
     */
    public void setSelection(boolean seen) {
        TreePath[] paths = this.getTree().getSelectionPaths();

        if (paths != null)
            for (int i = 0; i < paths.length; i++) {
                // System.out.println(paths[i].getLastPathComponent());

                // Lese markierten Knoten
                DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) paths[i].getLastPathComponent();

                // Pr�fe, ob eine Episode markiert ist
                if (selectedNode.getUserObject() instanceof Episode) {
                    ((Episode) selectedNode.getUserObject()).setSeen(seen);
                } else {
                    DefaultMutableTreeNode nextNode = null;

                    // Pr�fe, ob es einen Folgenden Knoten gibt
                    if ((i + 1) < paths.length) {
                        nextNode = (DefaultMutableTreeNode) paths[i + 1].getLastPathComponent();
                    }

                    // Pr�fe, ob ganze Staffel auf gesehen gesetzt werden
                    // soll
                    if ((selectedNode.getUserObject() instanceof Season) && ((nextNode == null) || (nextNode.getUserObject() instanceof Season))) {
                        SeriesHandler.setSeasonAsSeen((Season) selectedNode.getUserObject(), seen);

                        // Pr�fe, ob ganze Serie auf gesehen gestzt werden
                        // soll
                    } else if ((selectedNode.getUserObject() instanceof Series) && ((nextNode == null) || (nextNode.getUserObject() instanceof Series))) {
                        SeriesHandler.setSeriesAsSeen((Series) selectedNode.getUserObject(), seen);
                    }

                }

            }

        this.update();

        // SeriesHandler.getInstance().save();
    }

    /**
     * Zeichnet den Baum neu.
     */
    public void update() {
       saveExpansionState();

        ((DefaultTreeModel) this.getTree().getModel()).setRoot(this.buildTree());

        loadExpansionState();

//        // Listet alle Serien auf.
//        for (Series serie : SeriesHandler.getInstance().getSeries()) {
//            DefaultMutableTreeNode staffelKnoten = new DefaultMutableTreeNode(serie);
//            // DEBUG closed
//            // System.out.println(serie.getTitle());
//
//            for (Season staffel : serie.getSeasons()) {
//                DefaultMutableTreeNode episodenKnoten = new DefaultMutableTreeNode(staffel);
//
//                // DEBUG closed
//                // System.out.println(staffel.getSeasonNumber());
//
//                staffel.getEpisodes().stream().filter(episode -> !episode.hasBeenSeen() || this.showSeen).forEach(episode -> {
//                    DefaultMutableTreeNode node = new DefaultMutableTreeNode(episode);
//                    episodenKnoten.add(node);
//                });
//
//                // Fuege Staffel nur hinzu, wenn nicht leer
//                if (episodenKnoten.getChildCount() > 0) {
//                    staffelKnoten.add(episodenKnoten);
//                }
//            }
//
//            // F�ge Serie nur hinzu, wenn nicht leer
//            if (staffelKnoten.getChildCount() > 0) {
//                root.add(staffelKnoten);
//            }
//        }

        StatistikPanel.getInstance().update();
    }

    /**
     * Save the expansion state of a tree.
     *
     * @return expanded tree path as Enumeration
     */
    public void saveExpansionState() {
        expensionState = new Vector<>();

        for (int i = 0; i < getTree().getRowCount(); i++) {
            TreePath path = getTree().getPathForRow(i);

            if (getTree().isExpanded(path))
                expensionState.add(path);
        }
    }

    /**
     * Restore the expansion state of a JTree.
     */
    public void loadExpansionState() {

        if (expensionState != null) {

            for (TreePath path : expensionState) {
                getTree().expandPath(path);

            }

        }

    }

}

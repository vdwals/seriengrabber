/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.etern.it.jserienkollar.gui;

import de.etern.it.jserienkollar.Checker;
import de.etern.it.jserienkollar.SerienGrabberMain;
import de.etern.it.jserienkollar.datenbank.*;
import de.etern.it.jserienkollar.datenbank.importobjekte.Episode;
import de.etern.it.jserienkollar.datenbank.importobjekte.Link;
import de.etern.it.jserienkollar.datenbank.importobjekte.Season;
import de.etern.it.jserienkollar.datenbank.importobjekte.Series;
import de.etern.it.jserienkollar.implement.BugReport;
import de.etern.it.jserienkollar.net.LinkProvider;
import de.etern.it.jserienkollar.net.ProviderHandler;
import de.etern.it.jserienkollar.net.StreamProvider;
import de.etern.it.properties.Property;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;

public class SeriePanel extends JPanel {
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm"); //$NON-NLS-1$

    private Series serie;
    private final ActionListener linkButton;
    private final ActionListener actButtonListener;

    private JLabel titel, datumErstellt, datumAktuallisiert;
    private JButton actualize, rename, delete;
    private JScrollPane streams;

    /**
     * Konstruktor
     */
    public SeriePanel() {
        this.linkButton = e -> {
            try {
                Desktop.getDesktop().browse(new URI(e.getActionCommand()));
            } catch (IOException | URISyntaxException e1) {
                BugReport.recordBug(e1, false);
            }
        };

        this.actButtonListener = arg0 -> {
            if (SeriePanel.this.serie != null)
                if (arg0.getSource() == getActualize()) {

                    final Series s = SeriePanel.this.serie;

                    new Thread(() -> {
                        for (LinkProvider provider : ProviderHandler.getInstance().getLinkProvider()) {
                            provider.updateSerie(s);
                        }

                        Checker<Link> linkChecker = new Checker<>(s.getTitel() + "-Linkthread");

                        Checker<Episode> episodeChecker = new Checker<>(s.getTitel() + "-Streamthread");

                        for (Season staffel : s.getSeasons()) {
                            staffel.getEpisodes().forEach(episodeChecker::addToCheck);
                        }

                        Thread t = new Thread(episodeChecker);
                        t.start();

                        try {
                            t.join();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        for (Season staffel : s.getSeasons()) {
                            for (Episode e : staffel.getEpisodes()) {
                                linkChecker.addToCheck(e.getStreams());
                            }
                        }
                        new Thread(linkChecker).start();
                    });

                } else if (arg0.getSource() == getRename()) {
                    String name = JOptionPane.showInputDialog(SerienGrabberMain.getInstance(), "Serientitel �ndern", SeriePanel.this.serie.getTitel());

                    if (name != null && name.length() > 0) {
                        SeriePanel.this.serie.setTitel(name);
                        SeriesHandler.getInstance().save();
                        SeriePanel.this.setSeries(SeriePanel.this.serie);
                    }

                } else if (arg0.getSource() == getDelete()) {
                    int option = JOptionPane.showConfirmDialog(SerienGrabberMain.getInstance(), "Sind Sie sicher, dass Sie die Serie l�schen m�chten?", "Serie l�schen", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);

                    if (option == JOptionPane.YES_OPTION) {
                        SeriesHandler.getInstance().delete(SeriePanel.this.serie);
                        SeriePanel.this.setSeries(null);
                    }
                }
        };

        this.build();
    }

    /**
     * Erzeugt die graphische Oberflaeche.
     */
    private void build() {
        this.removeAll();

        this.setLayout(new BorderLayout(5, 5));

        JPanel daten = new JPanel(new GridLayout(2, 1));
        daten.add(this.getDatumErstellt());
        daten.add(this.getDatumAktuallisiert());

        JPanel topLine = new JPanel(new BorderLayout(5, 5));
        topLine.add(getTitel(), BorderLayout.WEST);
        topLine.add(daten, BorderLayout.EAST);

        JPanel buttons = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 10));
        buttons.add(getDelete());
        buttons.add(getRename());
        buttons.add(getActualize());

        JPanel top = new JPanel(new BorderLayout(5, 5));
        top.add(topLine, BorderLayout.NORTH);
        top.add(new JLabel(" "), BorderLayout.CENTER);
        top.add(buttons, BorderLayout.EAST);

        this.add(top, BorderLayout.NORTH);
        this.add(this.getStreams(), BorderLayout.CENTER);

        this.validate();
        this.repaint();
    }

    /**
     * @return the datumAktuallisiert
     */
    private JLabel getDatumAktuallisiert() {
        if (this.datumAktuallisiert == null) {
            this.datumAktuallisiert = new JLabel(Property.getString("EpisodenPanel.1")); //$NON-NLS-1$
            this.datumAktuallisiert.setHorizontalAlignment(SwingConstants.RIGHT);

            Font old = this.datumAktuallisiert.getFont();
            this.datumAktuallisiert.setFont(new Font(old.getFontName(), old.getStyle(), 11));

            if ((this.serie != null) && (this.serie.getSeriesUpdate() != null)) {
                this.datumAktuallisiert.setText(Property.getString("EpisodenPanel.1") //$NON-NLS-1$
                        + dateFormat.format(this.serie.getSeriesUpdate()));
            }
        }
        return this.datumAktuallisiert;
    }

    /**
     * @return the datumErstellt
     */
    private JLabel getDatumErstellt() {
        if (this.datumErstellt == null) {
            this.datumErstellt = new JLabel(Property.getString("EpisodenPanel.3")); //$NON-NLS-1$
            this.datumErstellt.setHorizontalAlignment(SwingConstants.RIGHT);

            Font old = this.datumErstellt.getFont();
            this.datumErstellt.setFont(new Font(old.getFontName(), old.getStyle(), 11));

            if (this.serie != null) {
                this.datumErstellt.setText(Property.getString("EpisodenPanel.3") //$NON-NLS-1$
                        + dateFormat.format(this.serie.getSeriesCreation()));
            }
        }
        return this.datumErstellt;
    }

    private JButton getLinkButton(Link link) {
        StreamProvider streamProvider = ProviderHandler.getInstance().identifyStreamProvider(link.getProviderID());

        if (streamProvider == null) {
            return null;
        }

        JLabel name = new JLabel(streamProvider.getName());
        Font old = name.getFont();
        name.setFont(new Font(old.getName(), Font.BOLD, old.getSize()));

        JLabel checked = new JLabel(Property.getString("EpisodenPanel.6")); //$NON-NLS-1$
        old = checked.getFont();
        checked.setFont(new Font(old.getName(), old.getStyle(), old.getSize() - 2));

        if (link.getLinkUpdated() != null) {
            checked.setText(dateFormat.format(link.getLinkUpdated()));
        }

        JButton openLink = new JButton(ProviderHandler.getInstance().identifyLinkProvider(link.getProviderID()).getName());
        openLink.addActionListener(this.linkButton);
        openLink.setActionCommand(link.getLink());

        return openLink;
    }

    /**
     * @return the titel
     */
    private JLabel getTitel() {
        if (this.titel == null) {
            this.titel = new JLabel(Property.getString("EpisodenPanel.7")); //$NON-NLS-1$
            Font old = this.titel.getFont();
            this.titel.setFont(new Font(old.getFontName(), old.getStyle(), 22));

            if (this.serie != null) {
                this.titel.setText(this.serie.getTitel());
            }
        }
        return this.titel;
    }

    /**
     * Setzt die Episode und erzeugt die neue Oberflaeche.
     *
     * @param serie the episode to set
     */
    public void setSeries(Series serie) {
        this.serie = serie;
        this.titel = null;
        this.datumAktuallisiert = null;
        this.datumErstellt = null;
        this.delete = null;
        this.rename = null;
        this.streams = null;
        this.actualize = null;

        this.build();
    }

    /**
     * @return the streams
     */
    private JScrollPane getStreams() {
        if (this.streams == null) {
            JPanel buttons = new JPanel();
            buttons.setLayout(new ModifiedFlowLayout(ModifiedFlowLayout.LEFT, 10, 10));

            if (this.serie != null) {
                for (Link link : this.serie.getLinks()) {
                    JButton button = this.getLinkButton(link);

                    if (button != null) {
                        buttons.add(button);
                    }
                }
            }

            JPanel content = new JPanel(new BorderLayout());
            content.add(buttons, BorderLayout.NORTH);

            this.streams = new JScrollPane(content, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        }
        return this.streams;
    }

    /**
     * @return the actualize
     */
    private JButton getActualize() {
        if (actualize == null) {
            this.actualize = new JButton(Property.getString("SerienPanel.0"));
            this.actualize.addActionListener(actButtonListener);
            if (this.serie == null)
                this.actualize.setEnabled(false);
        }
        return actualize;
    }

    /**
     * @return the actualize
     */
    private JButton getRename() {
        if (rename == null) {
            this.rename = new JButton(Property.getString("SerienPanel.2"));
            this.rename.addActionListener(actButtonListener);
            if (this.serie == null)
                this.rename.setEnabled(false);
        }
        return rename;
    }

    /**
     * @return the actualize
     */
    private JButton getDelete() {
        if (delete == null) {
            this.delete = new JButton(Property.getString("SerienPanel.1"));
            this.delete.addActionListener(actButtonListener);
            if (this.serie == null)
                this.delete.setEnabled(false);
        }
        return delete;
    }

}

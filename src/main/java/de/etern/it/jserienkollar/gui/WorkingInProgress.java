/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.etern.it.jserienkollar.gui;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JWindow;
import javax.swing.SwingUtilities;
import javax.swing.border.EtchedBorder;

import de.etern.it.jserienkollar.implement.Console;
import de.etern.it.properties.Property;

public class WorkingInProgress {
    static final int                       SUCH_FORTSCHRITT_SLEEP = 500,
            SUCH_MAX_PUNKTE = 5;
    private static final WorkingInProgress INSTANCE               = new WorkingInProgress();

    public static WorkingInProgress getInstance() {
        return INSTANCE;
    }

    private final ArrayList<JLabel>       labels;
    private final JWindow           window;

    // TODO Eigenener Thread; Fortschrittsbalken; Zeitberechnung

    private final HashMap<JLabel, Thread> progressThreads;

    public WorkingInProgress() {
        this.labels = new ArrayList<>();
        this.window = new JWindow();

        this.window.setVisible(false);
        // this.window.setAlwaysOnTop(true);
        this.window.setLayout(new BorderLayout(20, 20));

        this.progressThreads = new HashMap<>();
    }

    /**
     * Fuehrt Suche durch und gibt Ergebnisse zurueck.
     */
    public void addChild(String text) {
        Console.getInstance().write(
                Property.getString("WorkingInProgress.0") + text); //$NON-NLS-1$

        final JLabel suchFortschritt = new JLabel(text);
        // suchFortschritt.setHorizontalAlignment(SwingConstants.CENTER);
        Font old = suchFortschritt.getFont();
        suchFortschritt.setFont(new Font(old.getName(), old.getStyle(), 20));

        this.labels.add(suchFortschritt);
        this.addFortschrittsAnzeige(suchFortschritt);
        SwingUtilities.invokeLater(() -> WorkingInProgress.this.window.setVisible(true));

        this.updateWindow();
    }

    /**
     * Fuegt eine Fortschrittsanzeige in Form von Punkten hinzu
     *
     * @param label
     */
    private void addFortschrittsAnzeige(final JLabel label) {
        // Erzeugt Punkte hinter dem Label zum Anzeigen des Suchvorgangs
        Thread suchFortschrittsAnzeige = new Thread(() -> {
            int counter = 0;

            while (true) {
                if (counter < SUCH_MAX_PUNKTE) {
                    label.setText(label.getText() + "."); //$NON-NLS-1$
                    counter++;
                } else {
                    label.setText(label.getText().substring(0,
                            label.getText().length() - SUCH_MAX_PUNKTE));
                    counter = 0;
                }

                try {
                    Thread.sleep(SUCH_FORTSCHRITT_SLEEP);
                } catch (InterruptedException e) {
                }
            }
        });
        suchFortschrittsAnzeige.start();

        this.progressThreads.put(label, suchFortschrittsAnzeige);
    }

    /**
     * Entfernt den letzten Eintrag.
     */
    public void removeChild() {
        if (this.labels.size() > 0) {
            this.progressThreads
            .remove(this.labels.get(this.labels.size() - 1))
            .interrupt();
            Console.getInstance().write(
                    Property.getString("WorkingInProgress.1") //$NON-NLS-1$
                    + this.labels.remove(this.labels.size() - 1)
                    .getText());
        }

        if (this.labels.size() == 0) {
            SwingUtilities.invokeLater(() -> WorkingInProgress.this.window.setVisible(false));

        } else {
            this.updateWindow();
        }
    }

    /**
     * Erneuert die Anzeige.
     */
    private void updateWindow() {
        JPanel tempSuche = new JPanel(new GridLayout(this.labels.size(), 1, 5,
                5));
        this.labels.forEach(tempSuche::add);

        final JPanel main = new JPanel(new BorderLayout());
        main.add(tempSuche, BorderLayout.NORTH);
        main.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));

        SwingUtilities.invokeLater(() -> {
            WorkingInProgress.this.window.getContentPane().removeAll();
            WorkingInProgress.this.window.getContentPane().add(main,
                    BorderLayout.CENTER);

            WorkingInProgress.this.window.pack();
            WorkingInProgress.this.window.setSize(
                    WorkingInProgress.this.window.getWidth() + 40,
                    10 + (31 * WorkingInProgress.this.labels.size()));
            // System.out.println("width: " + window.getWidth());
            // System.out.println("height: " + window.getHeight());
            WorkingInProgress.this.window
            .setLocation((Toolkit.getDefaultToolkit().getScreenSize().width - WorkingInProgress.this.window.getWidth()) / 2, (Toolkit.getDefaultToolkit().getScreenSize().height - WorkingInProgress.this.window.getHeight()) / 2);

            WorkingInProgress.this.window.validate();
            WorkingInProgress.this.window.repaint();
        });
    }
}

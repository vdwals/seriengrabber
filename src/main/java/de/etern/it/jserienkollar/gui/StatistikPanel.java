/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.etern.it.jserienkollar.gui;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import de.etern.it.jserienkollar.datenbank.importobjekte.Episode;
import de.etern.it.jserienkollar.datenbank.importobjekte.Season;
import de.etern.it.jserienkollar.datenbank.importobjekte.Series;
import de.etern.it.jserienkollar.datenbank.SeriesHandler;

/**
 * Klasse fuer das Panel zum Anzeigen verschiedener, statistischer Werte.
 *
 * @author Dennis van der Wals
 *
 */
public class StatistikPanel extends JPanel {

    private static final StatistikPanel INSTANCE         = new StatistikPanel();
    
    public static final Color     UNCHECKED_BACKGROUND = new Color(242, 242,
                                                               242),
            UNCHECKED_FONT = new Color(63, 63, 63), OK_BACKGROUND = new Color(
                    198, 239, 206), OK_FONT = new Color(0, 97, 0),
            FAILED_BACKGROUND = new Color(255, 199, 206),
            FAILED_FONT = new Color(156, 0, 6), NEUTRAL_BACKGROUND = new Color(
                    255, 235, 156), NEUTRAL_FONT = new Color(156, 101, 0);
    
    /**
     * Gibt die einzige Instanz des Panels zurueck.
     *
     * @return Instanz des Panels
     */
    public static StatistikPanel getInstance() {
        return INSTANCE;
    }
    
    private final JLabel serien;
    private final JLabel staffeln;
    private final JLabel episoden;
    private final JLabel ungesehen;
    private final JLabel links;
    private final JLabel offline;
    private final JLabel streams;
    private JLabel status;
    
    public StatistikPanel() {
        this.serien = new JLabel();
        this.staffeln = new JLabel();
        this.episoden = new JLabel();
        this.ungesehen = new JLabel();
        this.links = new JLabel();
        this.offline = new JLabel();
        this.streams = new JLabel();
        
        this.setLayout(new GridLayout(1, 8, 5, 5));
        this.add(this.getSubPanel("Serien", this.serien));
        this.add(this.getSubPanel("Staffeln", this.staffeln));
        this.add(this.getSubPanel("Episoden", this.episoden));
        this.add(this.getSubPanel("Ungesehen", this.ungesehen));
        this.add(this.getSubPanel("Links", this.links));
        this.add(this.getSubPanel("Streams", this.streams));
        this.add(this.getSubPanel("Offline", this.offline));
        this.add(this.getSubPanel("Status", this.getStatus()));
        
        this.update();
    }
    
    /**
     * Erstellt ein Standardpanel mit titel
     *
     * @param title
     *            Titel des Panels
     * @param label
     *            Hinzuzuf�gendes label
     * @return Panel
     */
    private JPanel getSubPanel(String title, JLabel label) {
        JPanel subPanel = new JPanel();
        subPanel.setBorder(BorderFactory.createTitledBorder(title));
        subPanel.setLayout(new GridLayout(1, 1));
        subPanel.add(label);
        return subPanel;
    }
    
    public void update() {
        this.serien
                .setText(SeriesHandler.getInstance().getSeries().size() + "");
        
        int staffelZaehler = 0;
        int episodenZaehler = 0;
        int linkZaehler = 0;
        int ungesehenZaehler = 0;
        int streamZaehler = SeriesHandler.getInstance().getOfflineLinksSize();
        
        for (Series serie : SeriesHandler.getInstance().getSeries()) {
            linkZaehler += serie.getLinks().size();
            staffelZaehler += serie.getSeasons().size();
            
            for (Season staffel : serie.getSeasons()) {
                episodenZaehler += staffel.getEpisodes().size();
                
                for (Episode episode : staffel.getEpisodes()) {
                    linkZaehler += episode.getProvider().size();
                    streamZaehler += episode.getStreams().size();
                    ungesehenZaehler += episode.getSeen();
                }
            }
        }
        
        // Umkehren, da gesehene gezaehlt wurden.
        ungesehenZaehler = episodenZaehler - ungesehenZaehler;
        
        linkZaehler += streamZaehler;
        
        this.staffeln.setText(staffelZaehler + "");
        this.episoden.setText(episodenZaehler + "");
        this.ungesehen.setText(ungesehenZaehler + "");
        this.links.setText(linkZaehler + "");
        this.offline.setText(SeriesHandler.getInstance().getOfflineLinksSize()
                + "");
        this.streams.setText(streamZaehler + "");
    }
    
    /**
     * @return the status
     */
    public JLabel getStatus() {
        if (status == null) {
            status = new JLabel("Unknown");
            status.setBackground(NEUTRAL_BACKGROUND);
            status.setForeground(NEUTRAL_FONT);
            status.setOpaque(true);
        }
        return status;
    }
    
    /**
     * @param status
     *            the status to set
     */
    public void setStatus(boolean online) {
        if (online) {
            getStatus().setText("Online");
            getStatus().setBackground(OK_BACKGROUND);
            getStatus().setForeground(OK_FONT);
        } else {
            getStatus().setText("Offline");
            getStatus().setBackground(FAILED_BACKGROUND);
            getStatus().setForeground(FAILED_FONT);
        }
    }
}

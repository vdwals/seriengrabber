/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
/**
 *
 */
package de.etern.it.jserienkollar.gui;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

import javax.swing.*;

import de.etern.it.jserienkollar.datenbank.importobjekte.Episode;
import de.etern.it.jserienkollar.datenbank.importobjekte.Link;
import de.etern.it.jserienkollar.datenbank.SearchResult;
import de.etern.it.jserienkollar.datenbank.importobjekte.Season;
import de.etern.it.jserienkollar.datenbank.importobjekte.Series;
import de.etern.it.jserienkollar.datenbank.SeriesHandler;
import de.etern.it.jserienkollar.implement.BugReport;
import de.etern.it.properties.Property;
import de.etern.it.jserienkollar.net.LinkProvider;
import de.etern.it.jserienkollar.net.ProviderHandler;

/**
 * Klasse zum Ausfuehren einer Suche
 *
 * @author Dennis
 *
 */
public class Suche extends JFrame implements ActionListener {
    private static final int  PREF_WIDTH       = 600, PREF_HEIGHT = 600;

    /**
     * @param args
     */
    public static void main(String[] args) {
        Suche s = new Suche();
        s.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        s.setVisible(true);
    }
    
    private JTextField                 suchString;
    private JButton                    suche, add, cancel;
    private JCheckBox                  gesehen;
    
    private JPanel                     ergebnisse;
    final private ArrayList<JPanel>    suchErgebnisPanel = new ArrayList<>();
    ArrayList<SearchResult>            suchErgebnisse;
    final private ArrayList<JCheckBox> checkboxes        = new ArrayList<>();
        
    private final ActionListener             linkButton;
    private final ActionListener checkboxListener;
    
    /** Standardkonstruktor **/
    public Suche() {
        this.setSize(PREF_WIDTH, PREF_HEIGHT);
        
        JPanel top = new JPanel(new BorderLayout(5, 5));
        top.add(new JLabel("Suchbegriff oder URL eingeben:"),
                BorderLayout.NORTH);
        top.add(this.getSuchString(), BorderLayout.CENTER);
        top.add(this.getSuche(), BorderLayout.EAST);
        top.add(this.getGesehen(), BorderLayout.SOUTH);
        
        JScrollPane ergebnisse = new JScrollPane(this.getErgebnisse());
        
        JPanel bottom = new JPanel(new BorderLayout());
        
        JPanel bottomButtons = new JPanel(new GridLayout());
        bottomButtons.add(this.getAdd());
        bottomButtons.add(this.getCancel());
        
        bottom.add(bottomButtons, BorderLayout.EAST);
        
        this.setLayout(new BorderLayout(10, 10));
        this.add(top, BorderLayout.NORTH);
        this.add(ergebnisse, BorderLayout.CENTER);
        this.add(bottom, BorderLayout.SOUTH);
        this.getRootPane().setDefaultButton(this.getSuche());
        
        this.linkButton = e -> {
            try {
                Desktop.getDesktop().browse(new URI(e.getActionCommand()));
            } catch (IOException | URISyntaxException e1) {
                BugReport.recordBug(e1, false);
            }
        };
        
        this.checkboxListener = new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent arg0) {
                Suche.this.getAdd().setEnabled(false);
                for (JCheckBox checkBox : Suche.this.checkboxes) {
                    if (checkBox.isSelected()) {
                        Suche.this.getAdd().setEnabled(true);
                        break;
                    }
                }
            }
        };
        
        this.setLocation(
                (Toolkit.getDefaultToolkit().getScreenSize().width - this
                        .getWidth()) / 2, (Toolkit.getDefaultToolkit()
                        .getScreenSize().height - this.getHeight()) / 2);
    }
    
    @Override
    public void actionPerformed(ActionEvent arg0) {
        if (arg0.getSource() == this.getSuche()) {
            
            WorkingInProgress.getInstance().addChild(
                    Property.getString("Suche.0")); //$NON-NLS-1$
            this.sucheStarten(this.getSuchString().getText());
            
        } else if (arg0.getSource() == this.getAdd()) {
            this.serieErstellen();
            
        } else if (arg0.getSource() == this.getCancel()) {
            this.setVisible(false); // verschwinden lassen
            this.dispose();
        }
    }
    
    /**
     * @return the add
     */
    private JButton getAdd() {
        if (this.add == null) {
            this.add = new JButton(Property.getString("Suche.1")); //$NON-NLS-1$
            this.add.setEnabled(false);
            this.add.addActionListener(this);
        }
        return this.add;
    }
    
    /**
     * @return the cancel
     */
    private JButton getCancel() {
        if (this.cancel == null) {
            this.cancel = new JButton(Property.getString("Suche.2")); //$NON-NLS-1$
            this.cancel.addActionListener(this);
        }
        return this.cancel;
    }
    
    /**
     * @return the ergebnisse
     */
    private JPanel getErgebnisse() {
        if (this.ergebnisse == null) {
            this.ergebnisse = new JPanel(new GridLayout(1, 1));
        }
        
        return this.ergebnisse;
    }
    
    /**
     * Erzeugt ein Panel fuer die Anzeige eines Ergebnisses
     *
     * @param titel
     *            Titel des Ergebnisses
     * @param sprache
     *            Sprache des Ergebnisses
     * @param link
     *            Link zum Ergebnis
     * @return Jpanel
     */
    private JPanel getErgebnissPanel(int i, String titel, String sprache,
            String link) {
        JPanel ergebnis = new JPanel(new BorderLayout(15, 15));
        
        JPanel center = new JPanel(new BorderLayout(15, 15));
        center.add(new JLabel(titel), BorderLayout.CENTER);
        center.add(new JLabel(sprache), BorderLayout.EAST);
        
        JButton b = new JButton(Property.getString("Suche.3")); //$NON-NLS-1$
        b.setActionCommand(link);
        b.addActionListener(this.linkButton);
        
        ergebnis.add(b, BorderLayout.EAST);
        ergebnis.add(center, BorderLayout.CENTER);
        
        JCheckBox c = new JCheckBox();
        c.addActionListener(this.checkboxListener);
        this.checkboxes.add(i, c);
        ergebnis.add(c, BorderLayout.WEST);
        
        JPanel main = new JPanel(new BorderLayout());
        main.add(ergebnis, BorderLayout.NORTH);
        
        return main;
    }
    
    /**
     * @return the gesehen
     */
    private JCheckBox getGesehen() {
        if (this.gesehen == null) {
            this.gesehen = new JCheckBox(Property.getString("Suche.4")); //$NON-NLS-1$
        }
        return this.gesehen;
    }
    
    /**
     * @return the suche
     */
    private JButton getSuche() {
        if (this.suche == null) {
            this.suche = new JButton(Property.getString("Suche.5")); //$NON-NLS-1$
            this.suche.addActionListener(this);
        }
        return this.suche;
    }
    
    /**
     * @return the suchString
     */
    private JTextField getSuchString() {
        if (this.suchString == null) {
            this.suchString = new JTextField();
        }
        
        return this.suchString;
    }
    
    /**
     * Started den Thread zum sammeln der erstinformationen einer neuen Serie.
     */
    private void serieErstellen() {
        final String name = JOptionPane.showInputDialog(this, "Serientitel");
        
        if (name != null) {
            
            SwingUtilities.invokeLater(() -> {
                Suche.this.getErgebnisse().removeAll();
                Suche.this.validate();
                Suche.this.repaint();
            });
            
            new Thread(() -> {
                ArrayList<SearchResult> gewaehlteErgebnisse = new ArrayList<>();

                // Auswahl filtern
                for (int i = 0; i < Suche.this.checkboxes.size(); i++) {
                    if (Suche.this.checkboxes.get(i).isSelected()) {
                        gewaehlteErgebnisse.add(Suche.this.suchErgebnisse
                                .get(i));
                    }
                }

                Series neu = new Series();
                neu.setTitel(name);
                for (SearchResult searchResult : gewaehlteErgebnisse) {
                    Series serieVonProvider = ProviderHandler
                            .getInstance()
                            .identifyLinkProvider(searchResult.getProvider())
                            .getSerie(searchResult.getLink(), name);

                    if (serieVonProvider != null) {
                        neu.joinSeries(serieVonProvider);
                    }
                }

                SeriesHandler.getInstance().addSerie(neu);
                SwingUtilities.invokeLater(() -> TreePanel.getInstance().update());

                for (Link providerLink : neu.getLinks()) {
                    ProviderHandler
                            .getInstance()
                            .identifyLinkProvider(providerLink.getProviderID())
                            .updateSerie(neu);
                }

                // Gesehen Status setzen
                if (Suche.this.getGesehen().isSelected()) {
                    for (Season staffel : neu.getSeasons()) {
                        for (Episode episode : staffel.getEpisodes()) {
                            episode.setSeen(Suche.this.getGesehen()
                                    .isSelected());
                        }
                    }
                }

                JOptionPane.showMessageDialog(null,
                        Property.getString("Suche.6")); //$NON-NLS-1$

                SwingUtilities.invokeLater(() -> TreePanel.getInstance().update());
            }).start();
        }
    }
    
    /**
     * Zeigt die Ergebnisse im Panel an
     */
    private void suchErgebnisseAnzeigen() {
        JPanel tempSuche = new JPanel(new GridLayout(
                this.suchErgebnisPanel.size(), 1));

        this.suchErgebnisPanel.forEach(tempSuche::add);
        
        final JPanel main = new JPanel(new BorderLayout(15, 15));
        main.add(tempSuche, BorderLayout.NORTH);
        
        SwingUtilities.invokeLater(() -> {
            Suche.this.getErgebnisse().removeAll();
            Suche.this.getErgebnisse().add(main);
            Suche.this.validate();
            Suche.this.repaint();
        });
        
        WorkingInProgress.getInstance().removeChild();
    }
    
    /**
     * Initialisiert die Suche
     *
     * @param suchtext
     */
    private void sucheStarten(final String suchtext) {
        new Thread(() -> {

            Suche.this.suche.setEnabled(false);

            Suche.this.suchErgebnisse = new ArrayList<>();
            for (final LinkProvider linkProvider : ProviderHandler
                    .getInstance().getLinkProvider()) {
                Suche.this.suchErgebnisse.addAll(linkProvider
                        .search(suchtext));
            }

            // Erzeugt die Ergebnisanzeige
            Suche.this.suchErgebnisPanel.clear();
            Suche.this.checkboxes.clear();
            for (int i = 0; i < Suche.this.suchErgebnisse.size(); i++) {
                Suche.this.suchErgebnisPanel.add(Suche.this
                        .getErgebnissPanel(i, Suche.this.suchErgebnisse.get(i).getTitle(), Suche.this.suchErgebnisse.get(i).getLanguage().toString(), Suche.this.suchErgebnisse.get(i).getLink()));
            }
            // DEBUG closed
            // System.out.println("Suche abgeschlossen");

            Suche.this.suchErgebnisseAnzeigen();

            Suche.this.suche.setEnabled(true);
        }).start();
    }
}

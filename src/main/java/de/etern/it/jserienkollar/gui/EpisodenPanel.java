/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.etern.it.jserienkollar.gui;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;

import de.etern.it.jserienkollar.Checker;
import de.etern.it.jserienkollar.datenbank.importobjekte.Episode;
import de.etern.it.jserienkollar.datenbank.importobjekte.Link;
import de.etern.it.jserienkollar.implement.BugReport;
import de.etern.it.jserienkollar.net.LinkProvider;
import de.etern.it.jserienkollar.net.ProviderHandler;
import de.etern.it.jserienkollar.net.StreamProvider;
import de.etern.it.properties.Property;

/**
 * Klasse fuer die Anzeige von Episoden
 *
 * @author Dennis van der Wals
 */
public class EpisodenPanel extends JPanel {
	/**
	 * Default Seria-ID
	 */
	private static final long serialVersionUID = 1L;

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm"); //$NON-NLS-1$

	private Episode episode;
	private final ActionListener linkButton, checkboxListener, actButtonListener;

	private JLabel titel, datumErstellt, datumAktuallisiert;
	private JButton actualize;
	private JScrollPane streams;
	private JCheckBox gesehen;

	/**
	 * Konstruktor
	 */
	public EpisodenPanel() {
		this.linkButton = e -> {
			try {
				Desktop.getDesktop().browse(new URI(e.getActionCommand()));
			} catch (IOException | URISyntaxException e1) {
				BugReport.recordBug(e1, false);
			}
		};

		this.checkboxListener = arg0 -> {
			if (EpisodenPanel.this.episode != null) {
				EpisodenPanel.this.episode.setSeen(EpisodenPanel.this.getGesehen().isSelected());

				TreePanel.getInstance().update();
			}
		};

		this.actButtonListener = arg0 -> {
			if (EpisodenPanel.this.episode != null) {

				Episode e = EpisodenPanel.this.episode;

				for (LinkProvider provider : ProviderHandler.getInstance().getLinkProvider()) {
					provider.updateSerie(e.getStaffel().getSerie());
				}

				final Checker<Episode> episodeChecker = new Checker<>(
						e.getStaffel().getSerie().getTitel() + "-Streamthread");
				episodeChecker.addToCheck(e);

				final Checker<Link> linkChecker = new Checker<>(e.getStaffel().getSerie().getTitel() + "-Linkthread");
				linkChecker.addToCheck(EpisodenPanel.this.episode.getStreams());

				new Thread(() -> {
					Thread t = new Thread(episodeChecker);
					t.start();
					try {
						t.join();
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}

					new Thread(linkChecker).start();
				}).start();
			}
		};

		build();
	}

	/**
	 * Erzeugt die graphische Oberflaeche.
	 */
	private void build() {
		removeAll();

		setLayout(new BorderLayout(5, 5));

		JPanel daten = new JPanel(new GridLayout(2, 1));
		daten.add(getDatumErstellt());
		daten.add(getDatumAktualisiert());

		JPanel topLine = new JPanel(new BorderLayout(5, 5));
		topLine.add(getTitel(), BorderLayout.WEST);
		topLine.add(daten, BorderLayout.EAST);

		JPanel top = new JPanel(new BorderLayout(5, 5));
		top.add(topLine, BorderLayout.NORTH);
		top.add(getGesehen(), BorderLayout.WEST);
		top.add(new JLabel(" "), BorderLayout.CENTER);
		top.add(getActualize(), BorderLayout.EAST);

		this.add(top, BorderLayout.NORTH);
		this.add(getStreams(), BorderLayout.CENTER);

		validate();
		this.repaint();
	}

	/**
	 * @return the actualize
	 */
	private JButton getActualize() {
		if (this.actualize == null) {
			this.actualize = new JButton("Aktuallisieren");
			this.actualize.addActionListener(this.actButtonListener);
		}
		return this.actualize;
	}

	/**
	 * @return the datumAktuallisiert
	 */
	private JLabel getDatumAktualisiert() {
		if (this.datumAktuallisiert == null) {
			this.datumAktuallisiert = new JLabel(Property.getString("EpisodenPanel.1")); //$NON-NLS-1$
			this.datumAktuallisiert.setHorizontalAlignment(SwingConstants.RIGHT);

			Font old = this.datumAktuallisiert.getFont();
			this.datumAktuallisiert.setFont(new Font(old.getFontName(), old.getStyle(), 11));

			if ((this.episode != null) && (this.episode.getEpisodeUpdate() != null)) {
				this.datumAktuallisiert.setText(Property.getString("EpisodenPanel.1") //$NON-NLS-1$
						+ dateFormat.format(this.episode.getEpisodeUpdate()));
			}
		}
		return this.datumAktuallisiert;
	}

	/**
	 * @return the datumErstellt
	 */
	private JLabel getDatumErstellt() {
		if (this.datumErstellt == null) {
			this.datumErstellt = new JLabel(Property.getString("EpisodenPanel.3")); //$NON-NLS-1$
			this.datumErstellt.setHorizontalAlignment(SwingConstants.RIGHT);

			Font old = this.datumErstellt.getFont();
			this.datumErstellt.setFont(new Font(old.getFontName(), old.getStyle(), 11));

			if (this.episode != null) {
				this.datumErstellt.setText(Property.getString("EpisodenPanel.3") //$NON-NLS-1$
						+ dateFormat.format(this.episode.getCreation()));
			}
		}
		return this.datumErstellt;
	}

	/**
	 * @return the gesehen
	 */
	private JCheckBox getGesehen() {
		if (this.gesehen == null) {
			this.gesehen = new JCheckBox(Property.getString("EpisodenPanel.5")); //$NON-NLS-1$
			this.gesehen.addActionListener(this.checkboxListener);

			if (this.episode != null) {
				this.gesehen.setSelected(this.episode.hasBeenSeen());
			}
		}
		return this.gesehen;
	}

	private JButton getLinkButton(List<Link> link) {
		StreamProvider streamProvider = ProviderHandler.getInstance()
				.identifyStreamProvider(link.get(0).getProviderID());

		if (streamProvider == null) {
			return null;
		}

		JLabel name = new JLabel(streamProvider.getName());
		Font old = name.getFont();
		name.setFont(new Font(old.getName(), Font.BOLD, old.getSize()));

		JLabel checked = new JLabel(Property.getString("EpisodenPanel.6")); //$NON-NLS-1$
		old = checked.getFont();
		checked.setFont(new Font(old.getName(), old.getStyle(), old.getSize() - 2));

		JPanel linkInfo = new JPanel();
		linkInfo.setLayout(new BorderLayout(5, 5));
		linkInfo.add(name, BorderLayout.NORTH);
		linkInfo.add(checked, BorderLayout.SOUTH);
		linkInfo.setOpaque(false);

		JButton openLink = new JButton();
		openLink.setLayout(new BorderLayout(5, 5));
		openLink.add(linkInfo, BorderLayout.NORTH);
		openLink.addActionListener(this.linkButton);
		openLink.setActionCommand(link.get(0).getLink());
		openLink.setPreferredSize(new Dimension(200, 75));

		LinkCounter[] links = new LinkCounter[link.size()];
		if (links.length > 1) {
			for (int index = 0; index < link.size(); index++) {
				links[index] = new LinkCounter((index + 1) + "/" + links.length, link.get(index));
			}

			JComboBox<LinkCounter> linkList = new JComboBox<>(links);
			linkList.addActionListener(e -> {
				@SuppressWarnings("unchecked")
				JComboBox<LinkCounter> source = (JComboBox<LinkCounter>) e.getSource();
				LinkCounter selected = ((LinkCounter) source.getSelectedItem());
				setLinkInfo(selected.getLink(), checked, openLink);
			});

			openLink.add(linkList, BorderLayout.SOUTH);
		}

		setLinkInfo(link.get(0), checked, openLink);

		return openLink;
	}

	/**
	 * @return the streams
	 */
	private JScrollPane getStreams() {
		if (this.streams == null) {
			JPanel buttons = new JPanel();
			// buttons.setLayout(new FlowLayout(FlowLayout.LEFT, 10, 10));
			buttons.setLayout(new ModifiedFlowLayout(ModifiedFlowLayout.LEFT, 10, 10));
			// buttons.setLayout(new BoxLayout(buttons, BoxLayout.LINE_AXIS));

			if (this.episode != null) {
				// DEBUG filter von provider auf offline setzen
				Map<StreamProvider, java.util.List<Link>> existingButtonMap = new HashMap<>();
				this.episode.getStreams().stream().filter(link -> !link.isOffline()).forEach(link -> {
					StreamProvider streamProvider = ProviderHandler.getInstance()
							.identifyStreamProvider(link.getProviderID());
					if (streamProvider != null) {
						if (!existingButtonMap.containsKey(streamProvider)) {
							existingButtonMap.put(streamProvider, new LinkedList<>());
						}
						existingButtonMap.get(streamProvider).add(link);
					}
				});

				existingButtonMap.keySet().stream().forEach(key -> {
					JButton button = getLinkButton(existingButtonMap.get(key));

					buttons.add(button);
				});
			}

			JPanel content = new JPanel(new BorderLayout());
			content.add(buttons, BorderLayout.NORTH);

			this.streams = new JScrollPane(content, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
					ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		}
		return this.streams;
	}

	/**
	 * @return the titel
	 */
	private JLabel getTitel() {
		if (this.titel == null) {
			this.titel = new JLabel(Property.getString("EpisodenPanel.7")); //$NON-NLS-1$
			Font old = this.titel.getFont();
			this.titel.setFont(new Font(old.getFontName(), old.getStyle(), 22));

			if (this.episode != null) {
				this.titel.setText(Property.getString("EpisodenPanel.8") //$NON-NLS-1$
						+ this.episode.getEpisodeNumber());
			}
		}
		return this.titel;
	}

	/**
	 * Setzt die Episode und erzeugt die neue Oberflaeche.
	 *
	 * @param episode
	 *            the episode to set
	 */
	public void setEpisode(Episode episode) {
		this.episode = episode;
		this.titel = null;
		this.datumAktuallisiert = null;
		this.datumErstellt = null;
		this.streams = null;
		this.gesehen = null;

		build();
	}

	private void setLinkInfo(Link link, JLabel checked, JButton openLink) {
		if (link.getLinkUpdated() != null) {
			checked.setText(dateFormat.format(link.getLinkUpdated()));
		}

		String hoverTxt = "<html>";
		hoverTxt += "<head><title>"
				+ ProviderHandler.getInstance().identifyStreamProvider(link.getProviderID()).getName()
				+ "</title></head><body><table border='0' align='left'>";

		hoverTxt += "<tr><th>" + "Adresse: " + "</th><th>" + link.getLink() + "</th></tr>";

		hoverTxt += "<tr><th>" + "Erstellt: " + "</th><th>" + link.getLinkCreated() + "</th></tr>";

		hoverTxt += "<tr><th>" + "Aktuallisiert: " + "</th><th>";
		if (link.getLinkUpdated() == null) {
			hoverTxt += "Nie" + "</th></tr>";
		} else {
			hoverTxt += link.getLinkUpdated() + "</th></tr>";
		}

		hoverTxt += "<tr><th>" + "Status: " + "</th><th>";
		if (link.isOffline()) {
			hoverTxt += "Offline" + "</th></tr>";
		} else {
			hoverTxt += "Online" + "</th></tr>";
		}

		hoverTxt += "</table></body></html>";

		openLink.setToolTipText(hoverTxt);
	}

}

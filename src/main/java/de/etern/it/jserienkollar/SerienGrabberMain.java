/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.etern.it.jserienkollar;

import de.etern.it.jserienkollar.datenbank.SeriesHandler;
import de.etern.it.jserienkollar.gui.*;
import de.etern.it.jserienkollar.implement.BugReport;
import de.etern.it.jserienkollar.implement.Console;
import de.etern.it.jserienkollar.implement.SplashScreen;
import de.etern.it.jserienkollar.implement.Version;
import de.etern.it.progressMonitor.CommonProgressMonitor;
import de.etern.it.properties.Property;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 * Hauptklasse zur Serienverwaltung, -�berwachung und -auflistung.
 *
 * @author Dennis van der Wals
 */
public class SerienGrabberMain extends JFrame {

	/**
	 * Generated SerialID
	 */
	private static final long serialVersionUID = -1857347949809776315L;

	private static SerienGrabberMain INSTANCE;

	public static final String SERIES_PANEL = "SERIES_PANEL", //$NON-NLS-1$
			SEASON_PANEL = "SEASON_PANEL", EPISODE_PANEL = "EPISODE_PANEL"; //$NON-NLS-1$ //$NON-NLS-2$

	public static SerienGrabberMain getInstance() {
		return INSTANCE;
	}

	/** Main methode **/
	public static void main(String[] args) {
		// Setzte Look&Feel
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			BugReport.recordBug(e, true);
		}

		Property.PACKET = "de.etern.it.jserienkollar";
		Property.setConfig("version", new Version(0, 2, 8, Version.ALPHA).toString());
		INSTANCE = new SerienGrabberMain();
		SwingUtilities.invokeLater(() -> SerienGrabberMain.getInstance().doIt());
	}

	private Suche suche;

	/* GUI */
	private final JPanel information;
	private final CardLayout cards;
	private final EpisodenPanel episodenPanel;
	private final SeriePanel serienPanel;

	private JCheckBox filterUnseen;

	/** Standardkonstruktor **/
	public SerienGrabberMain() {
		CommonProgressMonitor.INSTANCE.start();

		SplashScreen.getInstance().showSplash();
		SeriesHandler.getInstance();

		this.episodenPanel = new EpisodenPanel();
		this.serienPanel = new SeriePanel();

		this.information = new JPanel(new GridLayout(1, 1));
		this.cards = new CardLayout();
		this.information.setLayout(this.cards);

		this.information.add(this.serienPanel, SERIES_PANEL);
		this.information.add(this.episodenPanel, EPISODE_PANEL);
		// this.cardPanel.add(this.editorPanel, EDITOR_PANEL);

		WindowListener suchUpdate = new WindowListener() {

			@Override
			public void windowActivated(WindowEvent e) {
			}

			@Override
			public void windowClosed(WindowEvent e) {
				if (e.getSource() == SerienGrabberMain.this.suche) {
					SerienGrabberMain.this.setEnabled(true);
					TreePanel.getInstance().update();
				}
			}

			@Override
			public void windowClosing(WindowEvent e) {
				if (e.getSource() == SerienGrabberMain.this.suche) {
					SerienGrabberMain.this.setEnabled(true);
					TreePanel.getInstance().update();

				} else if (e.getSource() == SerienGrabberMain.this) {
					SerienGrabberMain.this.close();
				}
			}

			@Override
			public void windowDeactivated(WindowEvent e) {
			}

			@Override
			public void windowDeiconified(WindowEvent e) {
			}

			@Override
			public void windowIconified(WindowEvent e) {
			}

			@Override
			public void windowOpened(WindowEvent e) {
				if (e.getSource() == SerienGrabberMain.this.suche) {
					SerienGrabberMain.this.setEnabled(false);
				}
			}
		};

		this.suche = new Suche();
		this.suche.addWindowListener(suchUpdate);

		this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		this.addWindowListener(suchUpdate);
		this.buildGUI();
		this.setLocationRelativeTo(null);
	}

	/**
	 * Baut die GUI auf.
	 */
	private void buildGUI() {
		this.setSize(800, 600);
		this.setJMenuBar(this.getMenu());
		this.setLayout(new BorderLayout(5, 5));

		JScrollPane consolePane = new JScrollPane(Console.getInstance().getOutput(),
				ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		JPanel treePanel = new JPanel(new BorderLayout(5, 5));
		treePanel.add(getFilterUnseen(), BorderLayout.NORTH);
		treePanel.add(TreePanel.getInstance().getScrollPane(), BorderLayout.CENTER);

		JSplitPane serien = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, true, treePanel, this.information);
		JSplitPane front = new JSplitPane(JSplitPane.VERTICAL_SPLIT, true, serien, consolePane);

		JScrollPane progressPane = new JScrollPane(CommonProgressMonitor.INSTANCE.getPanel(),
				ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

		JTabbedPane main = new JTabbedPane(JTabbedPane.BOTTOM);
		main.addTab("Serien", front);
		main.addTab("Fortschritte", progressPane);

		this.add(main, BorderLayout.CENTER);
		this.add(StatistikPanel.getInstance(), BorderLayout.SOUTH);
	}

	/**
	 * Schlie�t das Programm.
	 */
	private void close() {
		SeriesHandler.getInstance().save();

		CommonProgressMonitor.INSTANCE.stop();

		System.exit(0);
	}

	/** Startet den Programmablauf **/
	public void doIt() {
		if (SeriesHandler.getInstance().getSeries().size() > 0)
			this.serienPanel.setSeries(SeriesHandler.getInstance().getSeries().get(0));
		this.setVisible(true);

		JWindow w = new JWindow();
		w.setSize(1, 1);
		w.setVisible(true);

		SplashScreen.getInstance().hideSplash();

		new Thread(DatabaseChecker.getInstance()).start();
	}

	/**
	 * @return the episodenPanel
	 */
	public EpisodenPanel getEpisodenPanel() {
		return this.episodenPanel;
	}

	/**
	 * @return the filterUnseen
	 */
	public JCheckBox getFilterUnseen() {
		if (this.filterUnseen == null) {
			this.filterUnseen = new JCheckBox(Property.getString("SerienGrabberMain.7")); //$NON-NLS-1$

			this.filterUnseen.setSelected(Property.getBoolean("filterUnseen.active")); //$NON-NLS-1$
			TreePanel.getInstance().showSeen = !SerienGrabberMain.this.filterUnseen.isSelected();
			TreePanel.getInstance().update();

			this.filterUnseen.addActionListener(e -> {
				TreePanel.getInstance().showSeen = !SerienGrabberMain.this.filterUnseen.isSelected();
				TreePanel.getInstance().update();

				Property.setBoolean("filterUnseen.active", //$NON-NLS-1$
						SerienGrabberMain.this.filterUnseen.isSelected());
			});
		}
		return this.filterUnseen;
	}

	private JMenuBar getMenu() {
		JMenuBar menu = new JMenuBar();

		JMenu file = new JMenu(Property.getString("SerienGrabberMain.2")); //$NON-NLS-1$
		file.add(new AbstractAction(Property.getString("SerienGrabberMain.3")) { //$NON-NLS-1$

			/** Serial-ID */
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				SerienGrabberMain.this.suche.setVisible(true);
			}
		});
		file.add(new AbstractAction("Verzeichnis") { //$NON-NLS-1$

			/** Serial-ID */
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				SeriesHandler.changePath();
			}
		});
		menu.add(file);

		JMenu edit = new JMenu(Property.getString("SerienGrabberMain.4")); //$NON-NLS-1$
		edit.add(new AbstractAction(Property.getString("SerienGrabberMain.5")) { //$NON-NLS-1$
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {

				// Setze alle markierten Episoden auf gesehen
				TreePanel.getInstance().setSelection(true);
			}
		});

		edit.add(new AbstractAction(Property.getString("SerienGrabberMain.6")) { //$NON-NLS-1$
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {

				// Setze alle markierten Episoden auf gesehen
				TreePanel.getInstance().setSelection(false);

			}
		});
		menu.add(edit);

		return menu;
	}

	/**
	 * Sets the active Panel of the CardLayout.
	 *
	 * @param card
	 *            Panel to activate
	 */
	public void setPanel(String card) {
		this.cards.show(this.information, card);
	}

	/**
	 * @return the serienPanel
	 */
	public SeriePanel getSerienPanel() {
		return serienPanel;
	}

}

/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.etern.it.jserienkollar.help;

import de.etern.it.bugReport.BugReport;

import javax.swing.*;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import java.awt.*;
import java.io.IOException;
import java.net.URI;

/**
 * Kleines Fenster zum Anzeigen einer Programmhilfe. Bilder ins Netz auslagern
 * (In den html-Dateien)
 *
 * @author Dennis van der Wals
 */
public class Help extends JFrame implements HyperlinkListener {
    private JEditorPane inhalt, kontext;

    /**
     * Standardkonstruktor. Laedt die Vordefinierten Hilfeseiten.
     *
     * @throws IOException
     */
    public Help() throws IOException {
        this.inhalt = new JEditorPane(ClassLoader.getSystemResource("gui/help/index_de.html"));
        this.inhalt.addHyperlinkListener(this);
        this.inhalt.setEditable(false);

        this.kontext = new JEditorPane(ClassLoader.getSystemResource("gui/help/einleitung_de.html"));
        this.kontext.addHyperlinkListener(this);
        this.kontext.setEditable(false);

        this.setLayout(new BorderLayout(5, 5));

        JScrollPane inhaltContainer = new JScrollPane(this.inhalt);
        JScrollPane kontextContainer = new JScrollPane(this.kontext);

        this.add(inhaltContainer, BorderLayout.WEST);
        this.add(kontextContainer, BorderLayout.CENTER);

        this.pack();
        this.setVisible(false);
    }

    @Override
    public void hyperlinkUpdate(HyperlinkEvent arg0) {
        HyperlinkEvent.EventType typ = arg0.getEventType();

        if (typ == HyperlinkEvent.EventType.ACTIVATED) {
            if (arg0.getSource() == this.inhalt) {
                try {
                    this.kontext.setPage(arg0.getURL());
                } catch (IOException e) {
                    BugReport.recordBug(e, false);
                }
            } else {
                try {
                    Desktop.getDesktop().browse(new URI(arg0.getURL().toString()));
                } catch (Exception e) {
                    BugReport.recordBug(e, false);
                }
            }
        }
    }

}
